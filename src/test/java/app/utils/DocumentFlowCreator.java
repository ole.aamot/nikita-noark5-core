package app.utils;

import app.domain.noark5.admin.User;
import app.domain.noark5.metadata.FlowStatus;
import app.domain.noark5.secondary.DocumentFlow;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.secondary.DocumentFlowSerializer;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;
import java.io.StringWriter;
import java.util.UUID;

public final class DocumentFlowCreator {

    /**
     * Create a default DocumentFlow for testing purposes. Two users are
     * created and associated with the document flow. A From-user and a To-user.
     *
     * @return a DocumentFlow with all values set
     */
    public static DocumentFlow createDocumentFlow() {
        User admin = new User();
        admin.setUsername("test_user_admin@example.com");
        admin.setSystemId(UUID.randomUUID());
        User recordKeeper = new User();
        recordKeeper.setSystemId(UUID.randomUUID());
        recordKeeper.setUsername("recordkeeper@example.com");
        DocumentFlow documentFlow = new DocumentFlow();
        documentFlow.setReferenceFlowFrom(admin);
        documentFlow.setFlowFrom(admin.getUsername());
        documentFlow.setReferenceFlowFromSystemID(UUID.randomUUID());
        documentFlow.setFlowTo(recordKeeper.getUsername());
        documentFlow.setReferenceFlowTo(recordKeeper);
        documentFlow.setReferenceFlowToSystemID(UUID.randomUUID());
        documentFlow.setFlowStatus(new FlowStatus("F", "Til fordeling"));
        return documentFlow;
    }

    public static String createUpdatedDocumentFlowAsJSON() throws IOException {
        DocumentFlow documentFlow = createDocumentFlow();
        documentFlow.setFlowComment("Added a comment to the flow!");
        return createDocumentFlowAsJSON(documentFlow);
    }

    public static String createDocumentFlowAsJSON() throws IOException {
        return createDocumentFlowAsJSON(createDocumentFlow());
    }

    public static String createDocumentFlowAsJSON(DocumentFlow documentFlow) throws IOException {
        StringWriter jsonWriter = new StringWriter();
        JsonGenerator jgen = new JsonFactory().createGenerator(jsonWriter);
        var serializer = new DocumentFlowSerializer();
        serializer.serializeNoarkEntity(documentFlow, new LinksNoarkObject(), jgen);
        jgen.close();
        return jsonWriter.toString();
    }
}
