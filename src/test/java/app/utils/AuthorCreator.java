package app.utils;

import app.domain.noark5.secondary.Author;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.secondary.AuthorSerializer;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;
import java.io.StringWriter;

import static app.utils.TestConstants.AUTHOR_TEST;
import static app.utils.TestConstants.AUTHOR_TEST_UPDATED;

public final class AuthorCreator {

    /**
     * Create a default Author for testing purposes
     *
     * @return a Author with all values set
     */
    public static Author createAuthor() {
        Author author = new Author();
        author.setAuthor(AUTHOR_TEST);
        return author;
    }

    public static String createAuthorAsJSON() throws IOException {
        return createAuthorAsJSON(createAuthor());
    }

    public static String createUpdatedAuthorAsJSON() throws IOException {
        Author author = createAuthor();
        author.setAuthor(AUTHOR_TEST_UPDATED);
        return createAuthorAsJSON(author);
    }

    public static String createAuthorAsJSON(Author author) throws IOException {
        StringWriter jsonWriter = new StringWriter();
        JsonGenerator jgen = new JsonFactory().createGenerator(jsonWriter);
        AuthorSerializer authorSerializer = new AuthorSerializer();
        authorSerializer.serializeNoarkEntity(author, new LinksNoarkObject(), jgen);
        jgen.close();
        return jsonWriter.toString();
    }
}
