package app.utils;

import app.domain.noark5.secondary.CrossReference;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.secondary.CrossReferenceSerializer;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;
import java.io.StringWriter;
import java.util.UUID;


public final class CrossReferenceCreator {

    public static String createCrossReferenceAsJSON(UUID fromUUID, UUID toUUID, String referenceType)
            throws IOException {
        StringWriter jsonWriter = new StringWriter();
        JsonGenerator jgen = new JsonFactory().createGenerator(jsonWriter);
        CrossReference crossReference = new CrossReference();
        crossReference.setFromSystemId(fromUUID);
        crossReference.setToSystemId(toUUID);
        crossReference.setReferenceType(referenceType);
        CrossReferenceSerializer serializer = new CrossReferenceSerializer();
        serializer.serializeNoarkEntity(crossReference, new LinksNoarkObject(), jgen);
        jgen.close();
        return jsonWriter.toString();
    }
}
