package app.webapp.testcontainers;

import app.webapp.exceptions.NikitaMisconfigurationException;
import com.jayway.jsonpath.JsonPath;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static app.utils.constants.HATEOASConstants.HREF;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

public class BaseTest {
    //extends BaseTestContainer {


    @Value("${server.servlet.context-path}")
    protected String contextPath;

    protected MockMvc mockMvc;

    public void setUp(WebApplicationContext webApplicationContext,
                      RestDocumentationContextProvider restDocumentation) {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .apply(documentationConfiguration(restDocumentation))
                .alwaysDo(document("{method-name}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())))
                .build();
    }


    protected void printDocumentation(ResultActions resultActions)
            throws Exception {
        resultActions.andDo(document("home",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())));
    }

    protected String getFirstSystemIDFromRequest(String url) {
        Pattern pattern = Pattern.compile(
                "([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f" +
                        "]{12})");
        Matcher matcher = pattern.matcher(url);
        if (matcher.find()) {
            return matcher.group(0);
        }
        return "";
    }

    public String getHref(String what, ResultActions resultActions)
            throws UnsupportedEncodingException {
        MockHttpServletResponse response = resultActions.andReturn()
                .getResponse();
        System.out.println(response.getContentAsString());
        String href = JsonPath.read(response.getContentAsString(),
                "$._links.['" + what + "']['" + HREF + "']");
        String[] split = href.split(contextPath);
        if (split != null && split.length == 2) {
            return contextPath + split[1];
        }
        throw new NikitaMisconfigurationException("Unable to split " + href);
    }

}
