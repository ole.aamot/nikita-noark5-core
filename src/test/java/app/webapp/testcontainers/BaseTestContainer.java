package app.webapp.testcontainers;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.utility.DockerImageName;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@ActiveProfiles("mariadb")
public abstract class BaseTestContainer {

    static final MariaDBContainer<?> dbContainer;

    static {
        dbContainer =
                new MariaDBContainer<>(DockerImageName.parse("mariadb:10.3.6"))
                        .withDatabaseName("noark")
                        .withUsername("noarkuser")
                        .withPassword("noarkpassword")
                        .withReuse(true);
        dbContainer.start();
    }

    protected MockMvc mockMvc;

    @DynamicPropertySource
    static void registerResourceServerIssuerProperty(DynamicPropertyRegistry registry) {
        // Leave a value for resource server, otherwise testing bombs
        registry.add("spring.security.oauth2.resourceserver.jwt.issuer-uri", () -> "https://example.com/");
        registry.add("spring.datasource.url", dbContainer::getJdbcUrl);
        registry.add("spring.datasource.username", dbContainer::getUsername);
        registry.add("spring.datasource.password", dbContainer::getPassword);
        registry.add("spring.datasource.driver-class-name", dbContainer::getDriverClassName);
        registry.add("spring.liquibase.change-log=", () -> "classpath:db/changelog/db.changelog-master.xml");
        registry.add("docker.client.strategy", () -> "org.testcontainers.dockerclient.UnixSocketClientProviderStrategy");
        registry.add("testcontainers.reuse.enable", () -> "true");
    }

    public void initSetup() {
        dbContainer.isRunning();
    }
}
