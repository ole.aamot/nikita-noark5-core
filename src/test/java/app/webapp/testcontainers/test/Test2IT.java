package app.webapp.testcontainers.test;

import app.webapp.spring.security.NikitaUserDetailsService;
import app.webapp.testcontainers.BaseTestContainer;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static app.utils.DocumentDescriptionCreator.createDocumentDescriptionAsJSON;
import static app.utils.DocumentDescriptionValidator.validateDocumentDescription;
import static app.utils.DocumentDescriptionValidator.validateDocumentDescriptionTemplate;
import static app.utils.constants.Constants.NOARK5_V5_CONTENT_TYPE_JSON;
import static app.utils.constants.Constants.ROLE_RECORDS_MANAGER;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Internal testing currently suspended in the codebase.
 * <p>
 * Note: In the upgrade to Spring Boot 3 we experienced problems with the domain model. There is no problem with the
 * domain model when running spring boot as an application, but there seems to be a problem with the domain model
 * is used in testing. Typically, the problem is that Hibernate is not able to deal with JOINED inheritance as it is
 * specified in nikita, and prints a Wrong entity retrieved exception stating that it expects a File when it was given
 * a Record (We tried to retrieve a Record). We have tried using a discriminator column, forcing the use of a
 * discriminator column but in test mode we are not able to get the Nikita domain model
 * <p>
 * We need to finish this Spring Boot 3 branch and move forward. All tests that will not run are currently commented
 * out until we have time to fix it.
 */
//@SpringBootTest(webEnvironment = RANDOM_PORT)
//@AutoConfigureMockMvc
////@TestInstance(TestInstance.Lifecycle.PER_CLASS)
//@ActiveProfiles("mariadb")
//@Transactional
public class Test2IT
        extends BaseTestContainer {

    @Autowired
    protected NikitaUserDetailsService userDetailsService;
    @Value("${server.servlet.context-path}")
    protected String contextPath;
    protected SimpleGrantedAuthority roleRecordsManager = new SimpleGrantedAuthority(ROLE_RECORDS_MANAGER);
    protected MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;

    @DynamicPropertySource
    static void registerResourceServerIssuerProperty(DynamicPropertyRegistry registry) {
        // Leave a value for resource server, otherwise testing bombs
        registry.add("spring.security.oauth2.resourceserver.jwt.issuer-uri", () -> "https://example.com/");
/*        registry.add("spring.datasource.url", () -> "jdbc:h2:mem:n5DemoDb");
        registry.add("spring.datasource.username", () -> "sa");
        registry.add("spring.datasource.password", () -> "");
        registry.add("spring.datasource.driver-class-name", () -> "org.h2.Driver");
        registry.add("spring.jpa.properties.hbm2ddl.auto", () -> "create-drop");
 */
    }

    @BeforeEach
    public void setUp() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    /**
     * Check that it is possible to create a DocumentDescription
     *
     * @throws Exception Serialising or validation exception
     */
    //@Test
    @Sql("/db-tests/basic_structure.sql")
    //@WithMockUser(username = "test_XXuser_admin@example.com", roles = {"RECORDS_MANAGER"})
    public void addAuthorWhenCreatingDocumentDescription() throws Exception {
        // First get template to create / POST DocumentDescription
        String urlNewDocumentDescription = "/noark5v5/api/arkivstruktur/registrering" +
                "/dc600862-3298-4ec0-8541-3e51fb900054/ny-dokumentbeskrivelse";

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlNewDocumentDescription)
                .with(jwt().authorities(roleRecordsManager))
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions.andExpect(status().isOk());
        validateDocumentDescriptionTemplate(resultActions);

        // Create a JSON object to POST
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(urlNewDocumentDescription)
                .with(jwt().authorities(roleRecordsManager))
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(createDocumentDescriptionAsJSON()));

        resultActions.andExpect(status().isCreated());
        validateDocumentDescription(resultActions);
    }

    //@Test
    //@WithMockUser(username = "test_user_admin@example.com", roles = {"RECORDS_MANAGER"})
    public void addAuthorWhenCreatingDocumentDescription1() throws Exception {
        // First get template to create / POST DocumentDescription
        String urlNewDocumentDescription = "/noark5v5/api/arkivstruktur/registrering" +
                "/dc600862-3298-4ec0-8541-3e51fb900054/ny-dokumentbeskrivelse";

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get(urlNewDocumentDescription)
                .with(jwt().authorities(roleRecordsManager))
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON));

        resultActions.andExpect(status().isOk());
        validateDocumentDescriptionTemplate(resultActions);

        // Create a JSON object to POST
        resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post(urlNewDocumentDescription)
                .with(jwt().authorities(roleRecordsManager))
                .contextPath(contextPath)
                .accept(NOARK5_V5_CONTENT_TYPE_JSON)
                .contentType(NOARK5_V5_CONTENT_TYPE_JSON)
                .content(createDocumentDescriptionAsJSON()));

        resultActions.andExpect(status().isCreated());
        validateDocumentDescription(resultActions);

    }
}

    /*
    //@Test
    @Sql({"/db-tests/basic_structure.sql"})
    @WithMockUser(username="test_user_admin@example.com", roles={"RECORDS_MANAGER"})
    void contextLoads() {

        FondsCreator f1 = new FondsCreator();
        f1.setFondsCreatorId("1111");
        f1.setFondsCreatorName("1111");

        this.repository.saveAll(List.of(f1, f1));

        ResponseEntity<ArrayNode> result = this.testRestTemplate.getForEntity("/noark5v5/api/arkivstruktur/arkivskaper", ArrayNode.class);
        assertEquals(200, result.getStatusCode().value());
        assertTrue(result.getBody().isArray());
        assertEquals(2, result.getBody().size());
    }
    */
