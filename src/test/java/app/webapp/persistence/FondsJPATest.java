package app.webapp.persistence;

import app.domain.noark5.Fonds;
import app.domain.repository.noark5.v5.IFondsRepository;
import jakarta.persistence.EntityManager;
import jakarta.validation.ConstraintViolationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.UUID;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * FondsRepository tests
 * <p>
 * Created by tsodring on 2019/04/08
 */
@ExtendWith(SpringExtension.class)
@DataJpaTest
public class FondsJPATest {

    @Autowired
    IFondsRepository fondsRepository;
    @Autowired
    EntityManager entityManager;
    private String fonds1SystemId;
    // Does not exist
    private String fonds2SystemId;

    @BeforeEach
    public void setUp() {
        fonds1SystemId = "3318a63f-11a7-4ec9-8bf1-4144b7f281cf";
        fonds2SystemId = "0c5e864c-3269-4e01-9430-17d55291dae7";
    }

    /**
     * When an invalid fonds is created (missing title, @NotNull in @Entity), a
     * ConstraintViolationException should be thrown.
     */
    @Test
    public void whenInvalidEntityIsCreated_thenDataException() {
        Assertions.assertThrows(ConstraintViolationException.class, () -> {
            Fonds fonds = new Fonds();
            fonds = fondsRepository.save(fonds);
            entityManager.flush();
            assertThat(fondsRepository.save(fonds)).isNotNull();
        });
    }

    /**
     * When querying the database for a fonds that does not exist, the
     * result should be a null value.
     */
    @Test
    public void whenGetNonExistingFondsEntity_thenIsNull() {
        assertThat(fondsRepository.findBySystemId(
                UUID.fromString(fonds2SystemId))).isNull();
    }

    /**
     * When the database has a number of fonds persisted (2), it should be
     * possible to retrieve a count of persisted fonds.
     */
    @Sql("/db-tests/basic_structure.sql")
    @WithMockUser("test_user_admin@example.com")
    public void whenInitializedByDbUnit_thenCheckSize() {
        // assertThat(fondsRepository.findAll().size()).isEqualTo(2);
    }

    /**
     * When the database has a fonds persisted, it should be possible to
     * retrieve the persisted fonds.
     */
    @Sql("/db-tests/basic_structure.sql")
    @WithMockUser("test_user_admin@example.com")
    public void whenInitializedByDbUnit_thenFindBySystemId() {
        Fonds fonds = fondsRepository.findBySystemId(
                UUID.fromString(fonds1SystemId));
        assertThat(fonds).isNotNull();
    }

    /**
     * When the database has a fonds persisted, it should be possible to
     * delete the persisted fonds.
     */
    @Sql("/db-tests/basic_structure.sql")
    @WithMockUser("test_user_admin@example.com")
    public void whenInitializedByDbUnit_thenDeleteBySystemId() {
        Fonds fonds = fondsRepository.findBySystemId(
                UUID.fromString(fonds1SystemId));
        fondsRepository.delete(fonds);
    }

    /**
     * An attempt to delete a non-existent fonds should result in an exception.
     * It probably does not make sense to have such a test, as fonds is null.
     */
    @Test
    public void whenDeleteNonExistingFondsEntity_thenIsNull() {
        Assertions.assertThrows(InvalidDataAccessApiUsageException.class, () -> {
            Fonds fonds = fondsRepository.findBySystemId(
                    UUID.fromString(fonds2SystemId));
            fondsRepository.delete(fonds);
        });
    }
}
