package app.webapp.explore.mapsid;

import jakarta.persistence.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.annotations.Parameter;

import java.io.Serializable;
import java.util.UUID;

import static app.utils.constants.N5ResourceMappings.SYSTEM_ID_ENG;
import static jakarta.persistence.CascadeType.ALL;
import static jakarta.persistence.FetchType.LAZY;
import static java.sql.Types.VARCHAR;

@Entity
public class Parent
        implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator",
            parameters = {@Parameter(
                    name = "uuid_gen_strategy_class",
                    value = "org.hibernate.id.uuid.CustomVersionOneStrategy")})
    @Column(name = SYSTEM_ID_ENG, unique = true, updatable = false, nullable =
            false)
    @JdbcTypeCode(VARCHAR)
    private UUID code;

    @OneToOne(mappedBy = "parent", fetch = LAZY, cascade = ALL)
    private Child child;

    public UUID getCode() {
        return code;
    }

    public void setCode(UUID code) {
        this.code = code;
    }

    public Child getChild() {
        return child;
    }

    public void setChild(Child child) {
        this.child = child;
        child.setParent(this);
    }

    @Override
    public String toString() {
        return "Parent{" +
                "code='" + code + '\'' +
                '}';
    }
}
