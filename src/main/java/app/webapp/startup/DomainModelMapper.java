package app.webapp.startup;

import app.domain.annotation.ANationalIdentifier;
import jakarta.persistence.Entity;
import org.reflections.Reflections;
import org.springframework.stereotype.Component;

import static app.utils.CommonUtils.FileUtils.addClassToMap;
import static app.utils.CommonUtils.FileUtils.addClassToNatIdentMap;

@Component
public class DomainModelMapper {
    public void mapDomainModelClasses() {
        Reflections ref = new Reflections("app.domain.noark5");
        for (Class<?> klass : ref.getTypesAnnotatedWith(Entity.class)) {
            String simpleName = klass.getSimpleName();
            addClassToMap(simpleName, klass);
            if (klass.isAnnotationPresent(ANationalIdentifier.class)) {
                addClassToNatIdentMap(klass,
                        klass.getAnnotation(ANationalIdentifier.class));
            }
        }
    }
}
