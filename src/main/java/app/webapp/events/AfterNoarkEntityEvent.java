package app.webapp.events;

import app.domain.interfaces.entities.ISystemId;
import jakarta.validation.constraints.NotNull;

/**
 * Base class that can be used to identify various CRUD events that can occur
 * in nikita.
 */
public class AfterNoarkEntityEvent {

    ISystemId entity;

    public AfterNoarkEntityEvent(@NotNull ISystemId entity) {
        this.entity = entity;
    }

    public String toString() {
        return entity.toString();
    }

    public ISystemId getEntity() {
        return entity;
    }
}
