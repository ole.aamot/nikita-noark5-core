package app.webapp.events;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.admin.ChangeLog;
import jakarta.validation.constraints.NotNull;

/**
 * Base class that can be used to identify UPDATE events that can occur in
 * nikita.
 */
public class AfterNoarkEntityUpdatedEvent
        extends AfterNoarkEntityEvent {

    private final ChangeLog changeLog;

    public AfterNoarkEntityUpdatedEvent(@NotNull ChangeLog changeLog,
                                        @NotNull ISystemId entity) {
        super(entity);
        this.changeLog = changeLog;
    }

    public ChangeLog getChangeLog() {
        return changeLog;
    }
}
