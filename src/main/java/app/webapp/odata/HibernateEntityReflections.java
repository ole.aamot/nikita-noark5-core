package app.webapp.odata;

import app.webapp.exceptions.NikitaMalformedInputDataException;
import app.webapp.exceptions.NikitaMisconfigurationException;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Optional;

import static app.utils.CommonUtils.entityMap;
import static app.utils.CommonUtils.natIdentMap;
import static app.utils.constants.Constants.CANNOT_FIND_FOREIGN_KEYS;
import static java.lang.String.format;

public class HibernateEntityReflections {
    private static final Logger logger =
            LoggerFactory.getLogger(HibernateEntityReflections.class);

    protected String getForeignKeySubUnit(String className) {
        for (Field field : FieldUtils.getAllFields(getClassFromClassName(className))) {
            // If the field is not a potential match for what we are looking
            // for simply continue
            if (!field.getName().contains(className)) {
                continue;
            }
            if (field.getAnnotation(ManyToOne.class) != null) {
                String type = field.getType().getSimpleName();
                if (className.equals(type)) {
                    return field.getName();
                }
            }
        }
        throw new NikitaMisconfigurationException(format(CANNOT_FIND_FOREIGN_KEYS, className, className));
    }

    protected Class getClassFromClassName(String fromClassName) {
        Class<?> klass = Optional.ofNullable(entityMap.get(fromClassName))
                .orElseThrow(() -> new NikitaMalformedInputDataException(
                        "Unsupported Entity class: " + fromClassName));
        return klass;
    }

    protected String getForeignKey(String fromClassName, String toClassName) {
        // This is a specific case that we have to deal with and perhaps is
        // applicable to others. The relationship is actually between File and
        // Record rather than CaseFile and RegistryEntry
        if (toClassName.equalsIgnoreCase("CaseFile") &&
                fromClassName.equalsIgnoreCase("RegistryEntry")) {
            fromClassName = "RecordEntity";
            toClassName = "File";
        }
        if (toClassName.equalsIgnoreCase("CaseFile") &&
                fromClassName.equalsIgnoreCase("RecordNote")) {
            fromClassName = "RecordEntity";
            toClassName = "File";
        }
        Class<?> klass = getClassFromClassName(fromClassName);
        String foreignKeyName = findKeyInFields(klass, fromClassName, toClassName);
        if (foreignKeyName.isEmpty() && null != natIdentMap.get(toClassName)) {
            foreignKeyName = "referenceNationalIdentifier";
        }
        return foreignKeyName;
    }

    protected String findKeyInFields(Class<?> klass, String fromClassName, String toClassName) {
        for (Field field : FieldUtils.getAllFields(klass)) {
            String variableName = field.getName();
            // If the field is not a potential match for what we are looking
            // for simply continue
            if (!variableName.contains(toClassName)) {
                continue;
            }
            if (field.getAnnotation(ManyToOne.class) != null) {
                String type = field.getType().getSimpleName();
                if (toClassName.equals(type)) {
                    return field.getName();
                }
            }

            if (field.getAnnotation(OneToMany.class) != null ||
                    field.getAnnotation(ManyToMany.class) != null) {
                for (java.lang.Class iface : field.getType().getInterfaces()) {
                    if (iface.isAssignableFrom(Collection.class)) {
                        Method method;
                        try {
                            method = klass.getMethod("get" +
                                    variableName.substring(0, 1).toUpperCase() +
                                    variableName.substring(1), null);
                            if (null == method) {
                                method = klass.getMethod("getReference" +
                                        variableName.substring(0, 1)
                                                .toUpperCase() +
                                        variableName.substring(1), null);
                            }
                        } catch (NoSuchMethodException e) {
                            String error = klass.getName() + " has no foreign" +
                                    " key for " + toClassName;
                            logger.error(error);
                            throw new NikitaMisconfigurationException(error);
                        }

                        Type genericReturnType = method.getGenericReturnType();
                        if (genericReturnType instanceof ParameterizedType) {
                            for (Type type :
                                    ((ParameterizedType) genericReturnType)
                                            .getActualTypeArguments()) {
                                java.lang.Class returnType = (java.lang.Class) type;
                                if (returnType.getSimpleName()
                                        .equals(toClassName)) {
                                    return variableName;
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NikitaMisconfigurationException(format(CANNOT_FIND_FOREIGN_KEYS, fromClassName, toClassName));
    }


    public String getPrimaryKey(String className) {

        Class klass = Optional.ofNullable(entityMap.get(className))
                .orElseThrow(() -> new NikitaMalformedInputDataException(
                        "Unsupported Noark class: " + className));

        Field[] allFields = FieldUtils.getAllFields(klass);
        for (Field field : allFields) {
            if (field.getAnnotation(Id.class) != null) {
                return field.getName();
            }
        }
        return "";
    }
}
