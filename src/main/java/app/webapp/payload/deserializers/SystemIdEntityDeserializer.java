package app.webapp.payload.deserializers;

import app.domain.interfaces.IBSM;
import app.domain.interfaces.entities.ICreate;
import app.domain.interfaces.entities.ILastModified;
import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.bsm.BSM;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;
import static java.util.UUID.fromString;

public class SystemIdEntityDeserializer<R extends SystemIdEntity>
        extends NoarkDeserializer {

    public void deserializeNoarkSystemIdEntity(
            ISystemId noarkSystemIdEntity,
            ObjectNode objectNode) {
        // Deserialize systemId
        JsonNode currentNode = objectNode.get(SYSTEM_ID);
        if (null != currentNode) {
            noarkSystemIdEntity.setSystemId(
                    fromString(currentNode.textValue()));
            objectNode.remove(SYSTEM_ID);
        }
    }

    public void deserializeSystemIdEntity(
            ISystemId systemIdEntity, ObjectNode objectNode,
            StringBuilder errors) {
        deserializeNoarkSystemIdEntity(
                systemIdEntity, objectNode);
        deserializeNoarkCreateEntity(
                systemIdEntity, objectNode, errors);
        deserializeNoarkLastModifiedEntity(
                systemIdEntity, objectNode, errors);
    }

    public void deserializeNoarkCreateEntity(ICreate noarkCreateEntity,
                                             ObjectNode objectNode, StringBuilder errors) {
        // Deserialize createdDate
        noarkCreateEntity.setCreatedDate(deserializeDateTime(CREATED_DATE, objectNode, errors));
        // Deserialize createdBy
        JsonNode currentNode = objectNode.get(CREATED_BY);
        if (null != currentNode) {
            noarkCreateEntity.setCreatedBy(currentNode.textValue());
            objectNode.remove(CREATED_BY);
        }
    }

    /*
     * Deserialize to make sure GET + modify + PUT work.
     */
    public void deserializeNoarkLastModifiedEntity(
            ILastModified noarkEntity,
            ObjectNode objectNode, StringBuilder errors) {
        JsonNode currentNode = objectNode.get(LAST_MODIFIED_DATE);
        if (null != currentNode) {
            noarkEntity.setLastModifiedDate(deserializeDateTime(
                    LAST_MODIFIED_DATE, objectNode, errors));
            objectNode.remove(LAST_MODIFIED_DATE);
        }
        currentNode = objectNode.get(LAST_MODIFIED_BY);
        if (null != currentNode) {
            noarkEntity.setLastModifiedBy(currentNode.textValue());
            objectNode.remove(LAST_MODIFIED_BY);
        }
    }

    /**
     * Deserialize businessSpecificMetadata (virksomhetsspesifikkeMetadata)
     *
     * @param objectNode Payload object
     * @param bsmObject  Instance of an Object that implements the IBSM interface
     * @throws IOException if something goes wrong. Should not occur.
     */
    public void deserializeBSM(ObjectNode objectNode, IBSM bsmObject) throws IOException {
        JsonNode currentNode = objectNode.get(BSM_DEF);
        if (null != currentNode) {
            BSM base = mapper.readValue(currentNode.traverse(), BSM.class);
            bsmObject.addReferenceBSMBase(base.getReferenceBSMBase());
            objectNode.remove(BSM_DEF);
        }
    }
}
