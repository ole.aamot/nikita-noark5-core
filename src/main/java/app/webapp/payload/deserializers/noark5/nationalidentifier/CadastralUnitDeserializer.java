package app.webapp.payload.deserializers.noark5.nationalidentifier;

import app.domain.noark5.nationalidentifier.CadastralUnit;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public class CadastralUnitDeserializer
        extends SystemIdEntityDeserializer<CadastralUnit> {
    @Override
    public CadastralUnit deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               CadastralUnit cadastralUnit = new CadastralUnit();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize systemID
        deserializeNoarkSystemIdEntity(cadastralUnit, objectNode);
        // Deserialize kommunenummer
        JsonNode currentNode = objectNode.get(MUNICIPALITY_NUMBER);
        if (null != currentNode) {
            cadastralUnit.setMunicipalityNumber(currentNode.textValue());
            objectNode.remove(MUNICIPALITY_NUMBER);
        }
        // Deserialize gaardsnummer
        cadastralUnit.setHoldingNumber(deserializeInteger(HOLDING_NUMBER, objectNode, errors, true));
        // Deserialize bruksnummer
        cadastralUnit.setSubHoldingNumber(deserializeInteger(SUB_HOLDING_NUMBER, objectNode, errors, true));
        // Deserialize festenummer
        cadastralUnit.setLeaseNumber(deserializeInteger(LEASE_NUMBER, objectNode, errors, false));
        // Deserialize seksjonsnummer
        cadastralUnit.setSectionNumber(deserializeInteger(SECTION_NUMBER, objectNode, errors, false));
        check_payload_at_end(errors, objectNode);
        return cadastralUnit;
    }

    @Override
    protected String getType() {
        return CADASTRAL_UNIT;
    }
}