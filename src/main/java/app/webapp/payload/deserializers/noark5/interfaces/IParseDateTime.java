package app.webapp.payload.deserializers.noark5.interfaces;

import app.webapp.exceptions.NikitaMalformedInputDataException;

import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

import static java.time.OffsetDateTime.parse;
import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE;
import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;
import static java.time.temporal.ChronoField.HOUR_OF_DAY;
import static java.time.temporal.ChronoField.MINUTE_OF_HOUR;

public interface IParseDateTime {

    default OffsetDateTime deserializeDateTime(String dateTime) {
        return parse(dateTime, ISO_OFFSET_DATE_TIME);
    }

    default OffsetDateTime deserializeDate(String date) {
        DateTimeFormatter dateFormatter;
        if (date.endsWith("Z")) {
            dateFormatter = new DateTimeFormatterBuilder().appendPattern(
                            "yyyy-MM-ddX")
                    .parseDefaulting(HOUR_OF_DAY, 0)
                    .toFormatter();
        } else {
            dateFormatter = new DateTimeFormatterBuilder()
                    .append(ISO_OFFSET_DATE)
                    // default values for hour and minute
                    .parseDefaulting(HOUR_OF_DAY, 0)
                    .parseDefaulting(MINUTE_OF_HOUR, 0)
                    .toFormatter();
        }
        ZonedDateTime zonedDateTime =
                ZonedDateTime.parse(date, dateFormatter);
        if (null == zonedDateTime) {
            String error = "Could not deserialize " + date;
            throw new NikitaMalformedInputDataException(error);
        }
        return zonedDateTime.toOffsetDateTime();
    }
}
