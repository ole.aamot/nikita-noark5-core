package app.webapp.payload.deserializers.noark5.interfaces;

import app.domain.interfaces.entities.IClassificationSystemEntity;
import app.domain.noark5.metadata.ClassificationType;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static app.utils.constants.N5ResourceMappings.CLASSIFICATION_SYSTEM_TYPE;

public interface IClassificationSystemTypeDeserializer extends IDeserializer {

    default void deserializeClassificationSystemType(IClassificationSystemEntity classificationSystem,
                                                     ObjectNode objectNode, StringBuilder errors) {
        ClassificationType classificationType = (ClassificationType) deserializeMetadataValue(objectNode,
                CLASSIFICATION_SYSTEM_TYPE, new ClassificationType(), errors, false);
        classificationSystem.setClassificationType(classificationType);
    }
}
