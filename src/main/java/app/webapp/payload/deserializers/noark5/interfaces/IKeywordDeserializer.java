package app.webapp.payload.deserializers.noark5.interfaces;

import app.domain.interfaces.IKeyword;
import app.domain.noark5.secondary.Keyword;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static app.utils.constants.N5ResourceMappings.KEYWORD;

public interface IKeywordDeserializer
        extends IDeserializer {

    default void deserializeKeyword(IKeyword keywordEntity,
                                    ObjectNode objectNode) {
        // Deserialize keyword
        JsonNode currentNode = objectNode.get(KEYWORD);
        if (null != currentNode) {
            if (currentNode.isArray()) {
                currentNode.iterator();
                for (JsonNode node : currentNode) {
                    String keywordText = node.textValue();
                    Keyword keyword = new Keyword();
                    keyword.setKeyword(keywordText);
                    keywordEntity.addKeyword(keyword);
                }
            }
            objectNode.remove(KEYWORD);
        }
    }
}
