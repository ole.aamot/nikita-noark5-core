package app.webapp.payload.deserializers.noark5.nationalidentifier;

import app.domain.noark5.metadata.CoordinateSystem;
import app.domain.noark5.nationalidentifier.Position;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public class PositionDeserializer
        extends SystemIdEntityDeserializer<Position> {
    @Override
    public Position deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               Position position = new Position();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize systemID
        deserializeNoarkSystemIdEntity(position, objectNode);
        // Deserialize koordinatsystem
        CoordinateSystem coordinateSystem = (CoordinateSystem) deserializeMetadataValue(objectNode, COORDINATE_SYSTEM,
                new CoordinateSystem(), errors, true);
        position.setCoordinateSystem(coordinateSystem);
        // Deserialize
        JsonNode currentNode = objectNode.get(X);
        if (null != currentNode) {
            position.setX(currentNode.doubleValue());
            objectNode.remove(X);
        }
        // Deserialize
        currentNode = objectNode.get(Y);
        if (null != currentNode) {
            position.setY(currentNode.doubleValue());
            objectNode.remove(Y);
        }
        // Deserialize
        currentNode = objectNode.get(Z);
        if (null != currentNode) {
            position.setZ(currentNode.doubleValue());
            objectNode.remove(Z);
        }
        check_payload_at_end(errors, objectNode);
        return position;
    }

    @Override
    protected String getType() {
        return POSITION;

    }
}