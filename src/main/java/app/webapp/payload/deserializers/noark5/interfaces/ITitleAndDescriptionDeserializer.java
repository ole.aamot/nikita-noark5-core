package app.webapp.payload.deserializers.noark5.interfaces;

import app.domain.interfaces.entities.ITitleDescription;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static app.utils.constants.N5ResourceMappings.DESCRIPTION;
import static app.utils.constants.N5ResourceMappings.TITLE;

public interface ITitleAndDescriptionDeserializer
        extends IDeserializer {

    default void deserializeNoarkTitleDescriptionEntity(ITitleDescription titleDescriptionEntity,
                                                        ObjectNode objectNode, StringBuilder errors) {
        // Deserialize title
        JsonNode currentNode = objectNode.get(TITLE);
        if (null != currentNode) {
            titleDescriptionEntity.setTitle(currentNode.textValue());
            rejectIfEmptyOrWhitespace(titleDescriptionEntity.getTitle(), TITLE);
            objectNode.remove(TITLE);
        } else {
            errors.append(TITLE + " is missing. ");
        }
        // Deserialize description
        currentNode = objectNode.get(DESCRIPTION);
        if (null != currentNode) {
            titleDescriptionEntity.setDescription(currentNode.textValue());
            objectNode.remove(DESCRIPTION);
        }
    }
}
