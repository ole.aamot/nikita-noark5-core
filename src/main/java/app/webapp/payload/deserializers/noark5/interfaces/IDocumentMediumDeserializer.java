package app.webapp.payload.deserializers.noark5.interfaces;

import app.domain.interfaces.IDocumentMedium;
import app.domain.noark5.metadata.DocumentMedium;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static app.utils.constants.N5ResourceMappings.DOCUMENT_MEDIUM;

public interface IDocumentMediumDeserializer
        extends IDeserializer {

    default void deserializeDocumentMedium(IDocumentMedium documentMediumEntity, ObjectNode objectNode, StringBuilder errors) {
        // Deserialize documentMedium
        DocumentMedium documentMedium = (DocumentMedium)
                deserializeMetadataValue(objectNode,
                        DOCUMENT_MEDIUM,
                        new DocumentMedium(),
                        errors, false);
        documentMediumEntity.setDocumentMedium(documentMedium);
    }

}
