package app.webapp.payload.deserializers.noark5;

import app.domain.noark5.Fonds;
import app.domain.noark5.metadata.FondsStatus;
import app.webapp.payload.deserializers.NoarkGeneralEntityDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.IDocumentMediumDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.IStorageLocationDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.FONDS;
import static app.utils.constants.N5ResourceMappings.FONDS_STATUS;

/**
 * Deserialize an incoming Fonds JSON object.
 */
public class FondsDeserializer
        extends NoarkGeneralEntityDeserializer<Fonds>
        implements IDocumentMediumDeserializer, IStorageLocationDeserializer {

    @Override
    public Fonds deserialize(JsonParser jsonParser, DeserializationContext dc)
            throws IOException {
               Fonds fonds = new Fonds();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize general properties
        deserializeNoarkGeneralEntity(fonds, objectNode, errors);
        deserializeDocumentMedium(fonds, objectNode, errors);
        deserializeStorageLocation(fonds, objectNode);
        // Deserialize seriesStatus
        FondsStatus fondsStatus = (FondsStatus) deserializeMetadataValue(
                objectNode, FONDS_STATUS, new FondsStatus(), errors, false);
        fonds.setFondsStatus(fondsStatus);
        check_payload_at_end(errors, objectNode);
        return fonds;
    }

    @Override
    protected String getType() {
        return FONDS;
    }
}