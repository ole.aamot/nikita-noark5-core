package app.webapp.payload.deserializers.noark5.nationalidentifier;

import app.domain.noark5.nationalidentifier.SocialSecurityNumber;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.SOCIAL_SECURITY_NUMBER;

public class SocialSecurityNumberDeserializer
        extends SystemIdEntityDeserializer<SocialSecurityNumber> {

    @Override
    public SocialSecurityNumber deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               SocialSecurityNumber socialSecurityNumber = new SocialSecurityNumber();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize systemID
        deserializeNoarkSystemIdEntity(socialSecurityNumber, objectNode);
        // Deserialize foedselsnummer
        JsonNode currentNode = objectNode.get(SOCIAL_SECURITY_NUMBER);
        if (null != currentNode) {
            socialSecurityNumber.setSocialSecurityNumber(currentNode.textValue());
            objectNode.remove(SOCIAL_SECURITY_NUMBER);
        }
        check_payload_at_end(errors, objectNode);
        return socialSecurityNumber;
    }

    @Override
    protected String getType() {
        return SOCIAL_SECURITY_NUMBER;
    }
}