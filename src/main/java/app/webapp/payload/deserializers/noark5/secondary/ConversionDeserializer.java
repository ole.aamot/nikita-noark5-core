package app.webapp.payload.deserializers.noark5.secondary;

import app.domain.noark5.metadata.Format;
import app.domain.noark5.secondary.Conversion;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public class ConversionDeserializer
        extends SystemIdEntityDeserializer<Conversion> {
    @Override
    public Conversion deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               Conversion conversion = new Conversion();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize systemID
        deserializeNoarkSystemIdEntity(conversion, objectNode);
        deserializeNoarkCreateEntity(conversion, objectNode, errors);
        // Deserialize konvertertDato
        conversion.setConvertedDate(deserializeDateTime(
                CONVERTED_DATE, objectNode, errors));
        // Deserialize konvertertAv
        JsonNode currentNode = objectNode.get(CONVERTED_BY);
        if (null != currentNode) {
            conversion.setConvertedBy(currentNode.textValue());
            objectNode.remove(CONVERTED_BY);
        }
        // Deserialize konvertertFraFormat
        Format fromFormat = (Format)
                deserializeMetadataValue(
                        objectNode,
                        CONVERTED_FROM_FORMAT,
                        new Format(),
                        errors, true);
        if (null != fromFormat.getCode()) {
            conversion.setConvertedFromFormat(fromFormat);
        }
        // Deserialize konvertertTilFormat
        Format toFormat = (Format)
                deserializeMetadataValue(
                        objectNode,
                        CONVERTED_TO_FORMAT,
                        new Format(),
                        errors, true);
        if (null != toFormat.getCode()) {
            conversion.setConverteLinksFormat(toFormat);
        }
        // Deserialize konverteringsverktoey
        currentNode = objectNode.get(CONVERSION_TOOL);
        if (null != currentNode) {
            conversion.setConversionTool(currentNode.textValue());
            objectNode.remove(CONVERSION_TOOL);
        }
        // Deserialize konverteringskommentar
        currentNode = objectNode.get(CONVERSION_COMMENT);
        if (null != currentNode) {
            conversion.setConversionComment(currentNode.textValue());
            objectNode.remove(CONVERSION_COMMENT);
        }
        check_payload_at_end(errors, objectNode);
        return conversion;
    }

    @Override
    protected String getType() {
        return CONVERSION;
    }
}