package app.webapp.payload.deserializers.noark5.admin;

import app.domain.noark5.admin.ReadLog;
import app.webapp.payload.deserializers.noark5.EventLogDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.READ_LOG;

public class ReadLogDeserializer
        extends EventLogDeserializer<ReadLog> {
    @Override
    public ReadLog deserialize(JsonParser jsonParser, DeserializationContext dc)
            throws IOException {
        ReadLog readLog = new ReadLog();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        deserializeEventLog(readLog, objectNode, errors);
        check_payload_at_end(errors, objectNode);
        return readLog;
    }

    @Override
    protected String getType() {
        return READ_LOG;
    }
}