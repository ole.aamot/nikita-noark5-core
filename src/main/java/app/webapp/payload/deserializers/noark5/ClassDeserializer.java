package app.webapp.payload.deserializers.noark5;

import app.domain.noark5.Class;
import app.webapp.payload.deserializers.NoarkGeneralEntityDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.IClassifiedDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.IDisposalDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.IKeywordDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.IScreeningDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.CLASS;
import static app.utils.constants.N5ResourceMappings.CLASS_ID;

/**
 * Deserialize an incoming Class JSON object.
 */
public class ClassDeserializer
        extends NoarkGeneralEntityDeserializer<Class>
        implements IClassifiedDeserializer, IDisposalDeserializer, IKeywordDeserializer, IScreeningDeserializer {

    @Override
    public Class deserialize(JsonParser jsonParser, DeserializationContext dc)
            throws IOException {
               Class klass = new Class();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize general properties
        deserializeNoarkGeneralEntity(klass, objectNode, errors);
        // Deserialize classId
        JsonNode currentNode = objectNode.get(CLASS_ID);
        if (null != currentNode) {
            klass.setClassId(currentNode.textValue());
            objectNode.remove(CLASS_ID);
        }
        deserializeKeyword(klass, objectNode);
        klass.setReferenceDisposal(
                deserializeDisposal(
                        objectNode, errors));
        // klasse.gradering is only XSD, not in the version 1.0 API
        // specification.  See
        // https://github.com/arkivverket/noark5-tjenestegrensesnitt-standard/pull/241
        klass.setReferenceClassified(
                deserializeClassified(
                        objectNode, errors));
        klass.setReferenceScreening(deserializeScreening(objectNode, errors));
        check_payload_at_end(errors, objectNode);
        return klass;
    }

    @Override
    protected String getType() {
        return CLASS;
    }
}