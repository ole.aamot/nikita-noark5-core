package app.webapp.payload.deserializers.noark5.secondary;

import app.domain.noark5.secondary.Author;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.AUTHOR;

public class AuthorDeserializer
        extends SystemIdEntityDeserializer<Author> {
    @Override
    public Author deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               Author author = new Author();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize systemID
        deserializeNoarkSystemIdEntity(author, objectNode);
        deserializeNoarkCreateEntity(author, objectNode, errors);
        // Deserialize forfatter
        JsonNode currentNode = objectNode.get(AUTHOR);
        if (null != currentNode) {
            author.setAuthor(currentNode.textValue());
            objectNode.remove(AUTHOR);
        }
        check_payload_at_end(errors, objectNode);
        return author;
    }

    @Override
    protected String getType() {
        return AUTHOR;
    }
}