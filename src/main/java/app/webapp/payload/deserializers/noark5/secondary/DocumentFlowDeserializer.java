package app.webapp.payload.deserializers.noark5.secondary;

import app.domain.noark5.metadata.FlowStatus;
import app.domain.noark5.secondary.DocumentFlow;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.UUID;

import static app.utils.constants.N5ResourceMappings.*;

public class DocumentFlowDeserializer
        extends SystemIdEntityDeserializer<DocumentFlow> {

    @Override
    public DocumentFlow deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               DocumentFlow documentFlow = new DocumentFlow();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize systemID
        deserializeNoarkSystemIdEntity(documentFlow, objectNode);
        deserializeNoarkCreateEntity(documentFlow, objectNode, errors);
        // Deserialize flytTil
        JsonNode currentNode = objectNode.get(DOCUMENT_FLOW_FLOW_TO);
        if (null != currentNode) {
            documentFlow.setFlowTo(currentNode.textValue());
            objectNode.remove(DOCUMENT_FLOW_FLOW_TO);
        } else {
            errors.append(DOCUMENT_FLOW_FLOW_TO + " is missing. ");
        }
        // Deserialize referanseFlytTil
        currentNode = objectNode.get(DOCUMENT_FLOW_REFERENCE_FLOW_TO);
        if (null != currentNode) {
            documentFlow.setReferenceFlowToSystemID
                    (UUID.fromString(currentNode.textValue()));
            objectNode.remove(DOCUMENT_FLOW_REFERENCE_FLOW_TO);
        }
        // Deserialize flytFra
        currentNode = objectNode.get(DOCUMENT_FLOW_FLOW_FROM);
        if (null != currentNode) {
            documentFlow.setFlowFrom(currentNode.textValue());
            objectNode.remove(DOCUMENT_FLOW_FLOW_FROM);
        } else {
            errors.append(DOCUMENT_FLOW_FLOW_FROM + " is missing. ");
        }
        // Deserialize referanseFlytFra
        currentNode = objectNode.get(DOCUMENT_FLOW_REFERENCE_FLOW_FROM);
        if (null != currentNode) {
            documentFlow.setReferenceFlowFromSystemID
                    (UUID.fromString(currentNode.textValue()));
            objectNode.remove(DOCUMENT_FLOW_REFERENCE_FLOW_FROM);
        }
        // Deserialize flytMottattDato
        documentFlow.setFlowReceivedDate(deserializeDateTime(
                DOCUMENT_FLOW_FLOW_RECEIVED_DATE, objectNode, errors));
        // Deserialize flytSendtDato
        documentFlow.setFlowSentDate(deserializeDateTime(
                DOCUMENT_FLOW_FLOW_SENT_DATE, objectNode, errors));
        // Deserialize flytStatus
        FlowStatus entity = (FlowStatus)
                deserializeMetadataValue(objectNode,
                        DOCUMENT_FLOW_FLOW_STATUS,
                        new FlowStatus(),
                        errors, true);
        documentFlow.setFlowStatus(entity);
        // Deserialize flytMerknad
        currentNode = objectNode.get(DOCUMENT_FLOW_FLOW_COMMENT);
        if (null != currentNode) {
            documentFlow.setFlowComment(currentNode.textValue());
            objectNode.remove(DOCUMENT_FLOW_FLOW_COMMENT);
        }
        check_payload_at_end(errors, objectNode);
        return documentFlow;
    }

    @Override
    protected String getType() {
        return DOCUMENT_FLOW;
    }
}