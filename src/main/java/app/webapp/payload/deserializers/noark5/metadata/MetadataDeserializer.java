package app.webapp.payload.deserializers.noark5.metadata;

import app.domain.noark5.metadata.Metadata;
import app.webapp.payload.deserializers.NoarkDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import static app.utils.constants.ErrorMessagesConstants.MALFORMED_PAYLOAD;
import static app.utils.constants.HATEOASConstants.LINKS;
import static app.utils.constants.N5ResourceMappings.CODE;
import static app.utils.constants.N5ResourceMappings.CODE_NAME;

public class MetadataDeserializer
        extends NoarkDeserializer<Metadata> {

    private static final Logger logger =
            LoggerFactory.getLogger(MetadataDeserializer.class);


    @Override
    public Metadata deserialize(JsonParser jsonParser,
                                DeserializationContext dc)
            throws IOException {
               Metadata metadata = new Metadata();
        ObjectNode objectNode = mapper.readTree(jsonParser);

        JsonNode node = objectNode.get(CODE);
        if (null != node) {
            metadata.setCode(node.textValue());
            objectNode.remove(CODE);
        } else {
            errors.append(CODE + " is missing. ");
        }
        node = objectNode.get(CODE_NAME);
        if (null != node) {
            metadata.setCodeName(node.textValue());
            objectNode.remove(CODE_NAME);
        }
        node = objectNode.get(LINKS);
        if (null != node) {
            logger.info("Payload contains " + LINKS + ". " +
                    "This value is being ignored.");
            objectNode.remove(LINKS);
        }
        if (objectNode.size() != 0) {
            errors.append(String.format(MALFORMED_PAYLOAD,
                    "metadata", checkNodeObjectEmpty(objectNode)));

        }

        check_payload_at_end(errors, objectNode);

        return metadata;
    }
}
