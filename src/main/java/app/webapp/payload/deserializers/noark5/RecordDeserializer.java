package app.webapp.payload.deserializers.noark5;

import app.domain.noark5.RecordEntity;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.*;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Deserialize an incoming Record JSON object.
 */
public class RecordDeserializer
        extends SystemIdEntityDeserializer<RecordEntity>
        implements IClassifiedDeserializer, IDisposalDeserializer, IDocumentMediumDeserializer, IKeywordDeserializer,
        IScreeningDeserializer {
    @Override
    public RecordEntity deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               RecordEntity record = new RecordEntity();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize general properties
        deserializeSystemIdEntity(record, objectNode, errors);
        // Deserialize archivedBy
        JsonNode currentNode = objectNode.get(RECORD_ARCHIVED_BY);
        if (currentNode != null) {
            record.setArchivedBy(currentNode.textValue());
            objectNode.remove(RECORD_ARCHIVED_BY);
        }
        // Deserialize archivedDate
        record.setArchivedDate(deserializeDateTime(RECORD_ARCHIVED_DATE, objectNode, errors));

        // Deserialize general Record properties
        // Deserialize recordId
        currentNode = objectNode.get(RECORD_ID);
        if (null != currentNode) {
            record.setRecordId(currentNode.textValue());
            objectNode.remove(RECORD_ID);
        }
        // Deserialize title (not using nikita.utils to preserve order)
        currentNode = objectNode.get(TITLE);
        if (null != currentNode) {
            record.setTitle(currentNode.textValue());
            objectNode.remove(TITLE);
        }
        // Deserialize  publicTitle
        currentNode = objectNode.get(FILE_PUBLIC_TITLE);
        if (null != currentNode) {
            record.setPublicTitle(currentNode.textValue());
            objectNode.remove(FILE_PUBLIC_TITLE);
        }
        // Deserialize description
        currentNode = objectNode.get(DESCRIPTION);
        if (null != currentNode) {
            record.setDescription(currentNode.textValue());
            objectNode.remove(DESCRIPTION);
        }
        deserializeDocumentMedium(record, objectNode, errors);
        deserializeKeyword(record, objectNode);
        record.setReferenceClassified(deserializeClassified(objectNode, errors));
        record.setReferenceDisposal(deserializeDisposal(objectNode, errors));
        record.setReferenceScreening(deserializeScreening(objectNode, errors));
        deserializeBSM(objectNode, record);
        check_payload_at_end(errors, objectNode);
        return record;
    }

    @Override
    protected String getType() {
        return RECORD;
    }
}