package app.webapp.payload.deserializers.noark5;

import app.domain.noark5.DocumentObject;
import app.domain.noark5.EventLog;
import app.domain.noark5.SystemIdEntity;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.UUID;

import static app.utils.constants.N5ResourceMappings.*;

public class EventLogDeserializer<C extends SystemIdEntity>
        extends SystemIdEntityDeserializer<DocumentObject> {

    @Override
    public EventLog deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
        EventLog eventLog = new EventLog();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        deserializeEventLog(eventLog, objectNode, errors);
        check_payload_at_end(errors, objectNode);
        return eventLog;
    }

    public void deserializeEventLog(EventLog eventLog, ObjectNode objectNode, StringBuilder errors) {
        deserializeSystemIdEntity(eventLog, objectNode, errors);
        UUID referenceArchiveUnit = deserializeUUID(REFERENCE_ARCHIVE_UNIT, objectNode, errors, false);
        if (null != referenceArchiveUnit) {
            eventLog.setReferenceArchiveUnitSystemId(referenceArchiveUnit);
        }
        eventLog.setEventDate(deserializeDateTime(EVENT_DATE, objectNode, errors));
        JsonNode currentNode = objectNode.get(EVENT_INITIATED_BY);
        if (null != currentNode) {
            eventLog.setEventInitiator(currentNode.textValue());
            objectNode.remove(EVENT_INITIATED_BY);
        }
        currentNode = objectNode.get(REFERENCE_INITIATED_BY);
        if (null != currentNode) {
            eventLog.setEventInitiatorSystemId(UUID.fromString(currentNode.textValue()));
            objectNode.remove(REFERENCE_INITIATED_BY);
        }
    }

    @Override
    protected String getType() {
        return EVENT_LOG;
    }
}