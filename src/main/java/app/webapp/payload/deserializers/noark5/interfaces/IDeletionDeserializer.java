package app.webapp.payload.deserializers.noark5.interfaces;

import app.domain.interfaces.entities.IDeletionEntity;
import app.domain.noark5.metadata.DeletionType;
import app.domain.noark5.secondary.Deletion;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static app.utils.constants.N5ResourceMappings.*;

public interface IDeletionDeserializer
        extends IDeserializer {
    default Deletion deserializeDeletion(ObjectNode objectNode, StringBuilder errors) {
        Deletion deletion = null;
        JsonNode currentNode = objectNode.get(DELETION);
        if (null != currentNode
                && !currentNode.equals(NullNode.getInstance())) {
            deletion = new Deletion();
            ObjectNode deletionObjectNode = currentNode.deepCopy();
            deserializeDeletionEntity(deletion, deletionObjectNode,
                    errors);
            if (0 == deletionObjectNode.size()) {
                objectNode.remove(DELETION);
            }
        } else if (null != currentNode) { // Remove NullNode
            objectNode.remove(DELETION);
        }
        return deletion;
    }

    default void deserializeDeletionEntity(IDeletionEntity deletionEntity, ObjectNode objectNode, StringBuilder errors) {
        // Deserialize deletionBy
        JsonNode currentNode = objectNode.get(DELETION_BY);
        if (null != currentNode
                && !currentNode.equals(NullNode.getInstance())) {
            deletionEntity.setDeletionBy(currentNode.textValue());
            objectNode.remove(DELETION_BY);
        } else {
            errors.append(DELETION_BY + " is missing. ");
        }
        // TODO referanseSlettetAv
        // Deserialize deletionType
        DeletionType entity = (DeletionType)
                deserializeMetadataValue(objectNode,
                        DELETION_TYPE,
                        new DeletionType(),
                        errors, true);
        deletionEntity.setDeletionType(entity);
        // Deserialize deletionDate
        deletionEntity.setDeletionDate(deserializeDateTime(DELETION_DATE, objectNode, errors));
    }

}
