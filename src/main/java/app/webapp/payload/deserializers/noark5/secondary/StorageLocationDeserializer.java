package app.webapp.payload.deserializers.noark5.secondary;

import app.domain.noark5.secondary.StorageLocation;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.STORAGE_LOCATION;

public class StorageLocationDeserializer
        extends SystemIdEntityDeserializer<StorageLocation> {
    @Override
    public StorageLocation deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               StorageLocation storageLocation = new StorageLocation();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize systemID
        deserializeNoarkSystemIdEntity(storageLocation, objectNode);
        deserializeNoarkCreateEntity(storageLocation, objectNode, errors);
        // Deserialize oppbevaringssted
        JsonNode currentNode = objectNode.get(STORAGE_LOCATION);
        if (null != currentNode) {
            storageLocation.setStorageLocation(currentNode.textValue());
            objectNode.remove(STORAGE_LOCATION);
        }
        check_payload_at_end(errors, objectNode);
        return storageLocation;
    }

    @Override
    protected String getType() {
        return STORAGE_LOCATION;
    }
}