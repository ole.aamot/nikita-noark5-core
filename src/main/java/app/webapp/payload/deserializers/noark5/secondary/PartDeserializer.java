package app.webapp.payload.deserializers.noark5.secondary;

import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.secondary.Part;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.IPartDeserializer;

/**
 * This is just a base class for the others to use
 */
public class PartDeserializer<P extends SystemIdEntity>
        extends SystemIdEntityDeserializer<Part>
        implements IPartDeserializer {
}