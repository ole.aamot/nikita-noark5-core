package app.webapp.payload.deserializers.noark5;

import app.domain.noark5.ClassificationSystem;
import app.webapp.payload.deserializers.NoarkGeneralEntityDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.IClassificationSystemTypeDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.CLASSIFICATION_SYSTEM;

/**
 * Deserialize an incoming ClassificationSystem JSON object.
 */
public class ClassificationSystemDeserializer
        extends NoarkGeneralEntityDeserializer<ClassificationSystem>
        implements IClassificationSystemTypeDeserializer {

    @Override
    public ClassificationSystem deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               ClassificationSystem classificationSystem = new ClassificationSystem();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize general properties
        deserializeNoarkGeneralEntity(classificationSystem, objectNode, errors);
        deserializeClassificationSystemType(classificationSystem, objectNode, errors);
        check_payload_at_end(errors, objectNode);
        return classificationSystem;
    }

    @Override
    protected String getType() {
        return CLASSIFICATION_SYSTEM;
    }
}