package app.webapp.payload.deserializers.noark5.casehandling;

import app.domain.noark5.casehandling.secondary.CorrespondencePart;
import app.domain.noark5.casehandling.secondary.CorrespondencePartPerson;
import app.webapp.payload.deserializers.noark5.secondary.CorrespondencePartDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.CORRESPONDENCE_PART_PERSON;

/**
 * Deserialize an incoming CorrespondencePartPerson JSON object.
 */
public class CorrespondencePartPersonDeserializer<P extends CorrespondencePart>
        extends CorrespondencePartDeserializer<CorrespondencePart> {

    @Override
    public CorrespondencePartPerson deserialize(
            JsonParser jsonParser, DeserializationContext dc)
            throws IOException {
               CorrespondencePartPerson correspondencePart = new CorrespondencePartPerson();
        ObjectNode objectNode = mapper.readTree(jsonParser);

        deserializeNoarkSystemIdEntity(correspondencePart, objectNode);
        deserializeCorrespondencePartPersonEntity(correspondencePart, objectNode, errors);

        check_payload_at_end(errors, objectNode);
        return correspondencePart;
    }

    @Override
    protected String getType() {
        return CORRESPONDENCE_PART_PERSON;
    }
}