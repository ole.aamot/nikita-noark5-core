package app.webapp.payload.builder.noark5.secondary;

import app.domain.interfaces.entities.ISystemId;
import app.domain.interfaces.entities.secondary.IAuthorEntity;
import app.domain.noark5.secondary.Author;
import app.webapp.payload.builder.interfaces.secondary.IAuthorLinksBuilder;
import app.webapp.payload.builder.noark5.SystemIdLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.AUTHOR;

/**
 * Created by tsodring on 2020/01/06.
 * <p>
 * Used to add AuthorLinks links with Author specific information
 */
@Component("authorLinksBuilder")
public class AuthorLinksBuilder
        extends SystemIdLinksBuilder
        implements IAuthorLinksBuilder {

    public AuthorLinksBuilder() {
    }

    @Override
    public void addSelfLink(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject) {
        String selfhref = getOutgoingAddress() +
                HREF_BASE_FONDS_STRUCTURE + SLASH + AUTHOR + SLASH + entity.getSystemId();
        linksNoarkObject.addLink(entity,
                new Link(selfhref, getRelSelfLink()));
        linksNoarkObject.addLink(entity,
                new Link(selfhref, entity.getBaseRel()));
    }

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        Author author = (Author) entity;
        if (author.getForDocumentDescription()) {
            addDocumentDescription(author, linksNoarkObject);
        }
        if (author.getForRecord()) {
            addRecordEntity(author, linksNoarkObject);
        }
    }

    /**
     * Create a REL/HREF pair for the parent Record associated with the given
     * Author
     * <p>
     * "../api/arkivstruktur/registrering/1234"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/registrering/"
     *
     * @param author           Author
     * @param linksNoarkObject linksDocumentObject
     */
    @Override
    public void addRecordEntity(IAuthorEntity author,
                                ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(author,
                new Link(getOutgoingAddress() + HREF_BASE_RECORD + SLASH +
                        author.getReferenceRecordEntity().getSystemId(),
                        REL_FONDS_STRUCTURE_RECORD));
    }

    /**
     * Create a REL/HREF pair for the parent DocumentDescription associated
     * with the given Author
     * <p>
     * "../api/arkivstruktur/dokumentbeskrivelse/1234"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/dokumentbeskrivelse/"
     *
     * @param author           The Author object
     * @param linksNoarkObject linksDocumentObject
     */
    @Override
    public void addDocumentDescription(IAuthorEntity author,
                                       ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(author,
                new Link(getOutgoingAddress() +
                        HREF_BASE_DOCUMENT_DESCRIPTION + SLASH +
                        author.getReferenceDocumentDescription().getSystemId(),
                        REL_FONDS_STRUCTURE_DOCUMENT_DESCRIPTION));
    }
}
