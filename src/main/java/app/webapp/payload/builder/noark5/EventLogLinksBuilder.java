package app.webapp.payload.builder.noark5;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.EventLog;
import app.domain.noark5.admin.User;
import app.webapp.payload.builder.interfaces.IEventLogLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.EVENT_LOG;
import static app.utils.constants.N5ResourceMappings.EVENT_TYPE;
import static java.util.Objects.requireNonNull;

/*
 * Used to add EventLogLinks links with EventLog specific information
 */
@Component("eventLogLinksBuilder")
public class EventLogLinksBuilder
        extends SystemIdLinksBuilder
        implements IEventLogLinksBuilder {

    public EventLogLinksBuilder() {
    }

    @Override
    public void addSelfLink(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject) {
        String selfHref = getOutgoingAddress() +
                HREF_BASE_LOGGING + SLASH + EVENT_LOG + SLASH + entity.getSystemId();
        linksNoarkObject.addLink(entity,
                new Link(selfHref, getRelSelfLink()));
        linksNoarkObject.addLink(entity,
                new Link(selfHref, entity.getBaseRel()));
    }

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        EventLog eventLog = (EventLog) entity;
        //addReferenceArchiveUnitLink((EventLog) eventLog, linksNoarkObject);
        addEventType(eventLog, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate
            (ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        EventLog eventLog = (EventLog) entity;
        addEventType(eventLog, linksNoarkObject);
    }

    public void addEventType
            (EventLog eventLog, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(eventLog, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + EVENT_TYPE,
                REL_METADATA_EVENT_TYPE, true));
    }

    protected void addReferenceArchiveUnitLink
            (EventLog eventLog, ILinksNoarkObject linksNoarkObject) {
        if (null != eventLog) {
            String refentityhref = getRefentityhref(eventLog);
            linksNoarkObject.addLink
                    (eventLog, new Link(refentityhref, eventLog.getBaseRel()));
        }
        linksNoarkObject.addLink
                (eventLog, new Link(getOutgoingAddress() + requireNonNull(eventLog).getHrefReferenceArchiveUnit(),
                        REL_ARCHIVE_UNIT));
        User user = eventLog.getReferenceEventInitiator();
        linksNoarkObject.addLink
                (eventLog, new Link(getOutgoingAddress() + HATEOAS_API_PATH + SLASH + user.getFunctionalTypeName() +
                        SLASH + user.getBaseTypeName() + SLASH + user.getSystemIdAsString(),
                        user.getBaseRel()));
    }

    private String getRefentityhref(EventLog eventLog) {
        String relkey = eventLog.getBaseRel();
        String section = HREF_BASE_FONDS_STRUCTURE;
        // TODO Avoid hack to return correct href
        if (relkey.contains(NOARK_CASE_HANDLING_PATH)) {
            section = HREF_BASE_CASE_HANDLING;
        } else if (relkey.contains(NOARK_ADMINISTRATION_PATH)) {
            section = HREF_BASE_ADMIN;
        } else if (relkey.contains(NOARK_LOGGING_PATH)) {
            section = HREF_BASE_LOGGING;
        }
        return getOutgoingAddress()
                + section + SLASH + eventLog.getBaseTypeName()
                + SLASH + eventLog.getSystemId();
    }

    @Override
    public void addEventLogLinks(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        // All SystemID entities should contain a link to their EventLogs.
        // By overriding this method and doing nothing, we can avoid an EventLog links
        // object from being created.
    }
}
