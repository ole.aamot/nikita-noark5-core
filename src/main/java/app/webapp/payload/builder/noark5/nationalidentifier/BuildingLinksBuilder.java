package app.webapp.payload.builder.noark5.nationalidentifier;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.builder.interfaces.nationalidentifier.IBuildingLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import org.springframework.stereotype.Component;

/**
 * Created by tsodring
 */
@Component
public class BuildingLinksBuilder
        extends NationalIdentifierLinksBuilder
        implements IBuildingLinksBuilder {

    public BuildingLinksBuilder() {
    }

    @Override
    public void addEntityLinks(
            ISystemId entity, ILinksNoarkObject linksNoarkObject) {
    }
}
