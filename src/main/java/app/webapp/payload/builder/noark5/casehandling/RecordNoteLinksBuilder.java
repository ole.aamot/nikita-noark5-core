package app.webapp.payload.builder.noark5.casehandling;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.builder.interfaces.IRecordNoteLinksBuilder;
import app.webapp.payload.builder.noark5.RecordLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.DOCUMENT_FLOW;

/**
 * Created by tsodring on 29/05/19.
 * <p>
 * Used to add RecordNoteLinks links with RecordNote specific information
 */
@Component("recordNoteLinksBuilder")
public class RecordNoteLinksBuilder
        extends RecordLinksBuilder
        implements IRecordNoteLinksBuilder {

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        super.addEntityLinks(entity, linksNoarkObject);
        addDocumentFlow(entity, linksNoarkObject);
        addNewDocumentFlow(entity, linksNoarkObject);
    }

    /**
     * Create a REL/HREF pair for the DocumentFlow associated with the
     * given RecordNote.
     * <p>
     * "../api/arkivstruktur/arkivnotat/1234/dokumentflyt"
     * "https://rel.arkivverket.no/noark5/v5/api/sakarkiv/dokumentflyt/"
     *
     * @param entity           recordNote
     * @param linksNoarkObject linksRecordNote
     */
    @Override
    public void addDocumentFlow(ISystemId entity,
                                ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD_NOTE + SLASH + entity.getSystemIdAsString() + SLASH + DOCUMENT_FLOW,
                REL_CASE_HANDLING_DOCUMENT_FLOW));
    }

    /**
     * Create a REL/HREF pair to create a new DocumentFlow associated with the
     * given RecordNote.
     * <p>
     * "../api/arkivstruktur/arkivnotat/1234/ny-dokumentflyt"
     * "https://rel.arkivverket.no/noark5/v5/api/sakarkiv/ny-dokumentflyt/"
     *
     * @param entity           recordNote
     * @param linksNoarkObject linksRecordNote
     */
    @Override
    public void addNewDocumentFlow(ISystemId entity,
                                   ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity,
                new Link(getOutgoingAddress() +
                        HREF_BASE_RECORD_NOTE + SLASH + entity.getSystemIdAsString() + SLASH + NEW_DOCUMENT_FLOW,
                        REL_CASE_HANDLING_NEW_DOCUMENT_FLOW));
    }
}
