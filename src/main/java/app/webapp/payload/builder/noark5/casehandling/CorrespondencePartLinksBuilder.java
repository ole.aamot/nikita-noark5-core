package app.webapp.payload.builder.noark5.casehandling;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.casehandling.secondary.CorrespondencePart;
import app.webapp.payload.builder.interfaces.secondary.ICorrespondencePartLinksBuilder;
import app.webapp.payload.builder.noark5.SystemIdLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.CORRESPONDENCE_PART_TYPE;

/**
 * Created by tsodring on 2/6/17.
 * <p>
 * Used to add CorrespondencePartLinks links with CorrespondencePart
 * specific information
 **/
@Component("correspondencePartLinksBuilder")
public class CorrespondencePartLinksBuilder
        extends SystemIdLinksBuilder
        implements ICorrespondencePartLinksBuilder {

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        addRecordEntity(entity, linksNoarkObject);
        addCorrespondencePartType(entity, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate(
            ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        addCorrespondencePartType(entity, linksNoarkObject);
    }

    @Override
    public void addRecordEntity(INoarkEntity entity,
                                ILinksNoarkObject linksNoarkObject) {
        CorrespondencePart correspondencePart = (CorrespondencePart) entity;
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + correspondencePart.getReferenceRecordEntity().getSystemId(),
                REL_FONDS_STRUCTURE_RECORD));
    }

    @Override
    public void addCorrespondencePartType(
            INoarkEntity entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + CORRESPONDENCE_PART_TYPE,
                REL_METADATA_CORRESPONDENCE_PART_TYPE, false));
    }
}
