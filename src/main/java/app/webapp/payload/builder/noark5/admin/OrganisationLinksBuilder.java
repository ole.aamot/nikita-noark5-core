package app.webapp.payload.builder.noark5.admin;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.builder.interfaces.admin.IOrganisationLinksBuilder;
import app.webapp.payload.builder.noark5.SystemIdLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.USER;

/**
 * Used to add OrganisationLinks links with Organisation
 * specific information
 */
@Component("organisationLinksBuilder")
public class OrganisationLinksBuilder
        extends SystemIdLinksBuilder
        implements IOrganisationLinksBuilder {

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        addUser(entity, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnCreate(ISystemId entity,
                                       ILinksNoarkObject linksNoarkObject) {
        addEntityLinks(entity, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnRead(ISystemId entity,
                                     ILinksNoarkObject linksNoarkObject) {
        addEntityLinks(entity, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate(
            ISystemId entity,
            ILinksNoarkObject linksNoarkObject) {
        super.addEntityLinksOnTemplate(entity, linksNoarkObject);
        addEntityLinks(entity, linksNoarkObject);
    }

    public void addUser(INoarkEntity entity,
                        ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_ADMIN + SLASH + USER,
                REL_ADMIN_USER, true));
    }
}
