package app.webapp.payload.builder.noark5.metadata;

import app.domain.interfaces.entities.IMetadataEntity;
import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.payload.builder.interfaces.metadata.IMetadataLinksBuilder;
import app.webapp.payload.builder.noark5.LinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import java.util.List;

import static app.utils.constants.Constants.*;

/**
 * Used to add links for metadata entities with specific information
 */
@Component()
public class MetadataLinksBuilder
        extends LinksBuilder
        implements IMetadataLinksBuilder {

    @Override
    public void addSelfLink(IMetadataEntity entity,
                            ILinksNoarkObject linksNoarkObject) {

        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HATEOAS_API_PATH + SLASH + entity.getFunctionalTypeName() +
                SLASH + entity.getBaseTypeName() + SLASH + entity.getCode(), getRelSelfLink()));
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HATEOAS_API_PATH + SLASH + entity.getFunctionalTypeName() +
                SLASH + entity.getBaseTypeName() + SLASH + entity.getCode(), entity.getBaseRel()));
    }

    @Override
    public void addLinks(ILinksNoarkObject linksNoarkObject) {
        Iterable<IMetadataEntity> entities = (List<IMetadataEntity>)
                (List) linksNoarkObject.getList();
        for (IMetadataEntity entity : entities) {
            addSelfLink(entity, linksNoarkObject);
            addEntityLinks(entity, linksNoarkObject);
        }
        // If linksNoarkObject is a list add a self link.
        // { "entity": [], "_links": [] }
        if (!linksNoarkObject.isSingleEntity()) {
            String url = getRequestPathAndQueryString();
            Link selfLink = new Link(url, getRelSelfLink(), false);
            linksNoarkObject.addSelfLink(selfLink);
        }
    }

    @Override
    public void addEntityLinks(
            IMetadataEntity entity, ILinksNoarkObject linksNoarkObject) {
        addCode(entity, linksNoarkObject);
    }

    @Override
    public void addLinksOnCreate(ILinksNoarkObject linksNoarkObject) {
        addLinks(linksNoarkObject);
    }

    @Override
    public void addLinksOnRead(ILinksNoarkObject linksNoarkObject) {
        addLinks(linksNoarkObject);
    }

    @Override
    public void addLinksOnUpdate(ILinksNoarkObject linksNoarkObject) {
        addLinks(linksNoarkObject);
    }

    @Override
    public void addLinksOnDelete(ILinksNoarkObject linksNoarkObject) {
        addLinks(linksNoarkObject);
    }

    @Override
    public void addLinksOnTemplate(ILinksNoarkObject linksNoarkObject) {
        Iterable<IMetadataEntity> entities =
                (List<IMetadataEntity>) (List) linksNoarkObject.getList();
        for (IMetadataEntity entity : entities) {
            addEntityLinksOnTemplate(entity, linksNoarkObject);
        }
    }

    // Subclass should handle this, empty links otherwise!
    @Override
    public void addEntityLinksOnCreate(IMetadataEntity entity,
                                       ILinksNoarkObject linksNoarkObject) {
        addEntityLinks(entity, linksNoarkObject);
    }

    // Subclass should handle this, empty links otherwise!
    @Override
    public void addEntityLinksOnTemplate(
            IMetadataEntity entity, ILinksNoarkObject linksNoarkObject) {
    }

    // Subclass should handle this, empty links otherwise!
    @Override
    public void addEntityLinksOnRead(IMetadataEntity entity,
                                     ILinksNoarkObject linksNoarkObject) {
        addEntityLinks(entity, linksNoarkObject);
    }

    @Override
    public void addCode(
            INoarkEntity entity, ILinksNoarkObject linksNoarkObject) {
        String code = ((IMetadataEntity) entity).getCode();
        // When generating a templated/efault code, code is null
        if (null != code) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_METADATA + SLASH + entity.getBaseTypeName() + SLASH +
                    code,
                    REL_METADATA + entity.getBaseTypeName() + SLASH));
        }
    }

    @Override
    public void addNewCode(
            INoarkEntity entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + NEW + DASH +
                entity.getBaseTypeName(), NOARK_BASE_REL + NEW +
                DASH + entity.getBaseTypeName() + SLASH));
    }
}
