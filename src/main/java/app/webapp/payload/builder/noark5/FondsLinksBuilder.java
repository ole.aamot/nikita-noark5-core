package app.webapp.payload.builder.noark5;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.Fonds;
import app.webapp.payload.builder.interfaces.IFondsLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;

/**
 * Used to add FondsLinks links with Fonds specific information
 */
@Component
public class FondsLinksBuilder
        extends SystemIdLinksBuilder
        implements IFondsLinksBuilder {

    public FondsLinksBuilder() {
        super();
    }

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        // link for object identity
        addFonds(entity, linksNoarkObject);
        // links for child entities
        addSeries(entity, linksNoarkObject);
        addNewSeries(entity, linksNoarkObject);
        addSubFonds(entity, linksNoarkObject);
        addNewSubFonds(entity, linksNoarkObject);
        // links for parent entities
        addParentFonds(entity, linksNoarkObject);
        addFondsCreator(entity, linksNoarkObject);
        addNewFondsCreator(entity, linksNoarkObject);
        // links for secondary entities
        // links for metadata entities
        addDocumentMedium(entity, linksNoarkObject);
        addFondsStatus(entity, linksNoarkObject);
        addNewStorageLocation(entity, linksNoarkObject);
        addStorageLocation(entity, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnCreate(ISystemId entity,
                                       ILinksNoarkObject linksNoarkObject) {
        addEntityLinks(entity, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnRead(ISystemId entity,
                                     ILinksNoarkObject linksNoarkObject) {
        addEntityLinks(entity, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate(ISystemId entity,
                                         ILinksNoarkObject linksNoarkObject) {
        addDocumentMedium(entity, linksNoarkObject);
        addFondsStatus(entity, linksNoarkObject);
    }

    /**
     * Create a REL/HREF pair for the list of FondsCreator associated with the
     * given Fonds. Checks if the Fonds has any FondsCreator associated with
     * it.
     * <p>
     * "../api/arkivstruktur/arkiv/1234/arkivskaper"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/arkivskaper/"
     *
     * @param entity           fonds
     * @param linksNoarkObject linksFonds
     */
    @Override
    public void addFondsCreator(ISystemId entity,
                                ILinksNoarkObject linksNoarkObject) {
        if (getFonds(entity).getReferenceFondsCreator().size() > 0) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_FONDS + SLASH + entity.getSystemIdAsString() + SLASH + FONDS_CREATOR,
                    REL_FONDS_STRUCTURE_FONDS_CREATOR, true));
        }
    }

    /**
     * Create a REL/HREF pair for the list of Series associated with the
     * given Fonds. Checks if the Fonds has any Series associated with it.
     * <p>
     * "../api/arkivstruktur/arkiv/1234/arkivdel"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/arkivdel/"
     *
     * @param entity           fonds
     * @param linksNoarkObject linksFonds
     */
    @Override
    public void addSeries(ISystemId entity,
                          ILinksNoarkObject linksNoarkObject) {
        if (getFonds(entity).getReferenceSeries() != null) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_FONDS + SLASH + entity.getSystemIdAsString() + SLASH + SERIES,
                    REL_FONDS_STRUCTURE_SERIES, true));
        }
    }

    /**
     * Create a REL/HREF pair for the given Fonds associated. This is the
     * equivalent to the self rel, but in addition identifies the object type.
     * <p>
     * "../api/arkivstruktur/arkiv/1234"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/arkiv/"
     *
     * @param entity             fonds
     * @param linksNoarkObject linksFonds
     */
    @Override
    public void addFonds(ISystemId entity,
                         ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FONDS + SLASH + entity.getSystemIdAsString(),
                REL_FONDS_STRUCTURE_FONDS));
    }

    /**
     * Create a REL/HREF pair for the list of sub (fonds) associated with the
     * given Fonds. Checks if the Fonds is actually associated with any sub
     * fonds.
     * <p>
     * "../api/arkivstruktur/arkiv/1234/underarkiv"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/underarkiv/"
     *
     * @param entity             fonds
     * @param linksNoarkObject linksFonds
     */
    @Override
    public void addSubFonds(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FONDS + SLASH + entity.getSystemIdAsString() + SLASH + SUB_FONDS,
                REL_FONDS_STRUCTURE_SUB_FONDS, true));
    }

    /**
     * Create a REL/HREF pair to create a new Series associated with the
     * given Fonds. This link should only be generated if the user is
     * authorised to create a Series.
     * <p>
     * "../api/arkivstruktur/arkiv/1234/ny-arkivdel"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-arkivdel/"
     *
     * @param entity             fonds
     * @param linksNoarkObject linksFonds
     */
    @Override
    public void addNewSeries(ISystemId entity,
                             ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FONDS + SLASH + entity.getSystemIdAsString() + SLASH + NEW_SERIES,
                REL_FONDS_STRUCTURE_NEW_SERIES));
    }


    @Override
    public void addStorageLocation(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        if (((Fonds) entity).getReferenceStorageLocation().size() > 0) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() +
                    SLASH + STORAGE_LOCATION,
                    REL_FONDS_STRUCTURE_STORAGE_LOCATION, true));
        }
    }

    @Override
    public void addNewStorageLocation(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() +
                SLASH + NEW_STORAGE_LOCATION,
                REL_FONDS_STRUCTURE_NEW_STORAGE_LOCATION, false));
    }

    /**
     * Create a REL/HREF pair to create a new FondsCreator associated with the
     * given Fonds. This link should only be generated if the user is
     * authorised to create a FondsCreator.
     * <p>
     * "../api/arkivstruktur/arkiv/1234/ny-arkivskaper"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-arkivskaper/"
     *
     * @param entity             fonds
     * @param linksNoarkObject linksFonds
     */
    @Override
    public void addNewFondsCreator(ISystemId entity,
                                   ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FONDS + SLASH + entity.getSystemIdAsString() + SLASH +
                NEW_FONDS_CREATOR, REL_FONDS_STRUCTURE_NEW_FONDS_CREATOR));
    }

    /**
     * Create a REL/HREF pair to create a new (sub) Fonds associated with
     * the given Fonds. This link should only be generated if the user is
     * authorised to create a sub fonds.
     * <p>
     * "../api/arkivstruktur/arkiv/1234/ny-arkiv"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-arkiv/"
     *
     * @param entity             fonds
     * @param linksNoarkObject linksFonds
     */
    @Override
    public void addNewSubFonds(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FONDS + SLASH + entity.getSystemIdAsString() + SLASH + NEW_FONDS,
                REL_FONDS_STRUCTURE_NEW_FONDS));
    }

    /**
     * Create a REL/HREF pair to get parent Fonds associated with
     * the given Fonds. Checks if parent fonds exists first.
     * <p>
     * "../api/arkivstruktur/arkiv/9856"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/forelderarkiv/"
     *
     * @param entity             fonds
     * @param linksNoarkObject linksFonds
     */
    @Override
    public void addParentFonds(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        Fonds fonds = getFonds(entity).getReferenceParentFonds();
        if (fonds != null) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_FONDS + SLASH + fonds.getSystemIdAsString(),
                    REL_FONDS_STRUCTURE_PARENT_FONDS));
        }
    }

    /**
     * Create a REL/HREF pair for the Series associated with the
     * given Fonds. Checks if the Fonds is actually associated with a
     * Series.
     * <p>
     * "../api/metadata/arkivstatus"
     * "https://rel.arkivverket.no/noark5/v5/api/metadata/arkivstatus/"
     *
     * @param entity             fonds
     * @param linksNoarkObject linksFonds
     */
    @Override
    public void addFondsStatus(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + FONDS_STATUS,
                REL_METADATA_FONDS_STATUS, true));
    }

    // Internal helper methods

    /**
     * Cast the ISystemId entity to a Fonds
     *
     * @param entity the Fonds
     * @return a Fonds object
     */
    private Fonds getFonds(@NotNull ISystemId entity) {
        return (Fonds) entity;
    }
}
