package app.webapp.payload.builder.noark5.nationalidentifier;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.builder.interfaces.nationalidentifier.IPositionLinksBuilder;
import app.webapp.payload.builder.noark5.SystemIdLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.COORDINATE_SYSTEM;

/**
 * Created by tsodring
 */
@Component
public class PositionLinksBuilder
        extends SystemIdLinksBuilder
        implements IPositionLinksBuilder {

    public PositionLinksBuilder() {
    }

    @Override
    public void addEntityLinks(
            ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        addCoordinateSystem(entity, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate(
            ISystemId entity,
            ILinksNoarkObject linksNoarkObject) {
        super.addEntityLinksOnTemplate(entity, linksNoarkObject);
        addCoordinateSystem(entity, linksNoarkObject);
    }

    public void addCoordinateSystem(ISystemId entity,
                                    ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + COORDINATE_SYSTEM,
                REL_METADATA_COORDINATE_SYSTEM, false));
    }

}
