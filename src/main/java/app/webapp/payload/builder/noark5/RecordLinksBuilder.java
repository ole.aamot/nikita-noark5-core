package app.webapp.payload.builder.noark5;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.RecordEntity;
import app.webapp.payload.builder.interfaces.IRecordLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;

/**
 * Used to add RecordLinks links with Record specific information
 */
@Component("recordLinksBuilder")
public class RecordLinksBuilder
        extends SystemIdLinksBuilder
        implements IRecordLinksBuilder {

    public RecordLinksBuilder() {
    }

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        // Add the child links
        addDocumentDescription(entity, linksNoarkObject);
        addNewDocumentDescription(entity, linksNoarkObject);
        // Add the parent links
        addReferenceSeries(entity, linksNoarkObject);
        addReferenceFile(entity, linksNoarkObject);
        addReferenceClass(entity, linksNoarkObject);
        // Add the secondary entity links
        // Part
        addPart(entity, linksNoarkObject);
        addNewPartPerson(entity, linksNoarkObject);
        addNewPartUnit(entity, linksNoarkObject);
        // CorrespondencePart
        addCorrespondencePart(entity, linksNoarkObject);
        addNewCorrespondencePartPerson(entity, linksNoarkObject);
        addNewCorrespondencePartUnit(entity, linksNoarkObject);
        addNewCorrespondencePartInternal(entity, linksNoarkObject);
        addStorageLocation(entity, linksNoarkObject);
        addNewStorageLocation(entity, linksNoarkObject);
        //addComment(entity, linksNoarkObject);
        addNewComment(entity, linksNoarkObject);
        addComment(entity, linksNoarkObject);
        addNewKeyword(entity, linksNoarkObject);
        addKeyword(entity, linksNoarkObject);
        //addCrossReference(entity, linksNoarkObject);
        addNewCrossReference(entity, linksNoarkObject);
        addClassifiedCodeMetadata(entity, linksNoarkObject);
        addDocumentMedium(entity, linksNoarkObject);
        addAccessRestriction(entity, linksNoarkObject);
        addScreeningDocument(entity, linksNoarkObject);
        addScreeningMetadata(entity, linksNoarkObject);
        addScreeningMetadataLocal(entity, linksNoarkObject);
        addNewScreeningMetadataLocal(entity, linksNoarkObject);

        // Add national identifiers
        addNewBuilding(entity, linksNoarkObject);
        addNewCadastralUnit(entity, linksNoarkObject);
        addNewDNumber(entity, linksNoarkObject);
        addNewPlan(entity, linksNoarkObject);
        addNewPosition(entity, linksNoarkObject);
        addNewSocialSecurityNumber(entity, linksNoarkObject);
        addNewUnit(entity, linksNoarkObject);
        addNationalIdentifier(entity, linksNoarkObject);
        addExpanLinksRegistryEntry(entity, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate(
            ISystemId entity,
            ILinksNoarkObject linksNoarkObject) {
        super.addEntityLinksOnTemplate(entity, linksNoarkObject);
        addClassifiedCodeMetadata(entity, linksNoarkObject);
        addDocumentMedium(entity, linksNoarkObject);
        addAccessRestriction(entity, linksNoarkObject);
        addScreeningDocument(entity, linksNoarkObject);
        addScreeningMetadata(entity, linksNoarkObject);
    }

    @Override
    public void addComment(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + COMMENT,
                REL_FONDS_STRUCTURE_COMMENT, false));
    }

    @Override
    public void addNewComment(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + NEW_COMMENT,
                REL_FONDS_STRUCTURE_NEW_COMMENT));
    }

    @Override
    public void addKeyword(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        if (((RecordEntity) entity).getReferenceKeyword().size() > 0) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() +
                    SLASH + KEYWORD, REL_FONDS_STRUCTURE_KEYWORD, true));
        }
    }

    @Override
    public void addNewKeyword(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + NEW_KEYWORD,
                REL_FONDS_STRUCTURE_NEW_KEYWORD, false));
    }

    @Override
    public void addStorageLocation(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        if (((RecordEntity) entity).getReferenceStorageLocation().size() > 0) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() +
                    SLASH + STORAGE_LOCATION,
                    REL_FONDS_STRUCTURE_STORAGE_LOCATION, true));
        }
    }

    @Override
    public void addNewStorageLocation(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() +
                SLASH + NEW_STORAGE_LOCATION,
                REL_FONDS_STRUCTURE_NEW_STORAGE_LOCATION, false));
    }

    @Override
    public void addCrossReference(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + CROSS_REFERENCE,
                REL_FONDS_STRUCTURE_CROSS_REFERENCE, false));
    }

    @Override
    public void addNewCrossReference(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + NEW_CROSS_REFERENCE,
                REL_FONDS_STRUCTURE_NEW_CROSS_REFERENCE, false));
    }


    /**
     * Create a REL/HREF pair for the parent Series associated with the given
     * Record. Checks if the Record is actually associated with a Series.
     * <p>
     * "../api/arkivstruktur/arkivdel/1234"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/arkivdel/"
     *
     * @param entity           record
     * @param linksNoarkObject linksRecord
     */
    @Override
    public void addReferenceSeries(ISystemId entity,
                                   ILinksNoarkObject linksNoarkObject) {
        RecordEntity record = getRecord(entity);
        if (record.getReferenceSeries() != null) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_SERIES + SLASH + record.getReferenceSeries().getSystemIdAsString(),
                    REL_FONDS_STRUCTURE_SERIES));
        }
    }

    /**
     * Create a REL/HREF pair for the parent File associated with the given
     * Record. Checks if the Record is actually associated with a File.
     * <p>
     * "../api/arkivstruktur/mappe/1234"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/mappe/"
     *
     * @param entity           record
     * @param linksNoarkObject linksRecord
     */
    @Override
    public void addReferenceFile(ISystemId entity,
                                 ILinksNoarkObject linksNoarkObject) {
        RecordEntity record = getRecord(entity);
        if (record.getReferenceFile() != null) {
            linksNoarkObject.addLink(entity,
                    new Link(getOutgoingAddress() + HREF_BASE_FILE + SLASH +
                            record.getReferenceFile().getSystemIdAsString(),
                            REL_FONDS_STRUCTURE_FILE));
        }
    }

    /**
     * Create a REL/HREF pair for the parent Class associated with the given
     * Record. Checks if the Record is actually associated with a Class.
     * <p>
     * "../api/arkivstruktur/klasse/1234"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/klasse/"
     *
     * @param entity             record
     * @param linksNoarkObject linksRecord
     */
    @Override
    public void addReferenceClass(ISystemId entity,
                                  ILinksNoarkObject linksNoarkObject) {
        RecordEntity record = getRecord(entity);
        if (record.getReferenceClass() != null) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_CLASS + SLASH + record.getReferenceClass().getSystemIdAsString(),
                    REL_FONDS_STRUCTURE_CLASS));
        }
    }

    @Override
    public void addNewDocumentDescription(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + NEW_DOCUMENT_DESCRIPTION,
                REL_FONDS_STRUCTURE_NEW_DOCUMENT_DESCRIPTION, false));
    }

    @Override
    public void addDocumentDescription(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + DOCUMENT_DESCRIPTION,
                REL_FONDS_STRUCTURE_DOCUMENT_DESCRIPTION, false));
    }

    @Override
    public void addNewReferenceSeries(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + REFERENCE_NEW_SERIES,
                REL_FONDS_STRUCTURE_NEW_REFERENCE_SERIES, false));
    }

    @Override
    public void addNewCorrespondencePartPerson(
            ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + NEW_CORRESPONDENCE_PART_PERSON,
                REL_FONDS_STRUCTURE_NEW_CORRESPONDENCE_PART_PERSON));
    }

    @Override
    public void addCorrespondencePart(
            ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + CORRESPONDENCE_PART,
                REL_FONDS_STRUCTURE_CORRESPONDENCE_PART, true));
    }

    @Override
    public void addNewCorrespondencePartUnit(
            ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + NEW_CORRESPONDENCE_PART_UNIT,
                REL_FONDS_STRUCTURE_NEW_CORRESPONDENCE_PART_UNIT));
    }

    @Override
    public void addPart(
            ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + PART,
                REL_FONDS_STRUCTURE_PART, true));
    }

    @Override
    public void addNewPartPerson(
            ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + NEW_PART_PERSON,
                REL_FONDS_STRUCTURE_NEW_PART_PERSON));
    }

    @Override
    public void addNewPartUnit(
            ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + NEW_PART_UNIT,
                REL_FONDS_STRUCTURE_NEW_PART_UNIT));
    }

    @Override
    public void addNewCorrespondencePartInternal(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        /*
        Temporary disabled as it causes problems for clients.
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_FONDS_STRUCTURE + SLASH + RECORD + SLASH + entity.getSystemIdAsString() + SLASH + NEW_CORRESPONDENCE_PART_INTERNAL,
                REL_FONDS_STRUCTURE_NEW_CORRESPONDENCE_PART_INTERNAL, false));

         */
    }

    /**
     * Create a REL/HREF pair to get the list of Author objects associated with
     * the given Record.
     * <p>
     * "../api/arkivstruktur/registrering/{systemID}/forfatter"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/forfatter/"
     *
     * @param entity             record
     * @param linksNoarkObject linksRecord
     */
    @Override
    public void addAuthor(ISystemId entity,
                          ILinksNoarkObject linksNoarkObject) {
        if (((RecordEntity) entity).getReferenceAuthor().size() > 0) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() +
                    SLASH + AUTHOR, REL_FONDS_STRUCTURE_AUTHOR, true));
        }
    }

    /**
     * Create a REL/HREF pair to create a new Author object associated with
     * the given Record.
     * <p>
     * "../api/arkivstruktur/registrering/{systemID}/ny-forfatter"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/ny-forfatter/"
     *
     * @param entity             record
     * @param linksNoarkObject linksRecord
     */
    @Override
    public void addNewAuthor(ISystemId entity,
                             ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() +
                SLASH + NEW_AUTHOR, REL_FONDS_STRUCTURE_NEW_AUTHOR));
    }

    @Override
    public void addNewBuilding(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + NEW_BUILDING,
                REL_FONDS_STRUCTURE_NEW_BUILDING));
    }

    @Override
    public void addNewCadastralUnit(ISystemId entity,
                                    ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + NEW_CADASTRAL_UNIT,
                REL_FONDS_STRUCTURE_NEW_CADASTRAL_UNIT));
    }

    @Override
    public void addNewDNumber(ISystemId entity,
                              ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + NEW_D_NUMBER,
                REL_FONDS_STRUCTURE_NEW_D_NUMBER));
    }

    @Override
    public void addNewPlan(ISystemId entity,
                           ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + NEW_PLAN,
                REL_FONDS_STRUCTURE_NEW_PLAN));
    }

    @Override
    public void addNewPosition(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + NEW_POSITION,
                REL_FONDS_STRUCTURE_NEW_POSITION));
    }

    @Override
    public void addNewSocialSecurityNumber(
            ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + NEW_SOCIAL_SECURITY_NUMBER,
                REL_FONDS_STRUCTURE_NEW_SOCIAL_SECURITY_NUMBER));
    }

    @Override
    public void addNewUnit(ISystemId entity,
                           ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + NEW_NI_UNIT,
                REL_FONDS_STRUCTURE_NEW_NI_UNIT));
    }

    @Override
    public void addNationalIdentifier(ISystemId entity,
                                      ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemIdAsString() + SLASH + NATIONAL_IDENTIFIER,
                REL_FONDS_STRUCTURE_NATIONAL_IDENTIFIER));
    }


    @Override
    public void addClassifiedCodeMetadata(ISystemId entity,
                                          ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + CLASSIFIED_CODE,
                REL_METADATA_CLASSIFIED_CODE));
    }

    @Override
    public void addAccessRestriction(ISystemId entity,
                                     ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + ACCESS_RESTRICTION,
                REL_METADATA_ACCESS_RESTRICTION, false));
    }

    @Override
    public void addScreeningDocument(ISystemId entity,
                                     ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + SCREENING_DOCUMENT,
                REL_METADATA_SCREENING_DOCUMENT, false));
    }

    @Override
    public void addScreeningMetadata(ISystemId entity,
                                     ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + SCREENING_METADATA,
                REL_METADATA_SCREENING_METADATA));
    }

    @Override
    public void addScreeningMetadataLocal(ISystemId entity,
                                          ILinksNoarkObject linksNoarkObject) {
        if (null != ((RecordEntity) entity).getReferenceScreening()) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_FONDS_STRUCTURE + SLASH + RECORD + SLASH +
                    entity.getSystemId() + SLASH + SCREENING_METADATA,
                    REL_FONDS_STRUCTURE_SCREENING_METADATA));
        }
    }

    @Override
    public void addNewScreeningMetadataLocal(ISystemId entity,
                                             ILinksNoarkObject linksNoarkObject) {
        if (null != ((RecordEntity) entity).getReferenceScreening()) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_FONDS_STRUCTURE + SLASH + RECORD + SLASH +
                    entity.getSystemId() + SLASH + NEW_SCREENING_METADATA,
                    REL_FONDS_STRUCTURE_NEW_SCREENING_METADATA));
        }
    }

    @Override
    public void addExpanLinksRegistryEntry(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_RECORD + SLASH + entity.getSystemId() + SLASH + RECORD_EXPAND_TO_REGISTRY_ENTRY,
                REL_FONDS_STRUCTURE_EXPAND_TO_REGISTRY_ENTRY, false));
    }

    /**
     * Cast the ISystemId entity to a Record
     *
     * @param entity the Record
     * @return a Record object
     */
    private RecordEntity getRecord(@NotNull ISystemId entity) {
        return (RecordEntity) entity;
    }
}
