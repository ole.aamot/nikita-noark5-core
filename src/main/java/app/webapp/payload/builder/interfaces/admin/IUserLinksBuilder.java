package app.webapp.payload.builder.interfaces.admin;

import app.webapp.payload.builder.interfaces.ILinksBuilder;

/**
 * Created by tsodring on 5/18/17.
 */
public interface IUserLinksBuilder
        extends ILinksBuilder {
    // Think it just has a self link
}
