package app.webapp.payload.builder.interfaces;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.links.ILinksNoarkObject;

/**
 * Created by tsodring on 29/05/19.
 * <p>
 * Describe Hateoas links handler
 */
public interface IRecordNoteLinksBuilder
        extends IRecordLinksBuilder {

    void addDocumentFlow(ISystemId entity,
                         ILinksNoarkObject linksNoarkObject);

    void addNewDocumentFlow(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject);
}
