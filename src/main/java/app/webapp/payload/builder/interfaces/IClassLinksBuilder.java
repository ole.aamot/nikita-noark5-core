package app.webapp.payload.builder.interfaces;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.links.ILinksNoarkObject;

/**
 * Created by tsodring on 2/6/17.
 * <p>
 * Describe Hateoas links handler for Class
 */
public interface IClassLinksBuilder extends ILinksBuilder {

    void addRegistration(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addNewRegistration(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addFile(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addNewFile(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addNewCaseFile(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addClassificationSystem(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addParentClass(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addSubClass(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addClass(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addNewClass(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addCrossReference(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addNewCrossReference(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addAccessRestriction(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addDisposalDecision(ISystemId entity, ILinksNoarkObject linksNoarkObject);

    void addKeyword(ISystemId entity,
                    ILinksNoarkObject linksNoarkObject);

    void addNewKeyword(ISystemId entity,
                       ILinksNoarkObject linksNoarkObject);

    void addClassifiedCodeMetadata(ISystemId entity,
                                   ILinksNoarkObject linksNoarkObject);

    void addScreeningDocument(ISystemId entity,
                              ILinksNoarkObject linksNoarkObject);

    void addScreeningMetadata(ISystemId entity,
                              ILinksNoarkObject linksNoarkObject);

    void addScreeningMetadataLocal(ISystemId entity,
                                   ILinksNoarkObject linksNoarkObject);

    void addNewScreeningMetadataLocal(ISystemId entity,
                                      ILinksNoarkObject linksNoarkObject);

}
