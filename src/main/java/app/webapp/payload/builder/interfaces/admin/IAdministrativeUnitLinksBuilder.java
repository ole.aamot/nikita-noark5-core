package app.webapp.payload.builder.interfaces.admin;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.builder.interfaces.ILinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;

/**
 * Created by tsodring on 5/18/17.
 */
public interface IAdministrativeUnitLinksBuilder
        extends ILinksBuilder {
    void addChildAdministrativeUnit(ISystemId entity,
                                    ILinksNoarkObject linksNoarkObject);
}
