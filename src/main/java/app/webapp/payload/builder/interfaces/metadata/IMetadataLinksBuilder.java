package app.webapp.payload.builder.interfaces.metadata;

import app.domain.interfaces.entities.IMetadataEntity;
import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.payload.builder.interfaces.ILinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;

/**
 * Created by tsodring on 4/3/17.
 */
public interface IMetadataLinksBuilder
        extends ILinksBuilder {

    void addSelfLink(IMetadataEntity entity,
                     ILinksNoarkObject linksNoarkObject);

    void addEntityLinks(IMetadataEntity entity,
                        ILinksNoarkObject linksNoarkObject);

    void addEntityLinksOnCreate(IMetadataEntity entity,
                                ILinksNoarkObject linksNoarkObject);

    void addEntityLinksOnTemplate(IMetadataEntity entity,
                                  ILinksNoarkObject linksNoarkObject);

    void addEntityLinksOnRead(IMetadataEntity entity,
                              ILinksNoarkObject linksNoarkObject);

    void addNewCode(INoarkEntity entity,
                    ILinksNoarkObject linksNoarkObject);

    void addCode(INoarkEntity entity, ILinksNoarkObject linksNoarkObject);
}
