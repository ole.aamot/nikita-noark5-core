package app.webapp.payload.builder.interfaces.secondary;

import app.domain.interfaces.entities.secondary.IDocumentFlowEntity;
import app.webapp.payload.builder.interfaces.ILinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;

public interface IDocumentFlowLinksBuilder
        extends ILinksBuilder {

    void addRecordEntityNote(IDocumentFlowEntity entity,
                             ILinksNoarkObject linksNoarkObject);

    void addRegistryEntry(IDocumentFlowEntity entity,
                          ILinksNoarkObject linksNoarkObject);

    void addFlowStatus(IDocumentFlowEntity documentFlow,
                       ILinksNoarkObject linksNoarkObject);
}
