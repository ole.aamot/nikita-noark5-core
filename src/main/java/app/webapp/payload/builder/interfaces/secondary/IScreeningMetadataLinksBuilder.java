package app.webapp.payload.builder.interfaces.secondary;

import app.webapp.payload.builder.interfaces.ISystemIdLinksBuilder;

public interface IScreeningMetadataLinksBuilder
        extends ISystemIdLinksBuilder {

}
