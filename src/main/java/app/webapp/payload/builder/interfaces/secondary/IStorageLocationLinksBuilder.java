package app.webapp.payload.builder.interfaces.secondary;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.builder.interfaces.ILinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;

public interface IStorageLocationLinksBuilder
        extends ILinksBuilder {
    void addFonds(ISystemId entity,
                  ILinksNoarkObject linksNoarkObject);

    void addSeries(ISystemId entity,
                   ILinksNoarkObject linksNoarkObject);

    void addFile(ISystemId entity,
                 ILinksNoarkObject linksNoarkObject);

    void addRecordEntity(ISystemId entity,
                         ILinksNoarkObject linksNoarkObject);
}
