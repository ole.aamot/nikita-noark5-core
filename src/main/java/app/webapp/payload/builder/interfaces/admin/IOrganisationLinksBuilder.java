package app.webapp.payload.builder.interfaces.admin;

import app.webapp.payload.builder.interfaces.ILinksBuilder;

public interface IOrganisationLinksBuilder
        extends ILinksBuilder {
}
