package app.webapp.payload.serializers;

/**
 * <p>
 * Having an own serializer is done to have more fine-grained control over the
 * output. The outgoing payloads do not adhere to any format that a library supports
 * so we have to provide our own implementation.
 * <p>
 * Only Norwegian property names are used on the outgoing JSON property names
 * and are in accordance with the Noark standard.
 * <p>
 * Only values that are part of the standard are included in the JSON. Internal
 * identifiers or other internal values are not exposed in a payload
 *
 * @author tsodring
 * @version 0.7
 * @since 0.7
 */
