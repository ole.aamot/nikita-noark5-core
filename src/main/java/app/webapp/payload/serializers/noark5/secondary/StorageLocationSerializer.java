package app.webapp.payload.serializers.noark5.secondary;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.secondary.StorageLocation;
import app.webapp.payload.builder.noark5.secondary.StorageLocationLinksBuilder;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.links.secondary.StorageLocationLinks;
import app.webapp.payload.serializers.noark5.SystemIdEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.STORAGE_LOCATION;

/**
 * Serialize an outgoing StorageLocation object as JSON.
 */
@LinksPacker(using = StorageLocationLinksBuilder.class)
@LinksObject(using = StorageLocationLinks.class)
public class StorageLocationSerializer
        extends SystemIdEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject storageLocationLinks,
                                     JsonGenerator jgen) throws IOException {
        StorageLocation storageLocation = (StorageLocation) noarkEntity;
        jgen.writeStartObject();
        printNullable(jgen, STORAGE_LOCATION, storageLocation.getStorageLocation());
        printLinks(jgen, storageLocationLinks.getLinks(storageLocation));
        jgen.writeEndObject();
    }
}
