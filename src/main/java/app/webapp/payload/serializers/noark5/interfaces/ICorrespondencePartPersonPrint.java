package app.webapp.payload.serializers.noark5.interfaces;

import app.domain.interfaces.entities.secondary.ICorrespondencePartPersonEntity;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

public interface ICorrespondencePartPersonPrint
        extends ICorrespondencePartPrint {
    default void printCorrespondencePartPerson(JsonGenerator jgen,
                                               ICorrespondencePartPersonEntity correspondencePartPerson)
            throws IOException {
        printGenericPerson(jgen, correspondencePartPerson);
    }
}
