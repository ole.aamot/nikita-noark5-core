package app.webapp.payload.serializers.noark5.interfaces;

import app.domain.interfaces.IClassified;
import app.domain.noark5.secondary.Classified;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public interface IClassifiedPrint
        extends IPrint {
    default void printClassified(JsonGenerator jgen, IClassified classifiedEntity)
            throws IOException {
        if (classifiedEntity != null) {
            Classified classified = classifiedEntity.getReferenceClassified();
            if (classified != null) {
                jgen.writeObjectFieldStart(CLASSIFIED);
                printNullableMetadata(jgen, CLASSIFICATION, classified.getClassification());
                printNullableDateTime(jgen, CLASSIFICATION_DATE, classified.getClassificationDate());
                printNullable(jgen, CLASSIFICATION_BY, classified.getClassificationBy());
                printNullableDateTime(jgen, CLASSIFICATION_DOWNGRADED_DATE,
                        classified.getClassificationDowngradedDate());
                printNullable(jgen, CLASSIFICATION_DOWNGRADED_BY, classified.getClassificationDowngradedBy());
                jgen.writeEndObject();
            }
        }
    }
}
