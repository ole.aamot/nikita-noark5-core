package app.webapp.payload.serializers.noark5.secondary;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.INoarkEntity;
import app.domain.interfaces.entities.secondary.IConversionEntity;
import app.webapp.payload.builder.noark5.secondary.ConversionLinksBuilder;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.links.secondary.ConversionLinks;
import app.webapp.payload.serializers.noark5.SystemIdEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Serialize an outgoing Conversion object as JSON.
 */
@LinksPacker(using = ConversionLinksBuilder.class)
@LinksObject(using = ConversionLinks.class)
public class ConversionSerializer
        extends SystemIdEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject conversionLinks, JsonGenerator jgen)
            throws IOException {
        IConversionEntity conversionEntity = (IConversionEntity) noarkEntity;
        jgen.writeStartObject();
        if (conversionEntity != null) {
            printSystemIdEntity(jgen, conversionEntity);
            printDateTime(jgen, CONVERTED_DATE, conversionEntity.getConvertedDate());
            printNullable(jgen, CONVERTED_BY, conversionEntity.getConvertedBy());
            printNullableMetadata(jgen, CONVERTED_FROM_FORMAT, conversionEntity.getConvertedFromFormat());
            printNullableMetadata(jgen, CONVERTED_TO_FORMAT, conversionEntity.getConverteLinksFormat());
            printNullable(jgen, CONVERSION_TOOL, conversionEntity.getConversionTool());
            printNullable(jgen, CONVERSION_COMMENT, conversionEntity.getConversionComment());
        }
        printLinks(jgen, conversionLinks.getLinks(conversionEntity));
        jgen.writeEndObject();
    }
}
