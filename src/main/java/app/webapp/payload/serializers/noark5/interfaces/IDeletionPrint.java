package app.webapp.payload.serializers.noark5.interfaces;

import app.domain.interfaces.IDeletion;
import app.domain.noark5.secondary.Deletion;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public interface IDeletionPrint
        extends IPrint {
    default void printDeletion(JsonGenerator jgen, IDeletion deletionEntity) throws IOException {
        if (deletionEntity != null) {
            Deletion deletion = deletionEntity.getReferenceDeletion();
            if (deletion != null) {
                jgen.writeObjectFieldStart(DELETION);
                printNullable(jgen, DELETION_BY, deletion.getDeletionBy());
                printNullableMetadata(jgen, DELETION_TYPE, deletion.getDeletionType());
                printNullableDateTime(jgen, DELETION_DATE, deletion.getDeletionDate());
                jgen.writeEndObject();
            }
        }
    }
}
