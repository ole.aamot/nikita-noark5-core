package app.webapp.payload.serializers.noark5.interfaces;

import app.domain.interfaces.IScreening;
import app.domain.noark5.secondary.Screening;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public interface IScreeningPrint
        extends IPrint {
    default void printScreening(JsonGenerator jgen, IScreening screeningEntity) throws IOException {
        if (null != screeningEntity && null != screeningEntity.getReferenceScreening()) {
            jgen.writeObjectFieldStart(SCREENING);
            printScreening(jgen, screeningEntity.getReferenceScreening());
            jgen.writeEndObject();
        }
    }

    default void printScreening(JsonGenerator jgen, Screening screening) throws IOException {
        if (screening != null) {
            printNullableMetadata(jgen, SCREENING_ACCESS_RESTRICTION, screening.getAccessRestriction());
            printNullable(jgen, SCREENING_AUTHORITY, screening.getScreeningAuthority());
            printNullableMetadata(jgen, SCREENING_SCREENING_DOCUMENT, screening.getScreeningDocument());
            printNullableDateTime(jgen, SCREENING_EXPIRES_DATE, screening.getScreeningExpiresDate());
            printNullable(jgen, SCREENING_DURATION, screening.getScreeningDuration());
        }
    }
}
