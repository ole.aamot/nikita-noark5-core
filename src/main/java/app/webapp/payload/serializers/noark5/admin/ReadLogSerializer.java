package app.webapp.payload.serializers.noark5.admin;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.INoarkEntity;
import app.domain.interfaces.entities.admin.IReadLogEntity;
import app.webapp.payload.builder.noark5.admin.ReadLogLinksBuilder;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.links.admin.ReadLogLinks;
import app.webapp.payload.serializers.noark5.EventLogSerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

@LinksPacker(using = ReadLogLinksBuilder.class)
@LinksObject(using = ReadLogLinks.class)
public class ReadLogSerializer
        extends EventLogSerializer {
    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject readLogLinks, JsonGenerator jgen)
            throws IOException {
        IReadLogEntity readLog = (IReadLogEntity) noarkEntity;
        jgen.writeStartObject();
        if (readLog != null) {
            serializeEventLog(readLog, jgen);
            serializeReadLog(readLog, jgen);
        }
        printLinks(jgen, readLogLinks.getLinks(readLog));
        jgen.writeEndObject();
    }

    /**
     * Empty method is left here so that we can bring in required functionality later
     *
     * @param readLog the readLog object
     * @param jgen    The JsonGenertor
     * @throws IOException if something goes wrong
     */
    public void serializeReadLog(IReadLogEntity readLog, JsonGenerator jgen) throws IOException {
    }
}
