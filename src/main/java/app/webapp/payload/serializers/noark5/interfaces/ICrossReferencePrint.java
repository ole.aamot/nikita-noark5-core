package app.webapp.payload.serializers.noark5.interfaces;

import app.domain.interfaces.ICrossReference;
import app.domain.interfaces.entities.ICrossReferenceEntity;
import app.domain.noark5.secondary.CrossReference;
import com.fasterxml.jackson.core.JsonGenerator;
import jakarta.validation.constraints.NotNull;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public interface ICrossReferencePrint
        extends IPrint {
    default void printCrossReferences(@NotNull JsonGenerator jgen, @NotNull ICrossReference crossReferences)
            throws IOException {
        if (crossReferences.getReferenceCrossReference().size() > 0) {
            jgen.writeArrayFieldStart(CROSS_REFERENCES);
            for (CrossReference crossReference : crossReferences.getReferenceCrossReference()) {
                jgen.writeStartObject();
                printCrossReference(jgen, crossReference);
                jgen.writeEndObject();
            }
            jgen.writeEndArray();
        }
    }

    default void printCrossReference(@NotNull JsonGenerator jgen, @NotNull ICrossReferenceEntity crossReference)
            throws IOException {
        if (crossReference != null) {
            printSystemIdEntity(jgen, crossReference);
            if (null != crossReference.getFromSystemId()) {
                jgen.writeStringField(FROM_SYSTEM_ID, crossReference.getFromSystemId().toString());
            }
            if (null != crossReference.getToSystemId()) {
                jgen.writeStringField(TO_SYSTEM_ID, crossReference.getToSystemId().toString());
            }
            if (null != crossReference.getReferenceType()) {
                jgen.writeStringField(REFERENCE_TYPE, crossReference.getReferenceType());
            }
        }
    }
}
