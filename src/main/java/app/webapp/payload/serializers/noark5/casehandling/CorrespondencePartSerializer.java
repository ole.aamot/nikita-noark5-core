package app.webapp.payload.serializers.noark5.casehandling;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.interfaces.entities.secondary.ICorrespondencePartEntity;
import app.domain.interfaces.entities.secondary.ICorrespondencePartInternalEntity;
import app.domain.interfaces.entities.secondary.ICorrespondencePartPersonEntity;
import app.domain.interfaces.entities.secondary.ICorrespondencePartUnitEntity;
import app.domain.noark5.casehandling.secondary.CorrespondencePart;
import app.domain.noark5.casehandling.secondary.CorrespondencePartInternal;
import app.domain.noark5.casehandling.secondary.CorrespondencePartPerson;
import app.domain.noark5.casehandling.secondary.CorrespondencePartUnit;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.interfaces.ICorrespondencePartInternalPrint;
import app.webapp.payload.serializers.noark5.interfaces.ICorrespondencePartPersonPrint;
import app.webapp.payload.serializers.noark5.interfaces.ICorrespondencePartUnitPrint;
import app.webapp.payload.serializers.noark5.secondary.PartSerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.CORRESPONDENCE_PART_TYPE;

/**
 * Serialize an outgoing CorrespondencePart object as JSON.
 */
public class CorrespondencePartSerializer
        extends PartSerializer
        implements ICorrespondencePartPersonPrint, ICorrespondencePartUnitPrint, ICorrespondencePartInternalPrint {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject correspondencePartLinks,
                                     JsonGenerator jgen)
            throws IOException {
        CorrespondencePart correspondencePart = (CorrespondencePart) noarkEntity;
        jgen.writeStartObject();
        printCorrespondencePart(jgen, correspondencePart);
        if (correspondencePart instanceof CorrespondencePartPerson) {
            printCorrespondencePartPerson(jgen, (ICorrespondencePartPersonEntity) correspondencePart);
        }
        if (correspondencePart instanceof CorrespondencePartUnit) {
            printCorrespondencePartUnit(jgen, (ICorrespondencePartUnitEntity) correspondencePart);
        }
        if (correspondencePart instanceof CorrespondencePartInternal) {
            printCorrespondencePartInternal(jgen, (ICorrespondencePartInternalEntity) correspondencePart);
        }
        printBSM(jgen, correspondencePart);
        printLinks(jgen, correspondencePartLinks.getLinks(correspondencePart));
        jgen.writeEndObject();
    }

    protected void printCorrespondencePart(JsonGenerator jgen, ICorrespondencePartEntity correspondencePart)
            throws IOException {
        printSystemIdEntity(jgen, correspondencePart);
        printNullableMetadata(jgen, CORRESPONDENCE_PART_TYPE, correspondencePart.getCorrespondencePartType());
    }
}
