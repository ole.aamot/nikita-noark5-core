package app.webapp.payload.serializers.noark5.secondary;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.secondary.DocumentFlow;
import app.webapp.payload.builder.noark5.secondary.DocumentFlowLinksBuilder;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.links.secondary.DocumentFlowLinks;
import app.webapp.payload.serializers.noark5.SystemIdEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

@LinksPacker(using = DocumentFlowLinksBuilder.class)
@LinksObject(using = DocumentFlowLinks.class)
public class DocumentFlowSerializer
        extends SystemIdEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject documentFlowLinks, JsonGenerator jgen)
            throws IOException {
        DocumentFlow documentFlow = (DocumentFlow) noarkEntity;
        jgen.writeStartObject();
        printSystemIdEntity(jgen, documentFlow);
        printCreateEntity(jgen, documentFlow);
        printNullable(jgen, DOCUMENT_FLOW_FLOW_TO, documentFlow.getFlowTo());
        printNullable(jgen, DOCUMENT_FLOW_FLOW_FROM, documentFlow.getFlowFrom());
        printNullableDateTime(jgen, DOCUMENT_FLOW_FLOW_RECEIVED_DATE, documentFlow.getFlowReceivedDate());
        printNullableDateTime(jgen, DOCUMENT_FLOW_FLOW_SENT_DATE, documentFlow.getFlowSentDate());
        printNullableMetadata(jgen, DOCUMENT_FLOW_FLOW_STATUS, documentFlow.getFlowStatus());
        printNullable(jgen, DOCUMENT_FLOW_FLOW_COMMENT, documentFlow.getFlowComment());
        printLinks(jgen, documentFlowLinks.getLinks(documentFlow));
        jgen.writeEndObject();
    }
}
