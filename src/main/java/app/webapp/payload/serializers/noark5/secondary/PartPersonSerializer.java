package app.webapp.payload.serializers.noark5.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.secondary.PartPerson;
import app.webapp.payload.links.LinksNoarkObject;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

/**
 * Serialize an outgoing PartPerson object as JSON.
 */
public class PartPersonSerializer
        extends PartSerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject partPersonLinks, JsonGenerator jgen)
            throws IOException {
        PartPerson partPerson = (PartPerson) noarkEntity;
        jgen.writeStartObject();
        printPartPerson(jgen, partPerson);
        printBSM(jgen, partPerson);
        printLinks(jgen, partPersonLinks.getLinks(partPerson));
        jgen.writeEndObject();
    }
}
