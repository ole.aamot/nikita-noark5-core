package app.webapp.payload.serializers.noark5.nationalidentifier;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.nationalidentifier.CadastralUnit;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.SystemIdEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public class CadastralUnitSerializer
        extends SystemIdEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject cadastralUnitLinks,
                                     JsonGenerator jgen) throws IOException {
        CadastralUnit cadastralUnit = (CadastralUnit) noarkEntity;
        jgen.writeStartObject();
        printSystemIdEntity(jgen, cadastralUnit);
        printNullable(jgen, MUNICIPALITY_NUMBER, cadastralUnit.getMunicipalityNumber());
        printNullable(jgen, HOLDING_NUMBER, cadastralUnit.getHoldingNumber());
        printNullable(jgen, SUB_HOLDING_NUMBER, cadastralUnit.getSubHoldingNumber());
        printNullable(jgen, LEASE_NUMBER, cadastralUnit.getLeaseNumber());
        printNullable(jgen, SECTION_NUMBER, cadastralUnit.getSectionNumber());
        printLinks(jgen, cadastralUnitLinks.getLinks(cadastralUnit));
        jgen.writeEndObject();
    }
}
