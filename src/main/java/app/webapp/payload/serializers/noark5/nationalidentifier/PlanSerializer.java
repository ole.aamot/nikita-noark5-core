package app.webapp.payload.serializers.noark5.nationalidentifier;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.nationalidentifier.Plan;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.SystemIdEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public class PlanSerializer
        extends SystemIdEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject planLinks,
                                     JsonGenerator jgen) throws IOException {
        Plan plan = (Plan) noarkEntity;
        jgen.writeStartObject();
        printSystemIdEntity(jgen, plan);
        printNullable(jgen, MUNICIPALITY_NUMBER, plan.getMunicipalityNumber());
        printNullable(jgen, COUNTY_NUMBER, plan.getCountyNumber());
        printNullableMetadata(jgen, COUNTRY_CODE, plan.getCountry());
        printNullable(jgen, PLAN_IDENTIFICATION, plan.getPlanIdentification());
        printLinks(jgen, planLinks.getLinks(plan));
        jgen.writeEndObject();
    }
}
