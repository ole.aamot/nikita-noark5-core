package app.webapp.payload.serializers.noark5.admin;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.INoarkEntity;
import app.domain.interfaces.entities.admin.IDeleteLogEntity;
import app.webapp.payload.builder.noark5.admin.DeleteLogLinksBuilder;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.links.admin.DeleteLogLinks;
import app.webapp.payload.serializers.noark5.EventLogSerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

@LinksPacker(using = DeleteLogLinksBuilder.class)
@LinksObject(using = DeleteLogLinks.class)
public class DeleteLogSerializer
        extends EventLogSerializer {
    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject deleteLogLinks, JsonGenerator jgen)
            throws IOException {
        IDeleteLogEntity deleteLog = (IDeleteLogEntity) noarkEntity;
        jgen.writeStartObject();
        if (deleteLog != null) {
            serializeEventLog(deleteLog, jgen);
            serializeDeleteLog(deleteLog, jgen);
        }
        printLinks(jgen, deleteLogLinks.getLinks(deleteLog));
        jgen.writeEndObject();
    }

    /**
     * Empty method is left here so that we can bring in required functionality later
     *
     * @param deleteLog the deleteLog object
     * @param jgen      The JsonGenertor
     * @throws IOException if something goes wrong
     */
    public void serializeDeleteLog(IDeleteLogEntity deleteLog, JsonGenerator jgen) throws IOException {
    }
}
