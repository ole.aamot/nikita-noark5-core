package app.webapp.payload.serializers.noark5.admin;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.admin.User;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.NoarkGeneralEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Serialize an outgoing UserLinks object as JSON.
 */
public class UserSerializer
        extends NoarkGeneralEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject userLinks,
                                     JsonGenerator jgen) throws IOException {
        User user = (User) noarkEntity;
        jgen.writeStartObject();
        printSystemIdEntity(jgen, user);
        printNullable(jgen, USER_NAME, user.getUsername());
        printNullable(jgen, FIRST_NAME, user.getFirstname());
        printNullable(jgen, SECOND_NAME, user.getLastname());
        printFinaliseEntity(jgen, user);
        printLinks(jgen, userLinks.getLinks(user));
        jgen.writeEndObject();
    }
}
