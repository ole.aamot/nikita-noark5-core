package app.webapp.payload.serializers.noark5;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.DocumentObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.interfaces.IElectronicSignaturePrint;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Serialize an outgoing DocumentObject object as JSON.
 */
public class DocumentObjectSerializer
        extends SystemIdEntitySerializer
        implements IElectronicSignaturePrint {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject documentObjectLinks,
                                     JsonGenerator jgen) throws IOException {
        DocumentObject documentObject = (DocumentObject) noarkEntity;
        jgen.writeStartObject();
        printSystemIdEntity(jgen, documentObject);
        printNullable(jgen, DOCUMENT_OBJECT_VERSION_NUMBER, documentObject.getVersionNumber());
        printNullableMetadata(jgen, DOCUMENT_OBJECT_VARIANT_FORMAT, documentObject.getVariantFormat());
        printNullableMetadata(jgen, DOCUMENT_OBJECT_FORMAT, documentObject.getFormat());
        printNullable(jgen, DOCUMENT_OBJECT_FORMAT_DETAILS, documentObject.getFormatDetails());
        printNullable(jgen, DOCUMENT_OBJECT_CHECKSUM, documentObject.getChecksum());
        printNullable(jgen, DOCUMENT_OBJECT_CHECKSUM_ALGORITHM, documentObject.getChecksumAlgorithm());
        printNullable(jgen, DOCUMENT_OBJECT_FILE_SIZE, documentObject.getFileSize());
        printNullable(jgen, DOCUMENT_OBJECT_FILE_NAME, documentObject.getOriginalFilename());
        printNullable(jgen, DOCUMENT_OBJECT_MIME_TYPE, documentObject.getMimeType());
        printCreateEntity(jgen, documentObject);
        printModifiedEntity(jgen, documentObject);
        printElectronicSignature(jgen, documentObject);
        printLinks(jgen, documentObjectLinks.getLinks(documentObject));
        jgen.writeEndObject();
    }
}
