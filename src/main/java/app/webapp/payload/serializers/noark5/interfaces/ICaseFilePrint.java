package app.webapp.payload.serializers.noark5.interfaces;

import app.domain.interfaces.entities.ICaseFileEntity;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public interface ICaseFilePrint
        extends IPrint {

    default void printCaseFileEntity(JsonGenerator jgen, ICaseFileEntity caseFile) throws IOException {
        if (caseFile.getCaseYear() != null) {
            jgen.writeNumberField(CASE_YEAR, caseFile.getCaseYear());
        }
        if (caseFile.getCaseSequenceNumber() != null) {
            jgen.writeNumberField(CASE_SEQUENCE_NUMBER, caseFile.getCaseSequenceNumber());
        }
        printNullableDateTime(jgen, CASE_DATE, caseFile.getCaseDate());
        printNullable(jgen, CASE_RESPONSIBLE, caseFile.getCaseResponsible());
        printNullable(jgen, CASE_RECORDS_MANAGEMENT_UNIT, caseFile.getRecordsManagementUnit());
        printNullableMetadata(jgen, CASE_STATUS, caseFile.getCaseStatus());
        printNullableDateTime(jgen, CASE_LOANED_DATE, caseFile.getLoanedDate());
        printNullable(jgen, CASE_LOANED_TO, caseFile.getLoaneLinks());
    }
}
