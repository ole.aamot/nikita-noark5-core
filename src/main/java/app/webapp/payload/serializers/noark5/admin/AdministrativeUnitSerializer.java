package app.webapp.payload.serializers.noark5.admin;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.admin.AdministrativeUnit;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.NoarkGeneralEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Serialize an outgoing AdministrativeUnit object as JSON.
 */
public class AdministrativeUnitSerializer
        extends NoarkGeneralEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity,
                                     LinksNoarkObject administrativeUnitLinks, JsonGenerator jgen) throws IOException {
        AdministrativeUnit administrativeUnit = (AdministrativeUnit) noarkEntity;
        jgen.writeStartObject();
        printSystemIdEntity(jgen, administrativeUnit);
        printNullable(jgen, ADMINISTRATIVE_UNIT_NAME, administrativeUnit.getAdministrativeUnitName());
        printNullable(jgen, SHORT_NAME, administrativeUnit.getShortName());
        printFinaliseEntity(jgen, administrativeUnit);
        printNullable(jgen, ADMINISTRATIVE_UNIT_STATUS, administrativeUnit.getAdministrativeUnitStatus());
        if (administrativeUnit.getParentAdministrativeUnit() != null) {
            jgen.writeStringField(ADMINISTRATIVE_UNIT_PARENT_REFERENCE,
                    administrativeUnit.getParentAdministrativeUnit().getSystemIdAsString());
        }
        printLinks(jgen, administrativeUnitLinks.getLinks(administrativeUnit));
        jgen.writeEndObject();
    }
}
