package app.webapp.payload.serializers.noark5.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.secondary.PartUnit;
import app.webapp.payload.links.LinksNoarkObject;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

/**
 * Serialize an outgoing PartUnit object as JSON.
 */
public class PartUnitSerializer
        extends PartSerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject partUnitLinks, JsonGenerator jgen)
            throws IOException {
        PartUnit partUnit = (PartUnit) noarkEntity;
        jgen.writeStartObject();
        printPartUnit(jgen, partUnit);
        printLinks(jgen, partUnitLinks.getLinks(partUnit));
        jgen.writeEndObject();
    }
}
