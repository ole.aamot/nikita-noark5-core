package app.webapp.payload.serializers.noark5.metadata;

import app.domain.interfaces.entities.IMetadataEntity;
import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.exceptions.NikitaException;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.NoarkEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;


/**
 * Serialize an outgoing Metadata entity as JSON.
 */
public class MetadataSerializer
        extends NoarkEntitySerializer {

    private static final Logger logger =
            LoggerFactory.getLogger(MetadataSerializer.class);

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject metadataLinks,
                                     JsonGenerator jgen) throws IOException {
        if (!(noarkEntity instanceof IMetadataEntity)) {
            String msg = "Internal error when serialising " +
                    noarkEntity + ". Not castable to nikita.domain" +
                    ".noark5.v5.interfaces.entities.IMetadataEntity";
            throw new NikitaException(msg);
        }
        IMetadataEntity metadataEntity = (IMetadataEntity) noarkEntity;
        jgen.writeStartObject();
        printMetadataEntity(jgen, metadataEntity);
        printLinks(jgen, metadataLinks.getLinks(metadataEntity));
        jgen.writeEndObject();
    }
}
