package app.webapp.payload.serializers.noark5;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.IChangeLogEntity;
import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.payload.builder.noark5.ChangeLogLinksBuilder;
import app.webapp.payload.links.ChangeLogLinks;
import app.webapp.payload.links.LinksNoarkObject;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

@LinksPacker(using = ChangeLogLinksBuilder.class)
@LinksObject(using = ChangeLogLinks.class)
public class ChangeLogSerializer
        extends EventLogSerializer {
    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject changeLogLinks, JsonGenerator jgen)
            throws IOException {
        IChangeLogEntity changeLog = (IChangeLogEntity) noarkEntity;
        jgen.writeStartObject();
        if (changeLog != null) {
            serializeEventLog(changeLog, jgen);
            serializeChangeLog(changeLog, jgen);
        }
        printLinks(jgen, changeLogLinks.getLinks(changeLog));
        jgen.writeEndObject();
    }

}
