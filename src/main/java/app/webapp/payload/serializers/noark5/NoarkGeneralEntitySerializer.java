package app.webapp.payload.serializers.noark5;

import app.domain.interfaces.entities.IFinalise;
import app.domain.interfaces.entities.ITitleDescription;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public class NoarkGeneralEntitySerializer
        extends SystemIdEntitySerializer {
    public NoarkGeneralEntitySerializer() {
    }

    public void printTitleAndDescription(JsonGenerator jgen, ITitleDescription titleDescriptionEntity)
            throws IOException {
        if (titleDescriptionEntity != null) {
            printNullable(jgen, TITLE, titleDescriptionEntity.getTitle());
            printNullable(jgen, DESCRIPTION, titleDescriptionEntity.getDescription());
        }
    }

    public void printFinaliseEntity(JsonGenerator jgen, IFinalise finaliseEntity)
            throws IOException {
        printNullable(jgen, FINALISED_BY, finaliseEntity.getFinalisedBy());
        printNullableDateTime(jgen, FINALISED_DATE, finaliseEntity.getFinalisedDate());
    }
}
