package app.webapp.payload.serializers.noark5.interfaces;

import app.domain.interfaces.entities.secondary.ICorrespondencePartInternalEntity;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.ADMINISTRATIVE_UNIT_FIELD;
import static app.utils.constants.N5ResourceMappings.CASE_HANDLER;

public interface ICorrespondencePartInternalPrint
        extends ICorrespondencePartPrint {
    default void printCorrespondencePartInternal(JsonGenerator jgen,
                                                 ICorrespondencePartInternalEntity correspondencePartInternal)
            throws IOException {
        if (null != correspondencePartInternal) {
            printNullable(jgen, ADMINISTRATIVE_UNIT_FIELD, correspondencePartInternal.getAdministrativeUnit());
            printNullable(jgen, CASE_HANDLER, correspondencePartInternal.getCaseHandler());
        }
    }
}
