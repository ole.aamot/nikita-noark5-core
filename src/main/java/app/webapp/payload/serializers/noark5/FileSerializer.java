package app.webapp.payload.serializers.noark5;

import app.domain.interfaces.entities.IFileEntity;
import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.File;
import app.domain.noark5.casehandling.CaseFile;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.interfaces.*;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.FILE_ID;
import static app.utils.constants.N5ResourceMappings.FILE_PUBLIC_TITLE;

/**
 * Serialize an outgoing File object as JSON.
 */
public class FileSerializer
        extends BaseFileSerializer
        implements ICaseFilePrint, IClassifiedPrint, ICrossReferencePrint, IDisposalPrint,
        IDocumentMediumPrint, IElectronicSignaturePrint, IScreeningPrint {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject fileLinks,
                                     JsonGenerator jgen) throws IOException {
        File file = (File) noarkEntity;
        jgen.writeStartObject();
        printFileEntity(jgen, file);
        if (file instanceof CaseFile) {
            printCaseFileEntity(jgen, (CaseFile) file);
        }
        printDocumentMedium(jgen, file);
        printFinaliseEntity(jgen, file);
        printCreateEntity(jgen, file);
        printModifiedEntity(jgen, file);
        printCrossReferences(jgen, file);
        printDisposal(jgen, file);
        printScreening(jgen, file);
        printClassified(jgen, file);
        printBSM(jgen, file);
        printLinks(jgen, fileLinks.getLinks(file));
        jgen.writeEndObject();
    }

    public void printFileEntity(JsonGenerator jgen,
                                IFileEntity file)
            throws IOException {
        printSystemIdEntity(jgen, file);
        printNullable(jgen, FILE_ID, file.getFileId());
        printTitleAndDescription(jgen, file);
        printNullable(jgen, FILE_PUBLIC_TITLE, file.getPublicTitle());
    }
}
