package app.webapp.payload.serializers.noark5.casehandling;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.casehandling.CaseFile;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.FileSerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

/**
 * Serialize an outgoing CaseFile object as JSON.
 */

public class CaseFileSerializer
        extends FileSerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject caseFileLinks,
                                     JsonGenerator jgen) throws IOException {
        CaseFile caseFile = (CaseFile) noarkEntity;
        jgen.writeStartObject();
        printFileEntity(jgen, caseFile);
        printCaseFileEntity(jgen, caseFile);
        printDocumentMedium(jgen, caseFile);
        printFinaliseEntity(jgen, caseFile);
        printCrossReferences(jgen, caseFile);
        printDisposal(jgen, caseFile);
        printScreening(jgen, caseFile);
        printClassified(jgen, caseFile);
        printBSM(jgen, caseFile);
        printLinks(jgen, caseFileLinks.getLinks(caseFile));
        jgen.writeEndObject();
    }
}
