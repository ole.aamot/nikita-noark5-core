package app.webapp.payload.serializers.noark5;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.Series;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.interfaces.*;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Serialize an outgoing Series object as JSON.
 */
public class SeriesSerializer
        extends NoarkGeneralEntitySerializer
        implements IClassifiedPrint, ICrossReferencePrint, IDeletionPrint, IDisposalPrint, IDisposalUndertakenPrint,
        IDocumentMediumPrint, IElectronicSignaturePrint, IScreeningPrint {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity,
                                     LinksNoarkObject seriesLinks,
                                     JsonGenerator jgen) throws IOException {
        Series series = (Series) noarkEntity;
        jgen.writeStartObject();
        printSystemIdEntity(jgen, series);
        printTitleAndDescription(jgen, series);
        printNullableMetadata(jgen, SERIES_STATUS, series.getSeriesStatus());
        printDocumentMedium(jgen, series);
        printFinaliseEntity(jgen, series);
        printCreateEntity(jgen, series);
        printModifiedEntity(jgen, series);
        printNullableDateTime(jgen, SERIES_START_DATE, series.getSeriesStartDate());
        printNullableDateTime(jgen, SERIES_END_DATE, series.getSeriesEndDate());
        if (null != series.getReferencePrecursorSystemID()) {
            print(jgen, SERIES_ASSOCIATE_AS_PRECURSOR, series.getReferencePrecursorSystemID());
        }
        if (null != series.getReferenceSuccessorSystemID()) {
            print(jgen, SERIES_ASSOCIATE_AS_SUCCESSOR, series.getReferenceSuccessorSystemID());
        }
        printDisposal(jgen, series);
        printDisposalUndertaken(jgen, series);
        printDeletion(jgen, series);
        printScreening(jgen, series);
        printClassified(jgen, series);
        printLinks(jgen, seriesLinks.getLinks(series));
        jgen.writeEndObject();
    }
}

