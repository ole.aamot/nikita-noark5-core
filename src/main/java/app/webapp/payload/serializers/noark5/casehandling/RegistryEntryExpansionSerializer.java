package app.webapp.payload.serializers.noark5.casehandling;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.casehandling.RegistryEntry;
import app.webapp.payload.links.LinksNoarkObject;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

/**
 * Serialize an outgoing expanded RegistryEntry object as JSON.
 */
public class RegistryEntryExpansionSerializer
        extends RegistryEntrySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject registryEntryLinks,
                                     JsonGenerator jgen) throws IOException {
        RegistryEntry registryEntry = (RegistryEntry) noarkEntity;
        jgen.writeStartObject();
        printRecordEntity(jgen, registryEntry);
        printRegistryEntryEntity(jgen, registryEntry);
        printLinks(jgen, registryEntryLinks.getLinks(registryEntry));
        jgen.writeEndObject();
    }
}
