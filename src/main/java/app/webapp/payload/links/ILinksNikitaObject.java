package app.webapp.payload.links;

import app.domain.interfaces.entities.INoarkEntity;

import java.util.List;

public interface ILinksNikitaObject {
    List<Link> getLinks(INoarkEntity entity);

    List<INoarkEntity> getList();

    void addLink(INoarkEntity entity, Link link);

    void addSelfLink(Link selfLink);

    void addNextLink(Link selfLink);

    void addLink(Link selfLink);

    List<Link> getSelfLinks();

    boolean isSingleEntity();

    Long getEntityVersion();
}
