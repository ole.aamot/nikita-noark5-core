package app.webapp.payload.links.admin;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.admin.ReadLogSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.READ_LOG;

@JsonSerialize(using = ReadLogSerializer.class)
public class ReadLogLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public ReadLogLinks(INoarkEntity entity) {
        super(entity);
    }

    public ReadLogLinks(SearchResultsPage page) {
        super(page, READ_LOG);
    }
}
