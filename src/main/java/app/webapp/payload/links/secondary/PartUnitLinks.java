package app.webapp.payload.links.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.secondary.PartUnitSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.PART_UNIT;

@JsonSerialize(using = PartUnitSerializer.class)
public class PartUnitLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public PartUnitLinks(INoarkEntity entity) {
        super(entity);
    }

    public PartUnitLinks(SearchResultsPage page) {
        super(page, PART_UNIT);
    }
}
