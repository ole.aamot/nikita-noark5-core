package app.webapp.payload.links.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.secondary.StorageLocationSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.STORAGE_LOCATION;

@JsonSerialize(using = StorageLocationSerializer.class)
public class StorageLocationLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public StorageLocationLinks() {
    }

    public StorageLocationLinks(INoarkEntity entity) {
        super(entity);
    }

    public StorageLocationLinks(SearchResultsPage page) {
        super(page, STORAGE_LOCATION);
    }
}
