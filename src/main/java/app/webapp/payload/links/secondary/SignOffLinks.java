package app.webapp.payload.links.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.secondary.SignOffSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.SIGN_OFF;

@JsonSerialize(using = SignOffSerializer.class)
public class SignOffLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public SignOffLinks() {
    }

    public SignOffLinks(INoarkEntity entity) {
        super(entity);
    }

    public SignOffLinks(SearchResultsPage page) {
        super(page, SIGN_OFF);
    }
}
