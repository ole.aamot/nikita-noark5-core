package app.webapp.payload.links.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.secondary.ConversionSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.CONVERSION;

@JsonSerialize(using = ConversionSerializer.class)
public class ConversionLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public ConversionLinks() {
    }

    public ConversionLinks(INoarkEntity entity) {
        super(entity);
    }

    public ConversionLinks(SearchResultsPage page) {
        super(page, CONVERSION);
    }
}
