package app.webapp.payload.links;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.serializers.noark5.DocumentDescriptionSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.DOCUMENT_DESCRIPTION;

@JsonSerialize(using = DocumentDescriptionSerializer.class)
public class DocumentDescriptionLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public DocumentDescriptionLinks(INoarkEntity entity) {
        super(entity);
    }

    public DocumentDescriptionLinks(SearchResultsPage page) {
        super(page, DOCUMENT_DESCRIPTION);
    }
}
