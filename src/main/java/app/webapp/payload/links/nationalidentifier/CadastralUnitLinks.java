package app.webapp.payload.links.nationalidentifier;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.nationalidentifier.CadastralUnitSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.CADASTRAL_UNIT;

@JsonSerialize(using = CadastralUnitSerializer.class)
public class CadastralUnitLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public CadastralUnitLinks(INoarkEntity entity) {
        super(entity);
    }

    public CadastralUnitLinks(SearchResultsPage page) {
        super(page, CADASTRAL_UNIT);
    }
}
