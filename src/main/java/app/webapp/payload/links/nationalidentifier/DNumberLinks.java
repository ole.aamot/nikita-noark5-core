package app.webapp.payload.links.nationalidentifier;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.nationalidentifier.DNumberSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.D_NUMBER;

@JsonSerialize(using = DNumberSerializer.class)
public class DNumberLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public DNumberLinks(INoarkEntity entity) {
        super(entity);
    }

    public DNumberLinks(SearchResultsPage page) {
        super(page, D_NUMBER);
    }
}
