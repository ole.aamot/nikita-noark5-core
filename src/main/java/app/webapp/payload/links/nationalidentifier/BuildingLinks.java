package app.webapp.payload.links.nationalidentifier;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.nationalidentifier.BuildingSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.BUILDING;

@JsonSerialize(using = BuildingSerializer.class)
public class BuildingLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public BuildingLinks(INoarkEntity entity) {
        super(entity);
    }

    public BuildingLinks(SearchResultsPage page) {
        super(page, BUILDING);
    }
}
