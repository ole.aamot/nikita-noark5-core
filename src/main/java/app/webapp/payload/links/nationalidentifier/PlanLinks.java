package app.webapp.payload.links.nationalidentifier;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.nationalidentifier.PlanSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.PLAN;

@JsonSerialize(using = PlanSerializer.class)
public class PlanLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public PlanLinks(INoarkEntity entity) {
        super(entity);
    }

    public PlanLinks(SearchResultsPage page) {
        super(page, PLAN);
    }
}
