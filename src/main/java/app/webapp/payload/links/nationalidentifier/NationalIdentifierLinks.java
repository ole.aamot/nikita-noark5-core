package app.webapp.payload.links.nationalidentifier;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.nationalidentifier.NationalIdentifierSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.NATIONAL_IDENTIFIER;

@JsonSerialize(using = NationalIdentifierSerializer.class)
public class NationalIdentifierLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public NationalIdentifierLinks(INoarkEntity entity) {
        super(entity);
    }

    public NationalIdentifierLinks(SearchResultsPage page) {
        super(page, NATIONAL_IDENTIFIER);
    }
}
