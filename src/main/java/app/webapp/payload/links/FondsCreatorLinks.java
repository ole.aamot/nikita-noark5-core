package app.webapp.payload.links;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.serializers.noark5.FondsCreatorSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.FONDS_CREATOR;

@JsonSerialize(using = FondsCreatorSerializer.class)
public class FondsCreatorLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public FondsCreatorLinks(INoarkEntity entity) {
        super(entity);
    }

    public FondsCreatorLinks(SearchResultsPage page) {
        super(page, FONDS_CREATOR);
    }
}
