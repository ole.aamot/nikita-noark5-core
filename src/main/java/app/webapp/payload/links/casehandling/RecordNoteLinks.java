package app.webapp.payload.links.casehandling;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.casehandling.RecordNoteSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.REGISTRY_ENTRY;

@JsonSerialize(using = RecordNoteSerializer.class)
public class RecordNoteLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public RecordNoteLinks(INoarkEntity entity) {
        super(entity);
    }

    public RecordNoteLinks(SearchResultsPage page) {
        super(page, REGISTRY_ENTRY);
    }

}
