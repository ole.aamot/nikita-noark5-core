package app.webapp.payload.links.casehandling;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.casehandling.RegistryEntryExpansionSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = RegistryEntryExpansionSerializer.class)
public class RegistryEntryExpansionLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {
    public RegistryEntryExpansionLinks(INoarkEntity entity) {
        super(entity);
    }
}
