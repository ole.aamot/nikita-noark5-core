package app.webapp.payload.links.casehandling;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.casehandling.CorrespondencePartSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.CORRESPONDENCE_PART;

@JsonSerialize(using = CorrespondencePartSerializer.class)
public class CorrespondencePartLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public CorrespondencePartLinks(INoarkEntity entity) {
        super(entity);
    }

    public CorrespondencePartLinks(SearchResultsPage page) {
        super(page, CORRESPONDENCE_PART);
    }
}
