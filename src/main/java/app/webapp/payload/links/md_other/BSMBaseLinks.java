package app.webapp.payload.links.md_other;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.metadata.BSMBaseSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.BSM_DEF;

@JsonSerialize(using = BSMBaseSerializer.class)
public class BSMBaseLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public BSMBaseLinks(INoarkEntity entity) {
        super(entity);
    }

    public BSMBaseLinks(SearchResultsPage page) {
        super(page, BSM_DEF);
    }
}
