package app.webapp.spring.security;

import app.domain.noark5.admin.User;
import app.domain.repository.admin.IUserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class NikitaUserDetailsService
        implements UserDetailsService {

    private final IUserRepository userRepository;

    public NikitaUserDetailsService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws
            UsernameNotFoundException {
        Optional<User> userOptional = userRepository.findByUsername(username);
        if (userOptional.isEmpty()) {
            throw new UsernameNotFoundException("No user found with this " +
                    "username: " + username);
        }
        User user = userOptional.get();
        return new NikitaUserDetails(user, user.getAuthorities());
    }
}
