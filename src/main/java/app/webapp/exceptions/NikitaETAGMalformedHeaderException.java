package app.webapp.exceptions;

/**
 * Created by tsodring on 12/8/16.
 * <p>
 * Has its own exception as ETAG issues occur beyond the boundaries of a controller. This is a special case exception
 */
public class NikitaETAGMalformedHeaderException extends NikitaMalformedHeaderException {

    public NikitaETAGMalformedHeaderException() {
        super();
    }

    public NikitaETAGMalformedHeaderException(String message) {
        super(message);
    }
}

