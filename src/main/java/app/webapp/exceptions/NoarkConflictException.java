package app.webapp.exceptions;

public class NoarkConflictException
        extends NikitaException {
    public NoarkConflictException(final String message) {
        super(message);
    }
}
