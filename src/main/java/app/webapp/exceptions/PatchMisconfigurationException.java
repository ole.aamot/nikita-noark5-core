package app.webapp.exceptions;

public class PatchMisconfigurationException
        extends NikitaException {

    public PatchMisconfigurationException(String message) {
        super(message);
    }
}
