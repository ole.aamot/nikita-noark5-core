package app.controller.noark5.metadata;

import app.domain.noark5.md_other.BSMMetadata;
import app.service.interfaces.metadata.IBSMMetadataService;
import app.webapp.exceptions.NikitaException;
import app.webapp.model.PatchMerge;
import app.webapp.payload.links.metadata.BSMMetadataLinks;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static app.utils.constants.Constants.*;
import static app.utils.constants.HATEOASConstants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = HREF_BASE_METADATA,
        produces = NOARK5_V5_CONTENT_TYPE_JSON)
public class BSMMetadataController {

    private final IBSMMetadataService bsmMetadataService;

    public BSMMetadataController(IBSMMetadataService bsmMetadataService) {
        this.bsmMetadataService = bsmMetadataService;
    }

    // API - All POST Requests (CRUD - CREATE)
    // Creates a new BSMMetadata
    // POST [contextPath][api]/metadata/ny-virksomhetsspesifikkeMetadata
    @Operation(summary = "Persists a new BSMMetadata object",
            description = "Returns the newly created BSMMetadata object after "
                    + "it is persisted to the database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = CREATED_VAL,
                    description = "BSMMetadata " +
                            API_MESSAGE_OBJECT_SUCCESSFULLY_CREATED),
            @ApiResponse(responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(responseCode = NOT_FOUND_VAL,
                    description = API_MESSAGE_MALFORMED_PAYLOAD),
            @ApiResponse(responseCode = CONFLICT_VAL,
                    description = API_MESSAGE_CONFLICT),
            @ApiResponse(responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @PostMapping(SLASH + NEW_BSM_DEF)
    public ResponseEntity<BSMMetadataLinks> createBSMMetadata(
            @RequestBody BSMMetadata bsmMetadata) {
        return ResponseEntity.status(CREATED)
                .body(bsmMetadataService.save(bsmMetadata));
    }

    // API - All GET Requests (CRUD - READ)
    // Retrieves all BSMMetadata of a given type (entityName)
    // GET [contextPath][api]/metadata/virksomhetsspesifikkeMetadata
    @Operation(summary = "Retrieves all BSMMetadataEntity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = OK_VAL,
                    description = "BSMMetadataEntity codes found"),
            @ApiResponse(responseCode = NOT_FOUND_VAL,
                    description = "No BSMMetadataEntity found"),
            @ApiResponse(responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping(SLASH + BSM_DEF)
    public ResponseEntity<BSMMetadataLinks> findAll() {
        return ResponseEntity.status(OK)
                .body(bsmMetadataService.findAll());
    }

    // Create a suggested fondsStatus(like a template) with default values
    // (nothing persisted)
    // GET [contextPath][api]/metadata/ny-BSMMetadata
    @Operation(summary = "Creates a suggested BSMMetadata")
    @ApiResponses(value = {
            @ApiResponse(responseCode = OK_VAL,
                    description = "BSMMetadata codes found"),
            @ApiResponse(responseCode = NOT_FOUND_VAL,
                    description = "No BSMMetadataEntity found"),
            @ApiResponse(responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping(value = NEW_BSM_DEF)
    public ResponseEntity<String>
    getBSMMetadataTemplate() {
        return ResponseEntity.status(OK)
                .body("{}");
    }

    // Get a BSMMetadata with given values
    // GET [contextPath][api]/metadata/virksomhetsspesifikkeMetadata/{systemId}
    @Operation(summary = "Updates a BSMMetadata identified by a given systemId",
            description = "Returns the newly updated bsmMetadata")
    @ApiResponses(value = {
            @ApiResponse(responseCode = OK_VAL,
                    description = "BSMMetadata OK"),
            @ApiResponse(responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(responseCode = NOT_FOUND_VAL,
                    description = API_MESSAGE_PARENT_DOES_NOT_EXIST +
                            " of type BSMMetadata"),
            @ApiResponse(responseCode = CONFLICT_VAL,
                    description = API_MESSAGE_CONFLICT),
            @ApiResponse(responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping(value = SLASH + BSM_DEF + SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<BSMMetadataLinks> getBSMMetadata(
            @Parameter(name = SYSTEM_ID,
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID)
            throws NikitaException {
        return ResponseEntity.status(OK)
                .body(bsmMetadataService.find(systemID));
    }

    // Update a BSMMetadata identified by a UUID
    // PATCH [contextPath][api]/metadata/virksomhetsspesifikkeMetadata/{systemId}
    @Operation(summary = "Updates a BSMMetadata identified by a given systemId",
            description = "Returns the newly updated bsmMetadata")
    @ApiResponses(value = {
            @ApiResponse(responseCode = OK_VAL,
                    description = "BSMMetadata OK"),
            @ApiResponse(responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(responseCode = NOT_FOUND_VAL,
                    description = API_MESSAGE_PARENT_DOES_NOT_EXIST +
                            " of type BSMMetadata"),
            @ApiResponse(responseCode = CONFLICT_VAL,
                    description = API_MESSAGE_CONFLICT),
            @ApiResponse(responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @PatchMapping(value = SLASH + BSM_DEF + SLASH + SYSTEM_ID_PARAMETER,
            consumes = CONTENT_TYPE_JSON_MERGE_PATCH)
    public ResponseEntity<BSMMetadataLinks> patchBSMMetadata(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of BSM to update",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID,
            @Parameter(name = "PatchMerge",
                    description = "Incoming merge payload",
                    required = true)
            @RequestBody PatchMerge patchMerge) throws NikitaException {
        return ResponseEntity.status(OK)
                .body((BSMMetadataLinks)
                        bsmMetadataService.handleUpdateRfc7396(
                                systemID, patchMerge));
    }

    // Update a BSMMetadata identified by a UUID
    // PATCH [contextPath][api]/metadata/virksomhetsspesifikkeMetadata/{systemId}
    @Operation(summary = "Updates a BSMMetadata identified by a given systemId",
            description = "Returns the newly updated bsmMetadata")
    @ApiResponses(value = {
            @ApiResponse(responseCode = OK_VAL,
                    description = "BSMMetadata OK"),
            @ApiResponse(responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(responseCode = NOT_FOUND_VAL,
                    description = API_MESSAGE_PARENT_DOES_NOT_EXIST +
                            " of type BSMMetadata"),
            @ApiResponse(responseCode = CONFLICT_VAL,
                    description = API_MESSAGE_CONFLICT),
            @ApiResponse(responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @PutMapping(value = SLASH + BSM_DEF + SLASH + SYSTEM_ID_PARAMETER,
            consumes = NOARK5_V5_CONTENT_TYPE_JSON)
    public ResponseEntity<BSMMetadataLinks> putBSMMetadata(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of BSM to update",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID,
            @Parameter(name = "BSMMetadata",
                    description = "Incoming BSMMetadata payload",
                    required = true)
            @RequestBody BSMMetadata bsmMetadata) throws NikitaException {
        return ResponseEntity.status(OK)
                .body(bsmMetadataService.handleUpdate(systemID,
                        bsmMetadata));
    }

    // API - All DELETE Requests (CRUD - DELETE)
    // Delete a BSMMetadata
    // DELETE [contextPath][api]/metadata/BSMMetadata/{systemID}
    @Operation(
            summary = "Deletes a single BSMMetadata entity identified by the " +
                    "given systemID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = NO_CONTENT_VAL,
                    description = "BSMMetadata entity deleted"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @DeleteMapping(value = SLASH + BSM_DEF + SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<String> deleteBSMMetadata(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of bsmMetadata object to delete.",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID) {
        bsmMetadataService.deleteEntity(systemID);
        return ResponseEntity.status(NO_CONTENT)
                .body(DELETE_RESPONSE);
    }
}
