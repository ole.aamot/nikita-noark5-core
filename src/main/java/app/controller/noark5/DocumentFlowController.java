package app.controller.noark5;

import app.domain.noark5.secondary.DocumentFlow;
import app.service.interfaces.secondary.IDocumentFlowService;
import app.webapp.exceptions.NikitaException;
import app.webapp.payload.links.secondary.DocumentFlowLinks;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static app.utils.constants.Constants.*;
import static app.utils.constants.HATEOASConstants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = HREF_BASE_CASE_HANDLING + SLASH + DOCUMENT_FLOW,
        produces = NOARK5_V5_CONTENT_TYPE_JSON)
public class DocumentFlowController {

    private final IDocumentFlowService documentFlowService;

    public DocumentFlowController(
            IDocumentFlowService documentFlowService) {
        this.documentFlowService = documentFlowService;
    }

    // API - All GET Requests (CRUD - READ)

    // GET [contextPath][api]/sakarkiv/dokumentflyt/
    // https://rel.arkivverket.no/noark5/v5/api/sakarkiv/dokumentflyt/
    @Operation(summary = "Retrieves multiple DocumentFlow entities limited " +
            "by ownership rights")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "DocumentFlow found"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping()
    public ResponseEntity<DocumentFlowLinks> findAllDocumentFlow() {
        DocumentFlowLinks documentFlowLinks =
                documentFlowService.findAll();
        return ResponseEntity.status(OK)
                .body(documentFlowLinks);
    }

    // GET [contextPath][api]/sakarkiv/dokumentflyt/{systemId}
    @Operation(summary = "Retrieves a single DocumentFlow entity given a " +
            "systemID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "DocumentFlow returned"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping(value = SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<DocumentFlowLinks> findDocumentFlowBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the DocumentFlow to retrieve",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemId) {
        DocumentFlowLinks documentFlowLinks =
                documentFlowService.findBySystemId(systemId);
        return ResponseEntity.status(OK)
                .body(documentFlowLinks);
    }

    // PUT [contextPath][api]/sakarkiv/dokumentflyt/{systemId}
    @Operation(summary = "Updates a DocumentFlow identified by a given systemId",
            description = "Returns the newly updated nationalIdentifierPerson")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "DocumentFlow " +
                            API_MESSAGE_OBJECT_ALREADY_PERSISTED),
            @ApiResponse(
                    responseCode = CREATED_VAL,
                    description = "DocumentFlow " +
                            API_MESSAGE_OBJECT_SUCCESSFULLY_CREATED),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = NOT_FOUND_VAL,
                    description = API_MESSAGE_PARENT_DOES_NOT_EXIST +
                            " of type DocumentFlow"),
            @ApiResponse(
                    responseCode = CONFLICT_VAL,
                    description = API_MESSAGE_CONFLICT),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @PutMapping(value = SLASH + SYSTEM_ID_PARAMETER,
            consumes = NOARK5_V5_CONTENT_TYPE_JSON)
    public ResponseEntity<DocumentFlowLinks> updateDocumentFlowBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of DocumentFlow to update",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID,
            @Parameter(name = "DocumentFlow",
                    description = "Incoming DocumentFlow object",
                    required = true)
            @RequestBody DocumentFlow documentFlow) throws NikitaException {
        DocumentFlowLinks documentFlowLinks =
                documentFlowService.updateDocumentFlowBySystemId(systemID, documentFlow);
        return ResponseEntity.status(OK)
                .body(documentFlowLinks);
    }

    // DELETE [contextPath][api]/sakarkiv/dokumentflyt/{systemID}/
    @Operation(summary = "Deletes a single DocumentFlow entity identified by " +
            "systemID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "DocumentFlow deleted"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @DeleteMapping(value = SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<String> deleteDocumentFlowBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the documentFlow to delete",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID) {
        documentFlowService.deleteDocumentFlowBySystemId(systemID);
        return ResponseEntity.status(NO_CONTENT)
                .body(DELETE_RESPONSE);
    }
}
