package app.controller.noark5.log;

import app.domain.noark5.admin.DeleteLog;
import app.service.interfaces.admin.IDeleteLogService;
import app.webapp.exceptions.NikitaException;
import app.webapp.payload.links.admin.DeleteLogLinks;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static app.utils.constants.Constants.*;
import static app.utils.constants.HATEOASConstants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = HREF_BASE_LOGGING + SLASH,
        produces = NOARK5_V5_CONTENT_TYPE_JSON)
public class DeleteLogController {

    private final IDeleteLogService deleteLogService;

    public DeleteLogController(IDeleteLogService deleteLogService) {
        this.deleteLogService = deleteLogService;
    }

    // GET [contextPath][api]/loggingogsporing/slettelogg/
    @Operation(
            summary = "Retrieves all DeleteLog entities limited by ownership " +
                    " rights")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "DeleteLog found"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)
    })
    @GetMapping(DELETE_LOG)
    public ResponseEntity<DeleteLogLinks> findAllDeleteLog() {
        DeleteLogLinks deleteLogLinks = deleteLogService.
                findDeleteLogByOwner();
        return ResponseEntity.status(OK)
                .body(deleteLogLinks);
    }

    // GET [contextPath][api]/loggingogsporing/slettelogg/{systemId}/
    @Operation(summary = "Retrieves a single deleteLog entity given a systemId")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "DeleteLog returned"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping(value = DELETE_LOG + SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<DeleteLogLinks> findOne(
            @Parameter(name = SYSTEM_ID,
                    description = "systemId of deleteLog to retrieve.",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID) {
        DeleteLogLinks deleteLogLinks =
                deleteLogService.findSingleDeleteLog(systemID);
        return ResponseEntity.status(OK)
                .body(deleteLogLinks);
    }

    // PUT [contextPath][api]/loggingogsporing/slettelogg/{systemId}/
    @Operation(
            summary = "Updates a DeleteLog object",
            description = "Returns the newly updated DeleteLog object after " +
                    "it is persisted to the database")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "DeleteLog " +
                            API_MESSAGE_OBJECT_ALREADY_PERSISTED),
            @ApiResponse(
                    responseCode = CREATED_VAL,
                    description = "DeleteLog " +
                            API_MESSAGE_OBJECT_SUCCESSFULLY_CREATED),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = NOT_FOUND_VAL,
                    description = API_MESSAGE_PARENT_DOES_NOT_EXIST +
                            " of type DeleteLog"),
            @ApiResponse(
                    responseCode = CONFLICT_VAL,
                    description = API_MESSAGE_CONFLICT),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @PutMapping(value = DELETE_LOG + SLASH + SYSTEM_ID_PARAMETER,
            consumes = NOARK5_V5_CONTENT_TYPE_JSON)
    public ResponseEntity<DeleteLogLinks> updateDeleteLog(
            @Parameter(name = SYSTEM_ID,
                    description = "systemId of deleteLog to update.",
                    required = true)
            @PathVariable(SYSTEM_ID) UUID systemID,
            @Parameter(name = "deleteLog",
                    description = "Incoming deleteLog object",
                    required = true)
            @RequestBody DeleteLog deleteLog) throws NikitaException {
        DeleteLogLinks deleteLogLinks =
                deleteLogService.handleUpdate(systemID, deleteLog);
        return ResponseEntity.status(OK)
                .body(deleteLogLinks);
    }

    // DELETE [contextPath][api]/loggingogsporing/slettelogg/{systemId}/
    @Operation(
            summary = "Deletes a single DeleteLog entity identified by " +
                    "systemID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = NO_CONTENT_VAL,
                    description = "ok message"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @DeleteMapping(value = DELETE_LOG + SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<String> deleteDeleteLogBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the deleteLog to delete",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID) {
        deleteLogService.deleteEntity(systemID);
        return ResponseEntity.status(HttpStatus.NO_CONTENT)
                .body(DELETE_RESPONSE);
    }
}
