package app.controller.noark5.secondary;

import app.domain.noark5.metadata.Metadata;
import app.service.interfaces.secondary.IScreeningMetadataService;
import app.webapp.exceptions.NikitaException;
import app.webapp.payload.links.secondary.ScreeningMetadataLinks;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static app.utils.constants.Constants.*;
import static app.utils.constants.HATEOASConstants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = HREF_BASE_FONDS_STRUCTURE + SLASH + SCREENING_METADATA,
        produces = NOARK5_V5_CONTENT_TYPE_JSON)
public class ScreeningMetadataController {

    private final IScreeningMetadataService screeningMetadataService;

    public ScreeningMetadataController(
            IScreeningMetadataService screeningMetadataService) {
        this.screeningMetadataService = screeningMetadataService;
    }

    // API - All GET Requests (CRUD - READ)

    // GET [contextPath][api]/arkivstruktur/skjermingmetadata/
    // https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/skjermingmetadata/
    @Operation(summary = "Retrieves multiple ScreeningMetadata entities limited " +
            "by ownership rights")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "ScreeningMetadata found"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping()
    public ResponseEntity<ScreeningMetadataLinks> findAllScreeningMetadata() {
        ScreeningMetadataLinks screeningMetadataLinks =
                screeningMetadataService.findAll();
        return ResponseEntity.status(OK)
                .body(screeningMetadataLinks);
    }

    // GET [contextPath][api]/arkivstruktur/skjermingmetadata/{systemId}
    @Operation(summary = "Retrieves a single ScreeningMetadata entity given a " +
            "systemId")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "ScreeningMetadata returned"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping(value = SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<ScreeningMetadataLinks>
    findScreeningMetadataBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the ScreeningMetadata to retrieve",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID) {
        ScreeningMetadataLinks screeningMetadataLinks =
                screeningMetadataService.findBySystemId(systemID);
        return ResponseEntity.status(OK)
                .body(screeningMetadataLinks);
    }

    // PUT [contextPath][api]/arkivstruktur/skjermingmetadata/{systemId}
    @Operation(summary = "Updates a ScreeningMetadata identified by a given " +
            "systemId",
            description = "Returns the newly updated nationalIdentifierPerson")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "ScreeningMetadata " +
                            API_MESSAGE_OBJECT_ALREADY_PERSISTED),
            @ApiResponse(
                    responseCode = CREATED_VAL,
                    description = "ScreeningMetadata " +
                            API_MESSAGE_OBJECT_SUCCESSFULLY_CREATED),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = NOT_FOUND_VAL,
                    description = API_MESSAGE_PARENT_DOES_NOT_EXIST +
                            " of type ScreeningMetadata"),
            @ApiResponse(
                    responseCode = CONFLICT_VAL,
                    description = API_MESSAGE_CONFLICT),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @PutMapping(value = SLASH + SYSTEM_ID_PARAMETER,
            consumes = NOARK5_V5_CONTENT_TYPE_JSON)
    public ResponseEntity<ScreeningMetadataLinks>
    updateScreeningMetadataBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemId of ScreeningMetadata to update",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID,
            @Parameter(name = "ScreeningMetadata",
                    description = "Incoming ScreeningMetadata object",
                    required = true)
            @RequestBody Metadata screeningMetadata)
            throws NikitaException {
        ScreeningMetadataLinks screeningMetadataLinks =
                screeningMetadataService.updateScreeningMetadataBySystemId(
                        systemID, screeningMetadata);
        return ResponseEntity.status(OK)
                .body(screeningMetadataLinks);
    }

    // DELETE [contextPath][api]/arkivstruktur/skjermingmetadata/{systemID}/
    @Operation(summary = "Deletes a single ScreeningMetadata entity identified by " +
            "systemID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "ScreeningMetadata deleted"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @DeleteMapping(value = SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<String> deleteScreeningMetadataBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the screeningMetadata to delete",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID) {
        screeningMetadataService.deleteScreeningMetadataBySystemId(systemID);
        return ResponseEntity.status(NO_CONTENT)
                .body(DELETE_RESPONSE);
    }
}
