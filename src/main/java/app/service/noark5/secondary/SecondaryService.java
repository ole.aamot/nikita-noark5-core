package app.service.noark5.secondary;

import app.domain.interfaces.*;
import app.domain.repository.noark5.v5.secondary.*;
import app.service.interfaces.secondary.ISecondaryService;
import org.springframework.stereotype.Service;

/**
 * This is a catch-all Service class to reduce the number of class members in
 * other classes. It currently is solely used when deleting objects to make
 * sure we don't have any orphans left when deleting secondary entities.
 */
@Service
public class SecondaryService
        implements ISecondaryService {

    private final IDeletionRepository deletionRepository;
    private final IDisposalRepository disposalRepository;
    private final IDisposalUndertakenRepository disposalUndertakenRepository;
    private final IClassifiedRepository classifiedRepository;
    private final IScreeningRepository screeningRepository;
    private final IElectronicSignatureRepository electronicSignatureRepository;

    public SecondaryService(
            IDeletionRepository deletionRepository,
            IDisposalRepository disposalRepository,
            IDisposalUndertakenRepository disposalUndertakenRepository,
            IClassifiedRepository classifiedRepository,
            IScreeningRepository screeningRepository,
            IElectronicSignatureRepository electronicSignatureRepository) {
        this.deletionRepository = deletionRepository;
        this.disposalRepository = disposalRepository;
        this.disposalUndertakenRepository = disposalUndertakenRepository;
        this.classifiedRepository = classifiedRepository;
        this.screeningRepository = screeningRepository;
        this.electronicSignatureRepository = electronicSignatureRepository;
    }

    @Override
    public void deleteDeletionObject(IDeletion deletion) {
        if (null != deletion.getReferenceDeletion()) {
            deletionRepository.delete(deletion.getReferenceDeletion());
        }
    }

    @Override
    public void deleteDisposalObject(IDisposal disposal) {
        if (null != disposal.getReferenceDisposal()) {
            disposalRepository.delete(disposal.getReferenceDisposal());
        }
    }

    @Override
    public void deleteDisposalUndertakenObject(
            IDisposalUndertaken disposalUndertaken) {
        if (null != disposalUndertaken.getReferenceDisposalUndertaken()) {
            disposalUndertakenRepository.delete(disposalUndertaken
                    .getReferenceDisposalUndertaken());
        }
    }

    @Override
    public void deleteClassifiedObject(IClassified classified) {
        if (null != classified.getReferenceClassified()) {
            classifiedRepository.delete(classified.getReferenceClassified());
        }
    }

    @Override
    public void deleteScreeningObject(IScreening screening) {
        if (null != screening.getReferenceScreening()) {
            screeningRepository.delete(screening.getReferenceScreening());
        }
    }

    @Override
    public void deleteElectronicSignatureObject(
            IElectronicSignature electronicSignature) {
        if (null != electronicSignature.getReferenceElectronicSignature()) {
            electronicSignatureRepository.delete(
                    electronicSignature.getReferenceElectronicSignature());
        }
    }
}
