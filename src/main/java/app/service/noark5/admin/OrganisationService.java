package app.service.noark5.admin;

import app.domain.noark5.admin.Organisation;
import app.domain.repository.noark5.v5.admin.IOrganisationRepository;
import app.service.application.IPatchService;
import app.service.interfaces.IOrganisationService;
import app.service.noark5.NoarkService;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.payload.builder.interfaces.admin.IOrganisationLinksBuilder;
import app.webapp.payload.links.admin.OrganisationLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;

/**
 * Service class for Organisation
 */
@Service
public class OrganisationService
        extends NoarkService
        implements IOrganisationService {

    private static final Logger logger =
            LoggerFactory.getLogger(OrganisationService.class);

    private final IOrganisationRepository organisationRepository;
    private final IOrganisationLinksBuilder organisationLinksBuilder;

    public OrganisationService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            IOrganisationRepository organisationRepository,
            IOrganisationLinksBuilder organisationLinksBuilder) {
        super(entityManager, applicationEventPublisher, patchService);
        this.organisationRepository = organisationRepository;
        this.organisationLinksBuilder = organisationLinksBuilder;
    }

    // All CREATE operations

    /**
     * Persists a new organisation object to the database where
     * owned_by is set "system". It checks first to make sure that an
     * administrative unit with the given name does not exist. If it does exist
     * do nothing. This code should really only be called when creating some
     * basic data i nikita to make it operational for demo purposes.
     *
     * @param organisation organisation object with values set
     * @return the newly persisted organisation object
     */
    @Override
    @Transactional
    public void save(Organisation organisation) {
        organisationRepository.save(organisation);
    }

    // All READ methods

    /**
     * Retrieve all organisation
     *
     * @return list of all organisation
     */
    @Override
    public OrganisationLinks findAll() {
        return (OrganisationLinks) processODataQueryGet();
    }

    /**
     * Retrieve a single organisation identified by systemId
     *
     * @param systemId systemId of the organisation
     * @return the organisation
     */
    @Override
    public OrganisationLinks findBySystemId(@NotNull final UUID systemId) {
        return packAsLinks(organisationRepository.findBySystemId(systemId));
    }

    /**
     * Retrieve a single organisation identified by systemId
     *
     * @param systemId systemId of the organisation
     * @return the organisation
     */
    @Override
    public Organisation findOrganisationBySystemId(@NotNull final UUID systemId) {
        return organisationRepository.findBySystemId(systemId);
    }

    /**
     * Update a particular organisation identified by the given systemId.
     *
     * @param incomingOrganisation organisation to update
     * @return the updated organisation
     */
    @Override
    @Transactional
    public OrganisationLinks update(UUID systemId, Organisation incomingOrganisation) {
        Organisation existingOrganisation =
                getOrganisationOrThrow(systemId);
        // Here copy all the values you are allowed to copy ....
        if (null != existingOrganisation.getOrganisationName()) {
            existingOrganisation.setOrganisationName(
                    incomingOrganisation.getOrganisationName());
        }
        // Note setVersion can potentially result in a NoarkConcurrencyException
        // exception as it checks the ETAG value
        existingOrganisation.setVersion(getETag());
        return packAsLinks(incomingOrganisation);
    }

    /**
     * Delete a organisation identified by the given systemId from the database.
     *
     * @param systemId systemId of the organisation to delete
     */
    @Override
    @Transactional
    public void deleteEntity(@NotNull final UUID systemId) {
        deleteEntity(getOrganisationOrThrow(systemId));
    }

    @Override
    public OrganisationLinks generateDefaultOrganisation() {
        Organisation organisation = new Organisation();
        organisation.setOrganisationName(
                "Formell navn på organisasjonen");
        organisation.setVersion(-1L, true);
        return packAsLinks(organisation);
    }

    // All HELPER methods

    public OrganisationLinks packAsLinks(
            @NotNull final Organisation organisation) {
        OrganisationLinks organisationLinks =
                new OrganisationLinks(organisation);
        applyLinksAndHeader(organisationLinks, organisationLinksBuilder);
        return organisationLinks;
    }

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. Note. If you call this, be aware
     * that you will only ever get a valid Organisation back. If there is no valid
     * Organisation, an exception is thrown
     *
     * @param systemId systemId of the organisation object you are looking for
     * @return the newly found organisation object or null if it does not exist
     */
    private Organisation getOrganisationOrThrow(@NotNull final UUID systemId) {
        Organisation organisation = organisationRepository.findBySystemId(systemId);
        if (organisation == null) {
            String info = INFO_CANNOT_FIND_OBJECT +
                    " Organisation, using systemId " + systemId;
            logger.info(info);
            throw new NoarkEntityNotFoundException(info);
        }
        return organisation;
    }
}
