package app.service.noark5.admin;

import app.domain.noark5.admin.AdministrativeUnit;
import app.domain.noark5.admin.AuthorityName;
import app.domain.noark5.admin.Organisation;
import app.domain.noark5.admin.User;
import app.domain.repository.admin.AuthorityRepository;
import app.domain.repository.admin.IUserRepository;
import app.service.IUserService;
import app.service.application.IPatchService;
import app.service.noark5.NoarkService;
import app.webapp.exceptions.NikitaMalformedInputDataException;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.exceptions.UsernameExistsException;
import app.webapp.payload.builder.interfaces.admin.IUserLinksBuilder;
import app.webapp.payload.links.admin.UserLinks;
import app.webapp.spring.security.INikitaUserDetails;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.Optional;
import java.util.UUID;

import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;
import static app.utils.constants.Constants.SYSTEM;


@Service
public class UserService
        extends NoarkService
        implements IUserService {

    private static final Logger logger =
            LoggerFactory.getLogger(UserService.class);

    private final IUserRepository userRepository;
    private final AuthorityRepository authorityRepository;
    private final IUserLinksBuilder userLinksBuilder;
    private final PasswordEncoder encoder;
    private final AdministrativeUnitService administrativeUnitService;
    private final OrganisationService organisationService;

    public UserService(EntityManager entityManager,
                       ApplicationEventPublisher applicationEventPublisher,

                       IPatchService patchService,
                       IUserRepository userRepository,
                       AuthorityRepository authorityRepository,
                       IUserLinksBuilder userLinksBuilder,
                       PasswordEncoder encoder,
                       AdministrativeUnitService administrativeUnitService,
                       OrganisationService organisationService) {
        super(entityManager, applicationEventPublisher, patchService);
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
        this.userLinksBuilder = userLinksBuilder;
        this.encoder = encoder;
        this.administrativeUnitService = administrativeUnitService;
        this.organisationService = organisationService;
    }

    // All CREATE operations

    @Override
    @Transactional
    /**
     * Create a new user
     *
     * The person issuing the new user request has to be an admin for the
     * organisation. Get the logged in user and check that they have an ADMIN
     * authority.
     *
     * If the user is not logged in then this is the creation of a user and
     * an organisation.
     *
     * @param Incoming user to create
     * @return the newly persisted User object
     */
    public UserLinks createNewUser(final User user)
            throws UsernameExistsException {
        INikitaUserDetails userDetails = getPrincipal();
        if (null != userDetails) {
            User loggedInUser = userDetails.getUser();
            user.setOrganisation(organisationService.findOrganisationBySystemId(
                    userDetails.getOrganisation().getSystemId()));
            AdministrativeUnit administrativeUnit = administrativeUnitService
                    .getAdministrativeUnitOrThrow(loggedInUser);
            administrativeUnit.addUser(user);
        } else {
            Organisation organisation = new Organisation();
            organisation.setOrganisationName("Organisasjon for " +
                    user.getFirstname() + " " + user.getLastname());
            organisation.setTitle(organisation.getOrganisationName());
            organisationService.save(organisation);
            user.setOrganisation(organisation);
        }

        UserLinks userLinks = packAsLinks(createAndSaveUser(user));

        // If no administrativeUnit is associated with this user, create a
        // default one
        if (user.getAdministrativeUnits().size() < 1) {
            AdministrativeUnit administrativeUnit = new AdministrativeUnit();
            administrativeUnit.setAdministrativeUnitName("system opprettet " +
                    "AdministrativtEnhet for bruker " + user.getUsername());
            administrativeUnit.setShortName("adminenhet " + user.getUsername());
            administrativeUnit.setDefaultAdministrativeUnit(true);
            administrativeUnit.addUser(user);
            administrativeUnitService.createNewAdministrativeUnitByUser(
                    administrativeUnit, user);
        }
        return userLinks;
    }

    /**
     * Essentially we are just calling createAndSaveUser but I am leaving
     * this as an own method in case we need to add additional logic.
     * Note: There is an assumption here that the user object already contains
     * a reference to the administrativeUnit
     *
     * @param user The incoming user object
     * @throws UsernameExistsException if the username already exists in the
     *                                 database
     */
    public void createNewUserDuringStartup(final User user)
            throws UsernameExistsException {
        createAndSaveUser(user);
    }

    // All READ operations

    @Override
    public UserLinks findBySystemID(@NotNull final UUID systemId) {
        return packAsLinks(getUserOrThrow(systemId));
    }

    @Override
    public UserLinks findAll() {
        return (UserLinks) processODataQueryGet();
    }

    /**
     * Check to see is a user with a given email address already exists
     * in the system.
     *
     * @param username The username/emailaddress to check
     * @return true if the username is registered, false otherwise
     */
    @Override
    public boolean userExists(String username) {
        return userRepository.findByUsername(username).isPresent();
    }

    /**
     * Look up username and return the equivalent User instance if it
     * exist, or null if not.
     *
     * @param username The username/emailaddress to check
     * @return User if the username is registered, null otherwise
     */
    @Override
    public User userGetByUsername(String username) {
        Optional<User> userOptional = userRepository.findByUsername(username);
        return userOptional.isEmpty() ? null : userOptional.get();
    }

    /**
     * Look up systemId/UUID and return the equivalent User instance
     * if it exist, or null if not.
     *
     * @param systemId UUID for the username to check
     * @return User if the UUID is registered, null otherwise
     */
    @Override
    public User userGetBySystemId(@NotNull final UUID systemId) {
        Optional<User> userOptional = userRepository.findBySystemId(systemId);
        return userOptional.orElse(null);
    }

    /**
     * Check to see is a particular authority already exists
     * in the system.
     *
     * @param authority The authority to check
     * @return true if the authority exists, false otherwise
     */
    @Override
    public boolean authorityExists(AuthorityName authority) {
        return authorityRepository.findByAuthorityName(authority) != null;
    }

    @Override
    @Transactional
    public UserLinks handleUpdate(@NotNull final UUID systemId,
                                  @NotNull User incomingUser) {

        User existingUser = getUserOrThrow(systemId);

        // Copy all the values you are allowed to copy ....
        if (null != incomingUser.getFirstname()) {
            existingUser.setFirstname(incomingUser.getFirstname());
        }
        if (null != incomingUser.getLastname()) {
            existingUser.setLastname(incomingUser.getLastname());
        }

        // Note setVersion can potentially result in a NoarkConcurrencyException
        // exception as it checks the ETAG value
        existingUser.setVersion(getETag());
        return packAsLinks(existingUser);
    }

    // All DELETE operations

    /**
     * Delete a user identified by the given systemId from the database.
     *
     * @param systemId systemId of the user to delete
     */
    @Override
    @Transactional
    public void deleteEntity(@NotNull final UUID systemId) {
        deleteEntity(getUserOrThrow(systemId));
    }

    /**
     * Delete all user objects
     */
    @Override
    @Transactional
    public long deleteAll() {
        return userRepository.deleteByUsername(getUser());
    }

    @Override
    public UserLinks getDefaultUser() {
        User user = new User();
        user.setUsername("example@example.com");
        user.setFirstname("Hans");
        user.setLastname("Hansen");
        user.setVersion(-1L, true);
        return packAsLinks(user);
    }

    /**
     * Delete all objects belonging to the user identified by username
     */
    @Override
    @Transactional
    public long deleteByUsername(String username) {
        return userRepository.deleteByUsername(username);
    }

    // All helper methods

    @Override
    public User validateUserReference
            (String type, User user, String username, UUID systemId) {
        if (null == user && null != systemId) {
            user = userGetBySystemId(systemId);
        }
        if (null != user &&
                (!user.getUsername().equals(username)
                        || !user.getSystemId().equals(systemId))) {
            String info = "Inconsistent " + type + " values rejected. ";
            throw new NikitaMalformedInputDataException(info);
        }
        // The values are consistent, return existing user
        return user;
    }

    private User createAndSaveUser(@NotNull final User user) {
        user.setPassword(encoder.encode(user.getPassword()));
        user.setEnabled(true);
        user.setCreatedBy(SYSTEM);
        user.setCreatedDate(OffsetDateTime.now());
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        return userRepository.save(user);
    }

    public UserLinks packAsLinks(@NotNull final User user) {
        UserLinks userLinks = new UserLinks(user);
        applyLinksAndHeader(userLinks, userLinksBuilder);
        return userLinks;
    }

    /**
     * Get logged in user
     * <p>
     * Aware returning null is not a springy approach. but this is only used
     * in the class.
     *
     * @return the logged in user
     */
    protected INikitaUserDetails getPrincipal() {
        Object principal = SecurityContextHolder
                .getContext().getAuthentication().getPrincipal();
        if (principal instanceof INikitaUserDetails) {
            return (INikitaUserDetails) principal;
        } else {
            return null;
        }
    }


    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. If you call this, be aware
     * that you will only ever get a valid User back. If there is no valid
     * User, a NoarkEntityNotFoundException exception is thrown
     *
     * @param systemId The systemId of the user object to retrieve
     * @return the user object
     */
    private User getUserOrThrow(@NotNull final UUID systemId) {
        Optional<User> userOptional =
                userRepository.findBySystemId(systemId);
        if (userOptional.isEmpty()) {
            String error = INFO_CANNOT_FIND_OBJECT + " User, using systemId " +
                    systemId;
            logger.error(error);
            throw new NoarkEntityNotFoundException(error);
        }
        return userOptional.get();
    }
}
