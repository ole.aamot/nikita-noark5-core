package app.service.interfaces;

import app.domain.noark5.admin.AdministrativeUnit;
import app.domain.noark5.casehandling.SequenceNumberGenerator;
import jakarta.validation.constraints.NotNull;

public interface ISequenceNumberGeneratorService {
    Integer getNextCaseFileSequenceNumber(
            @NotNull final AdministrativeUnit administrativeUnit);

    Integer getNextRecordSequenceNumber(
            @NotNull final AdministrativeUnit administrativeUnit);

    SequenceNumberGenerator createSequenceNumberGenerator(
            @NotNull final AdministrativeUnit administrativeUnit);
}
