package app.service.interfaces;

import app.domain.noark5.bsm.BSMBase;
import app.domain.noark5.md_other.BSMMetadata;
import jakarta.validation.constraints.NotNull;

import java.util.List;
import java.util.Optional;

public interface IBSMService {
    Optional<BSMMetadata> findBSMByName(@NotNull String name);

    void validateBSMList(@NotNull List<BSMBase> referenceBSMBase);
}
