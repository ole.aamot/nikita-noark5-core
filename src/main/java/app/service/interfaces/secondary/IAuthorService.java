package app.service.interfaces.secondary;

import app.domain.noark5.DocumentDescription;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.secondary.Author;
import app.webapp.payload.links.secondary.AuthorLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IAuthorService {

    AuthorLinks associateAuthorWithDocumentDescription(
            @NotNull final Author author,
            @NotNull final DocumentDescription documentDescription);

    AuthorLinks associateAuthorWithRecord(
            @NotNull final Author author,
            @NotNull final RecordEntity record);

    AuthorLinks updateAuthorBySystemId(
            @NotNull final UUID systemId,
            @NotNull final Author incomingAuthor);

    void deleteAuthorBySystemId(@NotNull final UUID systemId);

    AuthorLinks findBySystemId(@NotNull final UUID systemId);

    AuthorLinks generateDefaultAuthor(@NotNull final UUID systemId);
}
