package app.service.interfaces.secondary;

import app.domain.noark5.File;
import app.domain.noark5.Fonds;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.Series;
import app.domain.noark5.secondary.StorageLocation;
import app.webapp.payload.links.secondary.StorageLocationLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IStorageLocationService {

    StorageLocationLinks createStorageLocationAssociatedWithFonds(
            @NotNull final StorageLocation storageLocation,
            @NotNull final Fonds fonds);

    StorageLocationLinks createStorageLocationAssociatedWithSeries(
            @NotNull final StorageLocation storageLocation,
            @NotNull final Series series);

    StorageLocationLinks createStorageLocationAssociatedWithFile(
            @NotNull final StorageLocation storageLocation,
            @NotNull final File file);

    StorageLocationLinks createStorageLocationAssociatedWithRecord(
            @NotNull final StorageLocation storageLocation,
            @NotNull final RecordEntity record);

    StorageLocationLinks findBySystemId(@NotNull final UUID systemId);

    StorageLocationLinks findAll();

    StorageLocationLinks updateStorageLocationBySystemId(
            @NotNull final UUID systemId,
            @NotNull final StorageLocation incomingStorageLocation);

    void deleteStorageLocationBySystemId(@NotNull final UUID systemId);

    StorageLocationLinks getDefaultStorageLocation(
            @NotNull final UUID systemId);
}
