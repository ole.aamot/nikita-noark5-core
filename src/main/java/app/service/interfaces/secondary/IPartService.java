package app.service.interfaces.secondary;

import app.domain.noark5.DocumentDescription;
import app.domain.noark5.File;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.secondary.Part;
import app.domain.noark5.secondary.PartPerson;
import app.domain.noark5.secondary.PartUnit;
import app.webapp.model.PatchObjects;
import app.webapp.payload.links.secondary.PartLinks;
import app.webapp.payload.links.secondary.PartPersonLinks;
import app.webapp.payload.links.secondary.PartUnitLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IPartService {

    PartPersonLinks updatePartPerson(
            @NotNull final UUID systemId, @NotNull final PartPerson incomingPart);

    PartUnitLinks updatePartUnit(
            @NotNull final UUID systemId, @NotNull final PartUnit incomingPart);

    PartUnitLinks createNewPartUnit(
            @NotNull final PartUnit partUnit,
            @NotNull final RecordEntity record);

    PartLinks handleUpdate(
            @NotNull final UUID systemId,
            @NotNull final PatchObjects patchObjects);

    PartPersonLinks createNewPartPerson(
            @NotNull final PartPerson partPerson,
            @NotNull final RecordEntity record);

    PartUnitLinks createNewPartUnit(
            @NotNull final PartUnit partUnit,
            @NotNull final File file);

    PartPersonLinks createNewPartPerson(
            @NotNull final PartPerson partPerson,
            @NotNull final File file);

    PartUnitLinks createNewPartUnit(
            @NotNull final PartUnit partUnit,
            @NotNull final DocumentDescription documentDescription);

    PartPersonLinks createNewPartPerson(
            @NotNull final PartPerson partPerson,
            @NotNull final DocumentDescription documentDescription);

    Part findBySystemId(@NotNull final UUID systemId);

    PartPersonLinks
    findPartPersonBySystemId(@NotNull final UUID systemId);

    PartUnitLinks
    findPartUnitBySystemId(@NotNull final UUID systemId);

    // All DELETE operations

    void deletePartUnit(@NotNull final UUID systemId);

    void deletePartPerson(@NotNull final UUID systemId);

    // All template operations
    PartUnitLinks generateDefaultPartUnit(
            @NotNull final UUID systemId);

    PartPersonLinks generateDefaultPartPerson(
            @NotNull final UUID systemId);
}
