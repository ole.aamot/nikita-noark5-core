package app.service.interfaces.secondary;

import app.domain.interfaces.*;
import jakarta.validation.constraints.NotNull;

public interface ISecondaryService {
    void deleteDeletionObject(@NotNull final IDeletion deletion);

    void deleteDisposalObject(@NotNull final IDisposal disposal);

    void deleteDisposalUndertakenObject(
            @NotNull final IDisposalUndertaken disposalUndertaken);

    void deleteClassifiedObject(@NotNull final IClassified classified);

    void deleteScreeningObject(@NotNull final IScreening screening);

    void deleteElectronicSignatureObject(
            @NotNull final IElectronicSignature electronicSignature);
}
