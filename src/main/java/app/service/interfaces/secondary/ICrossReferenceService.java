package app.service.interfaces.secondary;

import app.domain.noark5.Class;
import app.domain.noark5.File;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.secondary.CrossReference;
import app.webapp.payload.links.secondary.CrossReferenceLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface ICrossReferenceService {

    CrossReferenceLinks createCrossReferenceAssociatedWithFile(
            @NotNull final CrossReference crossReference,
            @NotNull final File file);

    CrossReferenceLinks createCrossReferenceAssociatedWithRecord(
            @NotNull final CrossReference crossReference,
            @NotNull final RecordEntity record);

    CrossReferenceLinks createCrossReferenceAssociatedWithClass(
            @NotNull final CrossReference crossReference,
            @NotNull final Class klass);

    CrossReferenceLinks findBySystemId(@NotNull final UUID systemId);

    CrossReferenceLinks findAll();

    void deleteCrossReferenceBySystemId(@NotNull final UUID systemId);

    CrossReferenceLinks getDefaultCrossReference(
            @NotNull final UUID systemId);

    CrossReferenceLinks updateCrossReferenceBySystemId(
            @NotNull final UUID systemId,
            @NotNull final CrossReference crossReference);
}
