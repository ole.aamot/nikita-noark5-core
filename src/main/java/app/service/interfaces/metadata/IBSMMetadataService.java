package app.service.interfaces.metadata;

import app.domain.noark5.md_other.BSMMetadata;
import app.service.interfaces.INoarkService;
import app.webapp.model.PatchObjects;
import app.webapp.payload.links.metadata.BSMMetadataLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IBSMMetadataService
        extends INoarkService {

    /**
     * Retrieve a BSMMetadata wrapped as a BSMMetadataLinks object
     * identified by the given systemId.
     * <p>
     *
     * @param systemId UUID of the BSMMetadata object to retrieve
     * @return The BSMMetadataLinks object
     */
    BSMMetadataLinks find(@NotNull final UUID systemId);

    /**
     * Persist a BSMMetadata object to the database and wrap the
     * persistedobject as a BSMMetadataLinks.
     * <p>
     *
     * @param bsmMetadata incoming BSMMetadata object
     * @return The persisted BSMMetadataLinks object
     */
    BSMMetadataLinks save(@NotNull final BSMMetadata bsmMetadata);

    /**
     * Undertake an update to an existing BSMMetadata object identified by
     * the given systemId. The update is done using a PATCH request
     * <p>
     *
     * @param systemId     UUID of the BSMMetadata to update
     * @param patchObjects List of operations to undertake
     * @return The updated BSMMetadata object wrapped as a BSMMetadataLinks
     */
    BSMMetadataLinks handleUpdate(
            @NotNull final UUID systemId,
            @NotNull final PatchObjects patchObjects);

    /**
     * Undertake an update to an existing BSMMetadata object identified by
     * the given systemId.
     * <p>
     *
     * @param systemId    UUID of the BSMMetadata to update
     * @param bsmMetadata incoming BSMMetadata object
     * @return The updated BSMMetadata object wrapped as a BSMMetadataLinks
     */
    BSMMetadataLinks handleUpdate(
            @NotNull final UUID systemId,
            @NotNull final BSMMetadata bsmMetadata);

    /**
     * Delete the BSMMetadata object identified by the given systemId.
     * <p>
     *
     * @param systemId UUID of the BSMMetadata to delete
     */
    void deleteEntity(@NotNull final UUID systemId);

    /**
     * Retrieve all BSMMetadata wrapped as a BSMMetadataLinks object
     * <p>
     *
     * @return The BSMMetadataLinks object containing all BSMMetadata
     */
    BSMMetadataLinks findAll();
}
