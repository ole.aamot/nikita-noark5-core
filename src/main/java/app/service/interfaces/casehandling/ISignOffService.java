package app.service.interfaces.casehandling;

import app.domain.noark5.secondary.SignOff;
import app.webapp.payload.links.secondary.SignOffLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface ISignOffService {

    SignOffLinks save(@NotNull final SignOff signOff);

    // All find methods
    SignOffLinks findBySystemId(@NotNull final UUID systemId);

    SignOffLinks updateSignOff(
            @NotNull final UUID systemId,
            @NotNull final SignOff signOff);

    void deleteSignOff(@NotNull final UUID systemId);

    SignOff findSignOffBySystemId(@NotNull final UUID systemId);

    SignOffLinks generateDefaultSignOff(@NotNull final UUID systemId);

    SignOffLinks packAsLinks(@NotNull final SignOff signOff);
}
