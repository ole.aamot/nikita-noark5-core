package app.service.interfaces.admin;

import app.domain.noark5.admin.ReadLog;
import app.webapp.payload.links.admin.ReadLogLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IReadLogService {

    ReadLogLinks generateDefaultReadLog();

    ReadLogLinks createNewReadLog(@NotNull final ReadLog readLog);

    ReadLogLinks findReadLogByOwner();

    ReadLogLinks findSingleReadLog(@NotNull final UUID systemId);

    ReadLogLinks handleUpdate(@NotNull final UUID systemId,
                              @NotNull ReadLog incomingReadLog);

    void deleteEntity(@NotNull final UUID systemId);
}
