package app.service.interfaces.admin;

import app.domain.noark5.admin.CreateLog;
import app.webapp.payload.links.admin.CreateLogLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface ICreateLogService {

    CreateLogLinks generateDefaultCreateLog();

    CreateLogLinks createNewCreateLog(@NotNull final CreateLog createLog);

    CreateLogLinks findCreateLogByOwner();

    CreateLogLinks findSingleCreateLog(@NotNull final UUID systemId);

    CreateLogLinks handleUpdate(@NotNull final UUID systemId,
                                @NotNull CreateLog incomingCreateLog);

    void deleteEntity(@NotNull final UUID systemId);
}
