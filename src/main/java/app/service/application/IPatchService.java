package app.service.application;

import app.webapp.model.PatchObject;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IPatchService {
    Object handlePatch(@NotNull final UUID originalObjectId, PatchObject patchObject);
}
