package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.EventType;

public interface IEventTypeRepository
        extends IMetadataRepository<EventType, String> {

}
