package app.domain.repository.noark5.v5.other;


import app.domain.noark5.md_other.BSMMetadata;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface IBSMMetadataRepository
        extends CrudRepository<BSMMetadata, UUID> {
    Optional<BSMMetadata> findByName(String name);
}
