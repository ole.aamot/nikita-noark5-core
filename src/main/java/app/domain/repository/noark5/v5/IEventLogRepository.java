package app.domain.repository.noark5.v5;

import app.domain.noark5.EventLog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IEventLogRepository
        extends CrudRepository<EventLog, UUID> {

    EventLog findBySystemId(UUID systemId);
}
