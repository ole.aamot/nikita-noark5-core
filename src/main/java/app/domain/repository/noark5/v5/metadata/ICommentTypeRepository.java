package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.CommentType;

public interface ICommentTypeRepository
        extends IMetadataRepository<CommentType, String> {

}
