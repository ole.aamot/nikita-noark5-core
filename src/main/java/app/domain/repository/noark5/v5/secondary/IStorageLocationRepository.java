package app.domain.repository.noark5.v5.secondary;

import app.domain.noark5.secondary.StorageLocation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IStorageLocationRepository
        extends CrudRepository<StorageLocation, UUID> {
    StorageLocation findBySystemId(UUID systemId);
}
