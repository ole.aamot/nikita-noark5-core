package app.domain.repository.noark5.v5.secondary;

import app.domain.noark5.secondary.Screening;
import app.domain.repository.noark5.v5.NoarkEntityRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IScreeningRepository extends
        NoarkEntityRepository<Screening, UUID> {
}
