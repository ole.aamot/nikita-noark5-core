package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.CaseStatus;

public interface ICaseStatusRepository
        extends IMetadataRepository<CaseStatus, String> {
}
