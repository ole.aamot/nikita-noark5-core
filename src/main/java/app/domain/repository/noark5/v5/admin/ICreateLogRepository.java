package app.domain.repository.noark5.v5.admin;

import app.domain.noark5.admin.CreateLog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ICreateLogRepository extends
        CrudRepository<CreateLog, UUID> {
    CreateLog findBySystemId(UUID systemId);
}
