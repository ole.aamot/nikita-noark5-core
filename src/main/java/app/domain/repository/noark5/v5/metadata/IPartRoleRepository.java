package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.PartRole;

/**
 * Created by tsodring on 21/02/18.
 */

public interface IPartRoleRepository
        extends IMetadataRepository<PartRole, String> {
}
