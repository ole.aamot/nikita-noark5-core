package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.PrecedenceStatus;

public interface IPrecedenceStatusRepository
        extends IMetadataRepository<PrecedenceStatus, String> {
}
