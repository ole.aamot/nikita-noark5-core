package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.MeetingFileType;

public interface IMeetingFileTypeRepository
        extends IMetadataRepository<MeetingFileType, String> {

}
