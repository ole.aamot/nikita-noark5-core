package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.SignOffMethod;

public interface ISignOffMethodRepository
        extends IMetadataRepository<SignOffMethod, String> {

}
