package app.domain.repository.noark5.v5.secondary;

import app.domain.noark5.secondary.ScreeningMetadataLocal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface IScreeningMetadataLocalRepository
        extends CrudRepository<ScreeningMetadataLocal, UUID> {

    Optional<ScreeningMetadataLocal> findBySystemId(UUID systemId);
}
