package app.domain.repository.admin;


import app.domain.noark5.admin.Authority;
import app.domain.noark5.admin.AuthorityName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorityRepository
        extends JpaRepository<Authority, String> {
    Authority findByAuthorityName(AuthorityName authorityName);
}
