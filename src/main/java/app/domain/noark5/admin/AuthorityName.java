package app.domain.noark5.admin;

public enum AuthorityName {
    ADMIN, RECORDS_MANAGER, RECORDS_KEEPER, CASE_HANDLER, LEADER, GUEST,
    EMAIL_IMPORT
}
