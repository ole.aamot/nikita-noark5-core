package app.domain.noark5.admin;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.admin.ICreateLogEntity;
import app.domain.noark5.EventLog;
import app.webapp.payload.builder.noark5.admin.CreateLogLinksBuilder;
import app.webapp.payload.deserializers.noark5.admin.CreateLogDeserializer;
import app.webapp.payload.links.admin.CreateLogLinks;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_LOGGING_CREATE_LOG;
import static app.utils.constants.Constants.TABLE_CREATE_LOG;
import static app.utils.constants.N5ResourceMappings.CREATE_LOG;

@Entity
@Table(name = TABLE_CREATE_LOG)
@JsonDeserialize(using = CreateLogDeserializer.class)
@LinksPacker(using = CreateLogLinksBuilder.class)
@LinksObject(using = CreateLogLinks.class)
public class CreateLog
        extends EventLog
        implements ICreateLogEntity {
    @Override
    public String getBaseTypeName() {
        return CREATE_LOG;
    }

    @Override
    public String getBaseRel() {
        return REL_LOGGING_CREATE_LOG;
    }

}
