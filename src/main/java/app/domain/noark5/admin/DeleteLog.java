package app.domain.noark5.admin;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.admin.IDeleteLogEntity;
import app.domain.noark5.EventLog;
import app.webapp.payload.builder.noark5.admin.DeleteLogLinksBuilder;
import app.webapp.payload.deserializers.noark5.admin.DeleteLogDeserializer;
import app.webapp.payload.links.admin.DeleteLogLinks;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_LOGGING_DELETE_LOG;
import static app.utils.constants.Constants.TABLE_DELETE_LOG;
import static app.utils.constants.N5ResourceMappings.DELETE_LOG;

@Entity
@Table(name = TABLE_DELETE_LOG)
@JsonDeserialize(using = DeleteLogDeserializer.class)
@LinksPacker(using = DeleteLogLinksBuilder.class)
@LinksObject(using = DeleteLogLinks.class)
public class DeleteLog
        extends EventLog
        implements IDeleteLogEntity {
    @Override
    public String getBaseTypeName() {
        return DELETE_LOG;
    }

    @Override
    public String getBaseRel() {
        return REL_LOGGING_DELETE_LOG;
    }

}
