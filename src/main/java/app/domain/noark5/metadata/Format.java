package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_FORMAT;
import static app.utils.constants.Constants.TABLE_FORMAT;
import static app.utils.constants.N5ResourceMappings.FORMAT;

// Noark 5v5 format
@Entity
@Table(name = TABLE_FORMAT)
public class Format
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public Format() {
    }

    public Format(String code, String codename) {
        super(code, codename);
    }

    public Format(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return FORMAT;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_FORMAT;
    }
}
