package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_PART_ROLE;
import static app.utils.constants.Constants.TABLE_PART_ROLE;
import static app.utils.constants.N5ResourceMappings.PART_ROLE;
// Noark 5v5 Partrolle
@Entity
@Table(name = TABLE_PART_ROLE)
public class PartRole
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public PartRole() {
    }

    public PartRole(String code, String codename) {
        super(code, codename);
    }

    public PartRole(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return PART_ROLE;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_PART_ROLE;
    }

}
