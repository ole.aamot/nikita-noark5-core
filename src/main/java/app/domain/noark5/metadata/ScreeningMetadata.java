package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_SCREENING_METADATA;
import static app.utils.constants.Constants.TABLE_SCREENING_METADATA;
import static app.utils.constants.N5ResourceMappings.SCREENING_METADATA;

// Noark 5v5 skjermingmetadata
@Entity
@Table(name = TABLE_SCREENING_METADATA)
public class ScreeningMetadata
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public ScreeningMetadata() {
    }

    public ScreeningMetadata(String code, String codename) {
        super(code, codename);
    }

    public ScreeningMetadata(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return SCREENING_METADATA;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_SCREENING_METADATA;
    }
}
