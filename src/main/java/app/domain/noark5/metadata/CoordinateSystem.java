package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_COORDINATE_SYSTEM;
import static app.utils.constants.Constants.TABLE_COORDINATE_SYSTEM;
import static app.utils.constants.N5ResourceMappings.COORDINATE_SYSTEM;

// Noark 5v5 Koordinatsystem
@Entity
@Table(name = TABLE_COORDINATE_SYSTEM)
public class CoordinateSystem extends Metadata {

    private static final long serialVersionUID = 1L;

    public CoordinateSystem() {
    }

    public CoordinateSystem(String code, String codename) {
        super(code, codename);
    }

    public CoordinateSystem(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return COORDINATE_SYSTEM;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_COORDINATE_SYSTEM;
    }
}
