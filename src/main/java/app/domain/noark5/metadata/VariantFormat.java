package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_VARIANT_FORMAT;
import static app.utils.constants.Constants.TABLE_VARIANT_FORMAT;
import static app.utils.constants.N5ResourceMappings.VARIANT_FORMAT;

// Noark 5v5 variantformat
@Entity
@Table(name = TABLE_VARIANT_FORMAT)
public class VariantFormat
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public VariantFormat() {
    }

    public VariantFormat(String code, String codename) {
        super(code, codename);
    }

    public VariantFormat(String code) {
        super(code, null);
    }

    @Override
    public String getBaseTypeName() {
        return VARIANT_FORMAT;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_VARIANT_FORMAT;
    }
}
