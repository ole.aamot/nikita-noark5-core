package app.domain.interfaces;

import app.domain.noark5.secondary.Part;
import jakarta.validation.constraints.NotNull;

import java.util.Set;

public interface IPart {
    Set<Part> getReferencePart();

    void addPart(@NotNull Part part);

    void removePart(@NotNull Part part);
}
