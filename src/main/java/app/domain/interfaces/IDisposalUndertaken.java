package app.domain.interfaces;

import app.domain.noark5.secondary.DisposalUndertaken;

public interface IDisposalUndertaken {
    DisposalUndertaken getReferenceDisposalUndertaken();

    void setDisposalUndertaken(DisposalUndertaken disposalUndertaken);

    void removeDisposalUndertaken();
}
