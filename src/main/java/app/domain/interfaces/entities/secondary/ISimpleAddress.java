package app.domain.interfaces.entities.secondary;

import app.domain.noark5.casehandling.secondary.SimpleAddress;

/**
 * Created by tsodring on 5/22/17.
 */
public interface ISimpleAddress {

    SimpleAddress getSimpleAddress();

    void setSimpleAddress(SimpleAddress simpleAddress);
}
