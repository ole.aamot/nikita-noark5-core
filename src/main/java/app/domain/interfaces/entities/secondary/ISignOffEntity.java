package app.domain.interfaces.entities.secondary;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.casehandling.RegistryEntry;
import app.domain.noark5.casehandling.secondary.CorrespondencePart;
import app.domain.noark5.metadata.SignOffMethod;

import java.time.OffsetDateTime;
import java.util.Set;
import java.util.UUID;

// TODO check if this inheritence is ok.
public interface ISignOffEntity
        extends ISystemId {
    OffsetDateTime getSignOffDate();

    void setSignOffDate(OffsetDateTime signOffDate);

    String getSignOffBy();

    void setSignOffBy(String signOffBy);

    String getSignOffMethodCodeName();

    void setSignOffMethodCodeName(String signOffMethodCodeName);

    String getSignOffMethodCode();

    void setSignOffMethodCode(String signOffMethodCode);

    SignOffMethod getSignOffMethod();

    void setSignOffMethod(SignOffMethod signOffMethod);

    UUID getReferenceSignedOffRecordSystemID();

    void setReferenceSignedOffRecordSystemID(
            UUID referenceSignedOffRecordSystemID);

    UUID getReferenceSignedOffCorrespondencePartSystemID();

    void setReferenceSignedOffCorrespondencePartSystemID(
            UUID referenceSignedOffCorrespondencePartSystemID);

    RegistryEntry getReferenceSignedOffRecord();

    void setReferenceSignedOffRecord
            (RegistryEntry referenceSignedOffRecord);

    CorrespondencePart getReferenceSignedOffCorrespondencePart();

    void setReferenceSignedOffCorrespondencePart
            (CorrespondencePart referenceSignedOffCorrespondencePart);

    Set<RegistryEntry> getReferenceRegistryEntry();

    void setReferenceRegistryEntry(Set<RegistryEntry> referenceRegistryEntry);

    void addRecordEntity(RegistryEntry record);

    void removeRecordEntity(RegistryEntry record);
}
