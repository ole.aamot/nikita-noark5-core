package app.domain.interfaces.entities.secondary;


import app.domain.noark5.casehandling.secondary.ContactInformation;

/**
 * Created by tsodring on 5/22/17.
 */
public interface IContactInformation {

    ContactInformation getContactInformation();

    void setContactInformation(ContactInformation contactInformation);
}
