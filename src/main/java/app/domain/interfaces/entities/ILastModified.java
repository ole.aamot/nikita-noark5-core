package app.domain.interfaces.entities;

import java.time.OffsetDateTime;

public interface ILastModified {
    OffsetDateTime getLastModifiedDate();

    void setLastModifiedDate(OffsetDateTime lastModifiedDate);

    String getLastModifiedBy();

    void setLastModifiedBy(String lastModifiedBy);
}
