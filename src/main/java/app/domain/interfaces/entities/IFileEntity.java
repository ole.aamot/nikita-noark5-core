package app.domain.interfaces.entities;

import app.domain.interfaces.*;
import app.domain.noark5.secondary.Keyword;

public interface IFileEntity
        extends INoarkGeneralEntity, IDocumentMedium, IStorageLocation,
        IKeyword, IClassified, IDisposal, IScreening, IComment,
        ICrossReference, IPart, IBSM {
    String getFileId();

    void setFileId(String fileId);

    String getPublicTitle();

    void setPublicTitle(String publicTitle);

    void removeKeyword(Keyword keyword);
}
