package app.domain.interfaces.entities.admin;

import app.domain.interfaces.entities.IEventLogEntity;

public interface IDeleteLogEntity
        extends IEventLogEntity {
}
