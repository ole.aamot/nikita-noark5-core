package app.domain.interfaces.entities.admin;

import app.domain.interfaces.entities.ICreate;
import app.domain.interfaces.entities.IFinalise;
import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.admin.Organisation;
import org.springframework.security.core.GrantedAuthority;

import java.time.OffsetDateTime;
import java.util.Collection;

public interface IUserEntity
        extends INoarkEntity, ICreate, IFinalise {
    String getUsername();

    void setUsername(String username);

    String getPassword();

    void setPassword(String password);

    String getFirstname();

    void setFirstname(String firstname);

    String getLastname();

    void setLastname(String lastname);

    Boolean getEnabled();

    void setEnabled(Boolean enabled);

    boolean isAccountNonExpired();

    void setAccountNonExpired(boolean accountNonExpired);

    boolean isCredentialsNonExpired();

    void setCredentialsNonExpired(boolean credentialsNonExpired);

    boolean isAccountNonLocked();

    void setAccountNonLocked(boolean accountNonLocked);

    Organisation getOrganisation();

    void setOrganisation(Organisation organisation);

    Collection<? extends GrantedAuthority> getAuthorities();

    OffsetDateTime getLastPasswordResetDate();

    void setLastPasswordResetDate(OffsetDateTime lastPasswordResetDate);
}
