package app.domain.interfaces.entities;

import app.domain.interfaces.IElectronicSignature;
import app.domain.interfaces.IPrecedence;
import app.domain.interfaces.ISignOff;
import app.domain.noark5.metadata.RegistryEntryStatus;
import app.domain.noark5.metadata.RegistryEntryType;

import java.time.OffsetDateTime;

public interface IRegistryEntryEntity extends IRecordEntity, IRecordNoteEntity,
        IElectronicSignature, IPrecedence, ISignOff {

    Integer getRecordYear();

    void setRecordYear(Integer recordYear);

    Integer getRecordSequenceNumber();

    void setRecordSequenceNumber(Integer recordSequenceNumber);

    Integer getRegistryEntryNumber();

    void setRegistryEntryNumber(Integer registryEntryNumber);

    RegistryEntryType getRegistryEntryType();

    void setRegistryEntryType(RegistryEntryType registryEntryType);

    RegistryEntryStatus getRegistryEntryStatus();

    void setRegistryEntryStatus(RegistryEntryStatus registryEntryStatus);

    OffsetDateTime getRecordDate();

    void setRecordDate(OffsetDateTime recordDate);

    String getRecordsManagementUnit();

    void setRecordsManagementUnit(String recordsManagementUnit);

}
