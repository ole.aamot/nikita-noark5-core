package app.domain.interfaces.entities;

import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.admin.User;

import java.time.OffsetDateTime;
import java.util.UUID;

public interface IEventLogEntity
        extends ISystemId {

    UUID getReferenceArchiveUnitSystemId();

    void setReferenceArchiveUnitSystemId(UUID referenceArchiveUnitSystemId);

    OffsetDateTime getEventDate();

    void setEventDate(OffsetDateTime changedDate);

    String getEventInitiator();

    void setEventInitiator(String eventInitiator);

    UUID getEventInitiatorSystemId();

    void setEventInitiatorSystemId(UUID eventInitiatorSystemId);

    User getReferenceEventInitiator();

    void setReferenceEventInitiator(User referenceEventInitiator);

    SystemIdEntity getReferenceArchiveUnit();

    void setReferenceArchiveUnit(SystemIdEntity referenceSystemIdEntity);

}
