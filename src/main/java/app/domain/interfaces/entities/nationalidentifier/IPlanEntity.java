package app.domain.interfaces.entities.nationalidentifier;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.metadata.Country;

public interface IPlanEntity
        extends ISystemId {
    String getMunicipalityNumber();

    void setMunicipalityNumber(String municipalityNumber);

    String getCountyNumber();

    void setCountyNumber(String countyNumber);

    Country getCountry();

    void setCountry(Country country);

    String getPlanIdentification();

    void setPlanIdentification(String planIdentification);
}
