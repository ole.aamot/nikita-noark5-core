package app.domain.interfaces.entities.nationalidentifier;

import app.domain.interfaces.entities.ISystemId;

public interface IUnitEntity
        extends ISystemId {
    String getUnitIdentifier();

    void setUnitIdentifier(String organisationNumber);
}
