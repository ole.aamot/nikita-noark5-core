package app.domain.interfaces.entities.nationalidentifier;

import app.domain.interfaces.entities.ISystemId;

public interface IDNumberEntity
        extends ISystemId {
    String getdNumber();

    void setdNumber(String dNumber);
}
