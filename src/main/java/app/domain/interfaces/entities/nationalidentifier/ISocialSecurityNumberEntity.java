package app.domain.interfaces.entities.nationalidentifier;

import app.domain.interfaces.entities.ISystemId;

public interface ISocialSecurityNumberEntity
        extends ISystemId {
    String getSocialSecurityNumber();

    void setSocialSecurityNumber(String socialSecurityNumber);
}
