package app.domain.interfaces;

import app.domain.noark5.secondary.CrossReference;

import java.util.List;

public interface ICrossReference {
    List<CrossReference> getReferenceCrossReference();

    void addCrossReference(CrossReference crossReference);

    void removeCrossReference(CrossReference crossReference);
}
