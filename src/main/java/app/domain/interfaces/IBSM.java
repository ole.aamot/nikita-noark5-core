package app.domain.interfaces;

import app.domain.noark5.bsm.BSMBase;

import java.util.List;

public interface IBSM {
    List<BSMBase> getReferenceBSMBase();
    void addBSMBase(BSMBase bSMBase);

    void addReferenceBSMBase(List<BSMBase> bSMBase);

    void removeBSMBase(BSMBase bSMBase);
}
