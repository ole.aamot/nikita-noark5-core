package app.utils.constants;

public class MailConstants {
    public static final String EMAIL_FROM = "from";
    public static final String EMAIL_TO = "to";
    public static final String EMAIL_MESSAGE_TEXT = "messageText";
    public static final String EMAIL_SUBJECT = "subject";
    public static final String EMAIL_CC = "cc";
    public static final String EMAIL_ATTACHMENTS = "attachments";
}
