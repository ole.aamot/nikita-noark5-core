package app.utils.constants;

public final class ErrorMessagesConstants {
    public static final String ERROR_MAPPING_METADATA =
            "During startup, nikita found a metadata ";
    public static final String METADATA_ENTITY_MISSING =
            "Cannot find metadata entity of type: ";
    public static final String METADATA_ENTITY_CLASS_MISSING =
            "Unable to find metadata entity class type: ";
    public static final String CROSS_REFERENCE_BAD_SYSTEM_ID = "The systemID " +
            "for fromSystemID [%s] in the CrossReference you tried to create " +
            "/ update does not equal the one [%s] present on the URL of the " +
            "HTTP request";
    public static final String CROSS_REFERENCE_DUPLICATE = "Attempt to " +
            "create duplicate cross reference from [%s] to [%s]";
    public static final String ENTITY_ASSOCIATION_PROBLEM = "Problem " +
            "assigning organisation to a secondary entity reference. The " +
            "primary entity is of type [%s]. The cause is [%s].";
    public static final String STRING_IS_BLANK =
            "The [%s] string cannot contain only blank values.";
    public static final String MALFORMED_PAYLOAD =
            "The [%s] you tried to create is malformed. The following fields" +
                    " are not recognised as fields [%s]";
    public static final String DESERIALIZE_LEFTOVER =
            "The following fields are left over after deserialising [%s]. ";

    public static final String ETAG_MISSING_NON_NUMERIC = "eTag value [%s] is not numeric. Nikita  uses numeric ETAG " +
            "values >= 0.";

    public static final String ETAG_MISSING_NEGATIVE = "Illegal eTag value [%s]. ETAG values show version of an " +
            "entity in the database and start at 0. Must be >= 0. Note An eTag is assigned to you.";

    public static final String REQUIRED_ETAG_MISSING = "When undertaking a %s request, an ETAG header must be present.";

    public static final String CALL_TO_PARENT_DESERIALIZER_NOT_ALLOWED = "Somehow " +
            "a child called NoarkDeserializer. This should not happen";

    public static final String CALL_TO_PARENT_GET_TYPE_NOT_ALLOWED = "Somehow " +
            "a child called NoarkDeserializer::getType(). This should not happen";
}
