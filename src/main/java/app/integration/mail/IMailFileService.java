package app.integration.mail;

import java.nio.file.Path;

public interface IMailFileService {
    String writeFileToStorage(Path path, String filename);
}
