package app.integration.crud;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

import static app.utils.constants.N5ResourceMappings.*;

/*
{
  "systemID": "9e913f66-81ea-4abd-b383-2a7041370af3",
  "opprettetAv" : "Bob the builder",
  "opprettetDato": "2023-12-08T01:00:36-08:00",
  "eventType": "CREATE",
  "objectType": "File"
}
 */
public record CRUDMessage(@JsonProperty(SYSTEM_ID) String systemId,
                          @JsonProperty(CREATED_BY) String createdBy,
                          @JsonProperty(CREATED_DATE) String createdDate,
                          @JsonProperty("eventType") String eventType,
                          @JsonProperty("objectType") String objectType
)
        implements Serializable {

    @Override
    public String toString() {
        return "{" +
                "\"" + SYSTEM_ID + "\": " + "\"" + systemId + "\"," +
                "\"" + CREATED_BY + "\": " + "\"" + createdBy + "\"," +
                "\"" + CREATED_DATE + "\": " + "\"" + createdDate + "\"," +
                "\"" + "eventType" + "\": " + "\"" + eventType + "\"," +
                "\"" + "objectType" + "\": " + "\"" + objectType + "\"" +
                "}";
    }
}
