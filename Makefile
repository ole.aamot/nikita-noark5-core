MVN = mvn

all: build

build:
	$(MVN) clean validate install
run: build
	curl http://localhost:8080/realms/recordkeeping/.well-known/openid-configuration|jq .authorization_endpoint |grep -q /protocol/ || echo "error: Unable to reach Keycloak API.  Is it running and configured?"
	$(MVN) -f pom.xml spring-boot:run
clean:
	$(MVN) -DskipTests=true clean
