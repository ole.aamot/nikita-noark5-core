import json
import logging

from constants import *
from net import *

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)


def create_nikita_vsm(nikita_header):
    vsm_objects = [{
        "navn": "email-v1:recipients",
        "type": "string",
        "utdatert": False,
        "beskrivelse": "",
        "kilde": "https://example.com"
    }, {
        "navn": "email-v1:messageId",
        "type": "string",
        "utdatert": False,
        "beskrivelse": "",
        "kilde": "https://example.com"
    }, {
        "navn": "email-v1:subject",
        "type": "string",
        "utdatert": False,
        "beskrivelse": "",
        "kilde": "https://example.com"
    }, {
        "navn": "email-v1:carbonCopies",
        "type": "string",
        "utdatert": False,
        "beskrivelse": "",
        "kilde": "https://example.com"
    }, {
        "navn": "email-v1:messageText",
        "type": "string",
        "utdatert": False,
        "beskrivelse": "",
        "kilde": "https://example.com"
    }
    ]

    for vsm_payload in vsm_objects:
        response = do_post_request(nikita_create_metadata_url, json.dumps(vsm_payload),
                                   nikita_header)
        if response.status == 201:
            logging.info(NIKITA_USER_CREATE_OK.format(vsm_payload['navn'], response.status))
        else:
            logging.error(NIKITA_USER_CREATE_NOT_OK.format(vsm_payload['navn'], response.status))
