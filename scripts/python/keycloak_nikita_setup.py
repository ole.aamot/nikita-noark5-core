import logging

from keycloak_user_setup import *
from nikita_user_data_setup import *
from nikita_user_setup import *
from nikita_vsm_setup import *

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)

# Toggle which operations that should occur. Useful when debugging

create_keycloak_users = False
create_nikita_users = True
create_nikita_user_data = True
create_nikita_vsm_data = True

def main():
    if create_keycloak_users:
        logging.info('Start Keycloak setup')
        keycloak_admin_token = login_keycloak_admin(url_admin_login, admin_client,
                                                    keycloak_admin_username,
                                                    keycloak_admin_password)
        keycloak_admin_header = {'Authorization': 'Bearer ' + keycloak_admin_token,
                                 'Content-type': 'application/json'}
        keycloak_admin_header_get = {'Authorization': 'Bearer ' + keycloak_admin_token}
        create_keycloak_realm(keycloak_admin_header)
        create_keycloak_roles(keycloak_admin_header)
        create_keycloak_client(keycloak_admin_header)
        create_keycloak_nikita_admin_user(keycloak_admin_header, nikita_admin_username,
                                          nikita_admin_password)
        create_keycloak_general_user(keycloak_admin_header)
        add_keycloak_users_to_role(keycloak_admin_header_get, keycloak_admin_header)
        logging.info('Finished Keycloak setup')

    if create_nikita_users:
        nikita_admin_token = login_nikita_user(url_nikita_user_login, nikita_client,
                                               nikita_admin_username, nikita_admin_password)
        nikita_admin_header = {'Authorization': 'Bearer ' + nikita_admin_token,
                               'Content-type': CONTENT_TYPE_NOARK}
        logging.info('Start Nikita user accounts setup')
        # User admin@example.com is added by default during nikita startup. Leaving this here
        # to show that it can 
        # create_nikita_user(nikita_admin_header, nikita_admin_username, nikita_admin_password)
        create_nikita_general_users(nikita_admin_header)
        logging.info('Finished Nikita user accounts setup')

    # Setup required for importing email / teaching at OsloMet
    if create_nikita_vsm_data:
        logging.info('Start Nikita VSM data setup')
        nikita_admin_token = login_nikita_user(url_nikita_user_login, nikita_client,
                                               nikita_admin_username, nikita_admin_password)
        nikita_admin_header = {'Authorization': 'Bearer ' + nikita_admin_token,
                               'Content-type': CONTENT_TYPE_NOARK}
        create_nikita_vsm(nikita_admin_header)

        # for user_id in range(1, total_nikita_user_to_create):
        #     username = nikita_username_template.format(str(user_id))
        #     password = nikita_password_template.format(str(user_id))
        #     logging.info('Creating VSM for user [{}]'.format(username))
        #     nikita_user_token = login_nikita_user(url_nikita_user_login, nikita_client,
        #                                           username, password)
        #     nikita_user_header_post = {'Authorization': 'Bearer ' + nikita_user_token,
        #                                'Content-type': CONTENT_TYPE_NOARK}
        #     create_nikita_vsm(nikita_user_header_post)
        logging.info('Finished Nikita VSM data setup')

    # Setup required for teaching at OsloMet
    if create_nikita_user_data:
        for user_id in range(1, total_nikita_user_to_create):
            username = nikita_username_template.format(str(user_id))
            logging.info('Start creating Nikita user data for user [{}]'.format(username))
            password = nikita_password_template.format(str(user_id))
            nikita_user_token = login_nikita_user(url_nikita_user_login, nikita_client,
                                                   username, password)
            nikita_user_header_post = {'Authorization': 'Bearer ' + nikita_user_token,
                                       'Content-type': CONTENT_TYPE_NOARK}
            nikita_user_header_get = {'Authorization': 'Bearer ' + nikita_user_token}

            organisation_name = 'organisasjon' + str(user_id)
            app_root = get_application_root(nikita_application_root_url, nikita_user_header_get)
            created_fonds = create_fonds(app_root[LINKS][NEW_FONDS_REL][HREF], organisation_name,
                                         nikita_user_header_post)
            create_fonds_creator(created_fonds[LINKS][NEW_FONDS_CREATOR_REL][HREF],
                                 organisation_name, nikita_user_header_post)
            create_email_series(created_fonds[LINKS][NEW_SERIES_REL][HREF], str(user_id),
                                nikita_user_header_post)
            create_regular_series(created_fonds[LINKS][NEW_SERIES_REL][HREF], str(user_id),
                                  nikita_user_header_post)
            logging.info('Finished creating Nikita user data')


if __name__ == '__main__':
    main()
