# Nikita as a spring application

Nikita is a basic spring boot application.

## Basic code structure

Nikita is a spring boot application and adheres to the following structure

### app/domain

All code that is related to the database is stored here.
Nikita used hibernate as the basic ORM model, but also makes use of Sprig Data CrudRepository for find-by-id and
delete-by-id types of.

Note: Database configuration is handled in application yml files.

*application.yml*: H2 Database, used
*application-mariadb.yml*: mariadb if profile is set to 'mariadb'
*application-postgres.yml*: postgres if profile is set to 'postgres'

### app/service:

It is a common practice that the serice layer acts as a boundary for database interaction. Once a method in the service
layer is finished you should no longer assume you can interact with the database. Nikita follows this practice. Note. It
is normal to have the controller deal with all issues relating to HTTP, however the Noark 5 API specification requires
the setting of ETAG values and OPTIONS header. These are values that are derived from the database.

service.interfaces
service.implementation

### app/controller:

The controller package deals with most HTTP issues. It provides endpoints

On of guiding principles used in app is to capture problems as early as possible and to throw an exception on the
incoming request. This is done to ensure the lower layers don't have to check for validity all

### app/webapp

### app/utils

### app/N5CoreApp.java

### app/integration

### app/optional

Such an approach provides a predictable structure with clear boundaries codebase between the layers.

Nikita differentiates between Noark elements in code andNikita elements in code.

The service package contains all business logic.

##  