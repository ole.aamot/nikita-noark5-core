# Updated install information for nikita

To build the jar file, run 'make'.  To start the service, run 'make
run'.

The server need a place to store uploaded documents.  Make sure
nikita->startup->base-directory in src/main/resources/application.yml
point to a writable directory.  The default value /data2 is most
likely not the one you want.

The server need a working connection to a Keycloak service.  Make sure
spring->security->oauth2->provider->keycloak->issuer-uri in
+src/main/resources/application.yml point to a working service.  The
default http://localhost:8080/realms/recordkeeping can be used if you
set up a standalone service using scripts/keycloak-setup-start.sh.

## RabbitMQ

##  
