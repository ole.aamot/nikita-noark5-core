# List of OData queries that app should suuprt

See the class TestOData for tests

This document should be replaced with example OData queries and an explanation of them

$filter=round(Decimal) eq 100
$filter=day(DateTime) eq 21
$filter=year(DateTime) eq 2010
$filter=floor(Decimal) eq 0
$filter=second(DateTime) eq 55
$filter=month(DateTime) eq 2
$filter=hour(DateTime) eq 1
$filter=minute(DateTime) eq 42
$filter=round(Decimal) eq 100
$filter=ceiling(Decimal) eq 1

$filter=(filnavn eq '<20180803044936.GA20015@mail.bluemosh.com>' and year(dokumentDato) eq 2018) or month(dokumentDato) eq 2

arkivstruktur/dokumentobjekt?filter=contains(filnavn, '<20180803044936.GA20015@mail.bluemosh.com>')
arkivstruktur/arkiv?$filter=contains(location, 'P48-S554') and temperat gt '22'$top=2$skip=4$orderby=tittel desc
https://n5.example.com/noark5v5/api/arkivstruktur/arkiv?$filter=tittel eq 'hello'$top=2$orderby=tittel desc
https://n5.example.com/noark5v5/api/arkivstruktur/arkiv?$top=2$skip=4$filter=tittel eq 'hello'$orderby=beskrivelse desc
https://n5.example.com/noark5v5/api/arkivstruktur/mappe?$filter=tittel lt 'hello'
https://n5.example.com/noark5v5/api/arkivstruktur/arkiv?$filter=startsWith(tittel, 'hello')
https://n5.example.com/noark5v5/api/arkivstruktur/arkiv?$filter=contains(beskrivelse, 'hello')
https://n5.example.com/noark5v5/api/arkivstruktur/mappe?$filter=tittel eq 'hello'

https://n5.example.com/noark5v5/api/arkivstruktur/mappe?$filter=tittel eq 'hello'
https://n5.example.com/noark5v5/api/arkivstruktur/mappe?$filter=systemID eq '12345'
https://n5.example.com/noark5v5/api/arkivstruktur/mappe?$filter=systemID eq 'hello' and tittel eq 'goodbye'
https://app.hioa.no/noark5v5/api/arkivstruktur/mappe?$filter=contains(tittel,'Eating')

https://n5.example.com/noark5v5/api/arkivstruktur/mappe?$filter=systemID eq '12345'
https://n5.example.com/noark5v5/api/arkivstruktur/mappe?$filter=year(endretDato) gt 2012
https://n5.example.com/noark5v5/api/arkivstruktur/mappe?$filter=year(endretDato) lt 2012
https://n5.example.com/noark5v5/api/arkivstruktur/mappe?$filter=year(endretDato) ge 2012
https://n5.example.com/noark5v5/api/arkivstruktur/mappe?$filter=year(endretDato) le 2012
https://n5.example.com/noark5v5/api/arkivstruktur/arkivdel/245ff5c7-c74b-4e92-89f5-78ab0ed6b50d/saksmappe?$top=2&$skip=4&$filter=tittel eq 'hello'&$orderby=beskrivelse desc
https://n5.example.com/noark5v5/api/arkivstruktur/arkiv?$filter=contains(location, 'P48-S554') and temperature gt '22'&$top=2&$skip=4&$orderby=tittel desc
https://n5.example.com/noark5v5/api/arkivstruktur/arkiv?$filter=tittel eq 'hello'&$top=2&$orderby=tittel desc
https://n5.example.com/noark5v5/api/arkivstruktur/arkiv?$top=2&$skip=4&$filter=tittel eq 'hello'&$orderby=beskrivelse desc
https://n5.example.com/noark5v5/api/arkivstruktur/mappe?$filter=tittel lt 'hello'
https://n5.example.com/noark5v5/api/arkivstruktur/arkiv?$filter=startsWith(tittel, 'hello')
https://n5.example.com/noark5v5/api/arkivstruktur/arkiv?$filter=contains(beskrivelse, 'hello')
https://n5.example.com/noark5v5/api/arkivstruktur/mappe?$filter=tittel eq 'hello'

https://n5.example.com/noark5v5/api/arkivstruktur/mappe?$filter=tittel eq 'hello'
https://n5.example.com/noark5v5/api/arkivstruktur/mappe?$filter=systemID eq '12345'
https://n5.example.com/noark5v5/api/arkivstruktur/mappe?$filter=systemID eq 'hello' and tittel eq 'goodbye'
https://app.hioa.no/noark5v5/api/arkivstruktur/mappe?$filter=contains(tittel,'Eating')

https://n5.example.com/noark5v5/api/arkivstruktur/mappe?$filter=systemID eq '12345'
https://n5.example.com/noark5v5/api/arkivstruktur/mappe?$filter==year(endretDato) gt 2012
https://n5.example.com/noark5v5/api/arkivstruktur/mappe?$filter==year(endretDato) lt 2012
https://n5.example.com/noark5v5/api/arkivstruktur/mappe?$filter==year(endretDato) ge 2012
https://n5.example.com/noark5v5/api/arkivstruktur/mappe?$filter==year(endretDato) le 2012
